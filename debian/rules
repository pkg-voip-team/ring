#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

# Hardening
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

# Build with LFS (See #913186)
CFLAGS += -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
CXXFLAGS += -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64

# Number of CPUS
NO_CPUS=$(shell nproc)
ifeq ($(NO_CPUS),0)
NO_CPUS=1
endif

%:
	dh $@ --without autoreconf

override_dh_auto_configure:
# Re-create tarballs from tarballs-unpacked
	mkdir -p daemon/contrib/tarballs
	for i in $(CURDIR)/daemon/contrib/tarballs-unpacked/*; do \
	  projectname=`basename $$i` && \
	  cd $$i && tar -caf ../../tarballs/$$projectname * && \
	  echo "Repacked $$projectname"; \
	done

# Daemon configure
	mkdir -p daemon/contrib/native
	cd daemon/contrib/native && \
	  ../bootstrap \
	    --disable-downloads \
	    --no-checksums \
	    --disable-ogg \
	    --disable-flac \
	    --disable-gmp \
	    --disable-vorbis \
	    --disable-vorbisenc \
	    --disable-speex \
	    --disable-sndfile \
	    --disable-gsm \
	    --disable-speexdsp \
	    --disable-asio \
	    --disable-boost \
	    --disable-cryptopp \
	    --disable-ffmpeg \
	    --disable-restbed \
	    --disable-vpx \
	    --disable-x264 \
	    --disable-yaml-cpp \
	    --disable-opendht \
	    --disable-gnutls \
	    --disable-sdbus-cpp \
	    --disable-secp256k1 \
	    --disable-upnp \
	    --disable-restinio \
	    --disable-fmt \
	    --disable-http_parser \
	    --disable-libarchive \
	    --disable-natpmp \
	    --disable-libgit2 \
	    --disable-webrtc-audio-processing && \
	  make list && \
	  make -j$(NO_CPUS) V=1
ifneq (,$(filter $(DEB_HOST_ARCH), armel mipsel))
# Link atomic library
	sed -i '/jamid_LDADD/s/libjami.la/& -latomic/' \
	  daemon/bin/dbus/Makefile.am
endif
	cd daemon && \
	  ./autogen.sh && \
	  ./configure \
	    --prefix=/usr \
	    --disable-shared

# Qt client configure
	dh_auto_configure -- \
	  -DENABLE_LIBWRAP=false \
	  -DWITH_WEBENGINE=false

override_dh_auto_build:
# Daemon build
	make -C daemon -j$(NO_CPUS) V=1
	pod2man daemon/man/jamid.pod > daemon/jamid.1

# Qt client build
	dh_auto_build

override_dh_auto_clean:
# Generated contrib tarballs
	rm -rfv daemon/contrib/tarballs

# Daemon clean
	[ -f daemon/contrib/native/Makefile ] && \
	  make -C daemon/contrib/native distclean || true
	rm -rfv daemon/contrib/native
	rm -rfv daemon/jamid.1

# Qt client clean
	dh_auto_clean

override_dh_auto_install:
# Daemon install
	cd daemon && make DESTDIR=$(CURDIR)/debian/jami-daemon install
	rm -rfv $(CURDIR)/debian/jami-daemon/usr/include
	rm -rfv $(CURDIR)/debian/jami-daemon/usr/lib/*.a
	rm -rfv $(CURDIR)/debian/jami-daemon/usr/lib/*.la
	rm -rfv $(CURDIR)/debian/jami-daemon/usr/lib/pkgconfig
	rm -rv  $(CURDIR)/debian/jami-daemon/usr/lib
# CMake files
	rm -rfv $(CURDIR)/debian/jami/usr/lib/cmake

# Qt client install
	dh_auto_install --destdir=$(CURDIR)/debian/jami

# Work around dak not liking files with timestamps too far in the past
execute_before_dh_strip_nondeterminism:
	find $(CURDIR)/debian/jami/ ! -newermt "jan 01, 2000" \
	  -exec touch --date="@$(SOURCE_DATE_EPOCH)" {} +


tmp_dir := $(shell mktemp -d)
version := $(shell dpkg-parsechangelog -ldebian/changelog | perl -ne \
                   'print $$1 if m{^Version:\s+(?:\d+:)?(\d.*)(?:\~ds.+)(?:\-\d+.*)};')
ds := $(shell dpkg-parsechangelog -ldebian/changelog | perl -ne \
              'print $$1 if m{^Version:\s+(?:\d+:)?(?:\d.*)(\~ds.+)(?:\-\d+.*)};')
pj := $(tmp_dir)/jami-*/daemon/contrib/tarballs-unpacked/pjproject-*/pjproject-*

# Repack the tarball with contrib tarballs unpacked
get-orig-source:
# Download jami tarball
	if [ -n "$$GET_ORIG_SOURCE_OVERRIDE_USCAN_TARBALL" ]; then \
	  mv $$GET_ORIG_SOURCE_OVERRIDE_USCAN_TARBALL $(tmp_dir); \
	else \
	  uscan \
	    --rename \
	    --destdir=$(tmp_dir) \
	    --download-version $(version); \
	fi

# Unpack jami tarball
	tar -C $(tmp_dir) -xf $(tmp_dir)/*.tar.*

# Remove original tarball
	rm $(tmp_dir)/*.tar.*

# Unpack all of the orig tarballs into tarballs-unpacked
	cd $(tmp_dir)/jami-*/daemon/contrib && \
	  mkdir tarballs-unpacked && \
	  for i in `find tarballs/ -name "*.tar.*"`; do \
	    project_name=`basename $$i` && \
	    mkdir tarballs-unpacked/$$project_name && \
	    tar -C tarballs-unpacked/$$project_name -xf $$i && \
	    echo "Unpacked $$project_name"; \
	  done

# Exclude files
#   This does not exclude everything that we need to exclude; so
#   debian/copyright's Files-Excluded section also excludes files.
	rm -rf $(pj)/build/vs # not needed, Visual Studio files
	rm -f  $(pj)/pjmedia/src/pjmedia-audiodev/s60_g729_bitstream.h # non distributable
	rm -f  $(pj)/pjsip-apps/src/pjsua/android/gradle/wrapper/* # not needed
	rm -r  $(pj)/pjsip-apps/src/swig/java/android/gradle/wrapper/* # not needed
	rm -rf $(pj)/third_party/g7221 # non-distributable (requires a license from Polycom)
	rm -rf $(pj)/third_party/ilbc # non distributable (the version included with pjproject)
	rm -rf $(pj)/third_party/milenage # non distributable
	rm -rf $(pj)/third_party/BaseClasses # non distributable (?)
	rm -rf $(pj)/third_party/portaudio # not needed
	rm -rf $(pj)/third_party/speex # not needed
	rm -rf $(pj)/third_party/mp3 # not needed
	rm -rf $(pj)/third_party/gsm # not needed
	rm -rf $(pj)/third_party/bdsound # not needed

# Remove all contrib tarballs
	rm -rf $(tmp_dir)/jami-*/daemon/contrib/tarballs

# Create the new tarball
	cd $(tmp_dir) && \
	  tar -czf \
	    $(CURDIR)/../ring_$(version)$(ds).orig.tar.gz \
	    jami-*

# Remove the temporary directory
	rm -rf $(tmp_dir)
