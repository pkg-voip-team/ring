<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="nn" sourcelanguage="en">
<context>
    <name>CallAdapter</name>
    <message>
        <location filename="../src/app/calladapter.cpp" line="220"/>
        <source>Missed call</source>
        <translation>Miska ring</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="221"/>
        <source>Missed call with %1</source>
        <translation>Miska samtale med %1</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="542"/>
        <source>Incoming call</source>
        <translation>Innkommende samtal</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="543"/>
        <source>%1 is calling you</source>
        <translation>%1 ringer deg</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="554"/>
        <source>is calling you</source>
        <translation>ringar deg</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="1052"/>
        <source>Screenshot</source>
        <translation>Skjermbild</translation>
    </message>
</context>
<context>
    <name>ConversationsAdapter</name>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="189"/>
        <source>%1 received a new message</source>
        <translation>% 1 fekk ein ny melding</translation>
    </message>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="244"/>
        <source>%1 received a new trust request</source>
        <translation>%1 fekk ein ny tillitsforlang</translation>
    </message>
</context>
<context>
    <name>CurrentCall</name>
    <message>
        <location filename="../src/app/currentcall.cpp" line="185"/>
        <source>Me</source>
        <translation>Eg er berre ein</translation>
    </message>
</context>
<context>
    <name>CurrentConversation</name>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="137"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="139"/>
        <source>Private group (restricted invites)</source>
        <translation>Privat gruppe (tilbud med begrensning)</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="141"/>
        <source>Private group</source>
        <translation>Privat gruppe</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="143"/>
        <source>Public group</source>
        <translation>offentlege grupperingar</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="324"/>
        <source>An error occurred while fetching this repository</source>
        <translation>Det kom ei feil medan du hent dette repository</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="326"/>
        <source>Unrecognized conversation mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="330"/>
        <source>Not authorized to update conversation information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="332"/>
        <source>An error occurred while committing a new message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="328"/>
        <source>An invalid message was detected</source>
        <translation>Ein urettleg melding vart oppdaga.</translation>
    </message>
</context>
<context>
    <name>JamiStrings</name>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="29"/>
        <source>Accept</source>
        <translation>Ta imot</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="30"/>
        <source>Accept in audio</source>
        <translation>Ta imot i lyd</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="31"/>
        <source>Accept in video</source>
        <translation>Ta imot i video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="32"/>
        <source>Refuse</source>
        <translation>Avviser</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="33"/>
        <source>End call</source>
        <translation>Avslutt samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="34"/>
        <source>Incoming audio call from {}</source>
        <translation>Innkommende lydsamtale frå {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="35"/>
        <source>Incoming video call from {}</source>
        <translation>Innkommende videoanrop frå {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="38"/>
        <source>Invitations</source>
        <translation>Invitasjonar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="39"/>
        <source>Jami is a universal communication platform, with privacy as its foundation, that relies on a free distributed network for everyone.</source>
        <translation>Jami er ein universell kommunikasjonsplattform, med personvernet som grunnlaget, som byggjer på eit fritt distribuert nettverk for alle.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="40"/>
        <source>Migrating to the Swarm technology will enable synchronizing this conversation across multiple devices and improve reliability. The legacy conversation history will be cleared in the process.</source>
        <translation>Med å flytta til Swarm-teknologien, kan du synkronisere samtalen mellom fleire enheter og bedøve påliteligheten.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="44"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="733"/>
        <source>Could not re-connect to the Jami daemon (jamid).
Jami will now quit.</source>
        <translation>Han kunne ikkje knytta seg til djævelen Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="45"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="732"/>
        <source>Trying to reconnect to the Jami daemon (jamid)…</source>
        <translation>Eg prøver å knyta meg til djævelen Jami (jamid)...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="48"/>
        <source>Version</source>
        <translation>Versionen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="51"/>
        <source>Jami is a free universal communication software that respects the freedom and privacy of its users.</source>
        <translation>Jami er ein gratis, universell kommunikasjonsoftware som respekterer friheten og privatlivet til brukarane sine.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="54"/>
        <source>Display QR code</source>
        <translation>Visa QR-kode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="55"/>
        <source>Open settings</source>
        <translation>Åpne innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="56"/>
        <source>Close settings</source>
        <translation>Nærm innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="57"/>
        <source>Add Account</source>
        <translation>Legg til konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="60"/>
        <source>Add to conference</source>
        <translation>Legg til konferansen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="61"/>
        <source>Add to conversation</source>
        <translation>Legg til samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="62"/>
        <source>Transfer this call</source>
        <translation>Flytta dette samtalet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="63"/>
        <source>Transfer to</source>
        <translation>Overføring til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="66"/>
        <source>Authentication required</source>
        <translation>Trenger verifikasjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="67"/>
        <source>Your session has expired or been revoked on this device. Please enter your password.</source>
        <translation>Sessionen din har gått ut eller er avkalla på denne enna.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="68"/>
        <source>JAMS server</source>
        <translation>JAMS server</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="69"/>
        <source>Authenticate</source>
        <translation>Tru deg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="70"/>
        <source>Delete account</source>
        <translation>Ta bort kontoen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="71"/>
        <source>In progress…</source>
        <translation>I framløpet...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="72"/>
        <source>Authentication failed</source>
        <translation>Authentifisering mislykt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="73"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="74"/>
        <source>Username</source>
        <translation>Bruksnamn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="75"/>
        <source>Alias</source>
        <translation>Aliefin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="78"/>
        <source>Allow incoming calls from unknown contacts</source>
        <translation>Tillate innkommende samtal frå ukjente kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="79"/>
        <source>Convert your account into a rendezvous point</source>
        <translation>Omdann kontoen din til ein møtepunkt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="80"/>
        <source>Automatically answer calls</source>
        <translation>Du svarar automatisk på samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="81"/>
        <source>Enable custom ringtone</source>
        <translation>Aktivei tilgjengeleg ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="82"/>
        <source>Select custom ringtone</source>
        <translation>Velg ein egnerk ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="83"/>
        <source>Select a new ringtone</source>
        <translation>Velg ein ny ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="84"/>
        <source>Certificate File (*.crt)</source>
        <translation>Dokumentarfil (*.crt)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="85"/>
        <source>Audio File (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</source>
        <translation>Lydfil (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="86"/>
        <source>Push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="87"/>
        <source>Enable push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="88"/>
        <source>Keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="89"/>
        <source>Change keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="92"/>
        <source>Change shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="93"/>
        <source>Press the key to be assigned to push-to-talk shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="94"/>
        <source>Assign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="97"/>
        <source>Enable read receipts</source>
        <translation>Legg til å lese kvitteringar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="98"/>
        <source>Send and receive receipts indicating that a message have been displayed</source>
        <translation>Sende og motta kvitteringar som viser at ein melding er vist</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="101"/>
        <source>Voicemail</source>
        <translation>Stemmelpost</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="102"/>
        <source>Voicemail dial code</source>
        <translation>Ryktkode for røystpost</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="105"/>
        <source>Security</source>
        <translation>Tryggleik</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="106"/>
        <source>Enable SDES key exchange</source>
        <translation>Legg til lag at SDES-nøkkelveksling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="107"/>
        <source>Encrypt negotiation (TLS)</source>
        <translation>Kryptering av forhandlingar (TLS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="108"/>
        <source>CA certificate</source>
        <translation>Kvar som er avgreidd</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="109"/>
        <source>User certificate</source>
        <translation>Brukarcertifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="110"/>
        <source>Private key</source>
        <translation>Privatnøkkel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="111"/>
        <source>Private key password</source>
        <translation>Passord med privatnøkkel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="112"/>
        <source>Verify certificates for incoming TLS connections</source>
        <translation>Verifisere sertifiseringar for innkommende TLS-tilkoblingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="113"/>
        <source>Verify server TLS certificates</source>
        <translation>Verifisere TLS-serversertifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="114"/>
        <source>Require certificate for incoming TLS connections</source>
        <translation>Krev attester for innkommende TLS-tilkoblingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="116"/>
        <source>Select a private key</source>
        <translation>Velg ein privat nøkkel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="117"/>
        <source>Select a user certificate</source>
        <translation>Velg ein brukarsertifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="118"/>
        <source>Select a CA certificate</source>
        <translation>Velg ein CA-sertifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="120"/>
        <source>Key File (*.key)</source>
        <translation>Nøkkelfil (*.nøkk)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="123"/>
        <source>Connectivity</source>
        <translation>Konektivitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="124"/>
        <source>Auto Registration After Expired</source>
        <translation>Auto registrering etter at den er utløyande</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="125"/>
        <source>Registration expiration time (seconds)</source>
        <translation>Registreringstidspunktet (sekunder)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="126"/>
        <source>Network interface</source>
        <translation>Nettverksgrensesnitt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="127"/>
        <source>Use UPnP</source>
        <translation>Bruk UPnP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="128"/>
        <source>Use TURN</source>
        <translation>Bruk TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="129"/>
        <source>TURN address</source>
        <translation>Adress til TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="130"/>
        <source>TURN username</source>
        <translation>Brukarnamn TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="131"/>
        <source>TURN password</source>
        <translation>Passord til TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="132"/>
        <source>TURN Realm</source>
        <translation>TURN-riket</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="133"/>
        <source>Use STUN</source>
        <translation>Bruk STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="134"/>
        <source>STUN address</source>
        <translation>Adressen til STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="137"/>
        <source>Allow IP Auto Rewrite</source>
        <translation>Lat IP- omskriving automatisk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="138"/>
        <source>Public address</source>
        <translation>Openbar adresse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="139"/>
        <source>Use custom address and port</source>
        <translation>Bruk tilpassad adresse og port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="140"/>
        <source>Address</source>
        <translation>Adress</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="141"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="144"/>
        <source>Media</source>
        <translation>Medeldamedia</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="145"/>
        <source>Enable video</source>
        <translation>Legg video til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="153"/>
        <source>SDP Session Negotiation (ICE Fallback)</source>
        <translation>Forhandlingar om SDP-møtet (ICE fallback)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="154"/>
        <source>Only used during negotiation in case ICE is not supported</source>
        <translation>berre brukt under forhandlingar i tilfelle ICE ikkje vert støtta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="155"/>
        <source>Audio RTP minimum Port</source>
        <translation>Lyd RTP minimumsport</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="156"/>
        <source>Audio RTP maximum Port</source>
        <translation>Høyeste RTP-port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="157"/>
        <source>Video RTP minimum Port</source>
        <translation>Video RTP minimumsport</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="158"/>
        <source>Video RTP maximum port</source>
        <translation>Maksimal RTP-port for video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="161"/>
        <source>Enable local peer discovery</source>
        <translation>Legg til lag for lokal peer-oppdaging</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="162"/>
        <source>Connect to other DHT nodes advertising on your local network.</source>
        <translation>Knyt deg til andre DHT-knodar som annonserer på lokalet ditt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="164"/>
        <source>Enable proxy</source>
        <translation>Legg på proxy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="165"/>
        <source>Proxy address</source>
        <translation>Adressen til representant</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="166"/>
        <source>Bootstrap</source>
        <translation>Bootstrap</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="169"/>
        <source>Back</source>
        <translation>Tilbaka</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="170"/>
        <source>Account</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="171"/>
        <source>General</source>
        <translation>Generell</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="172"/>
        <source>Extensions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="182"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="183"/>
        <source>Microphone</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="184"/>
        <source>Select audio input device</source>
        <translation>Velg ein lydinnlegg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="185"/>
        <source>Output device</source>
        <translation>Utgangseining</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="186"/>
        <source>Select audio output device</source>
        <translation>Velg lydutgjevarett</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="187"/>
        <source>Ringtone device</source>
        <translation>Ringtoneapparat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="188"/>
        <source>Select ringtone output device</source>
        <translation>Velg ringtone utgangseining</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="189"/>
        <source>Audio manager</source>
        <translation>Audio-håndtering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="193"/>
        <source>Video</source>
        <translation>Videoen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="194"/>
        <source>Select video device</source>
        <translation>Velg videoapparat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="195"/>
        <source>Device</source>
        <translation>Enhet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="196"/>
        <source>Resolution</source>
        <translation>Avtale om utbygging av</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="197"/>
        <source>Select video resolution</source>
        <translation>Velg video oppløsning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="198"/>
        <source>Frames per second</source>
        <translation>Framar per sekund</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="199"/>
        <source>Select video frame rate (frames per second)</source>
        <translation>Velg video framrate (frames per sekund)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="200"/>
        <source>Enable hardware acceleration</source>
        <translation>Legg til høve for maskinverksakselering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="203"/>
        <source>Select screen sharing frame rate (frames per second)</source>
        <translation>Velg skjermdelingsrammar (rammar per sekund)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="204"/>
        <source>no video</source>
        <translation>ingen video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="209"/>
        <source>Back up account here</source>
        <translation>Bakset opp konto her</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="210"/>
        <source>Back up account</source>
        <translation>Baksekonto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="284"/>
        <source>Unavailable</source>
        <translation>Utilgjengeleg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="311"/>
        <source>Turn off sharing</source>
        <translation>Slukk av deling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="312"/>
        <source>Stop location sharing in this conversation (%1)</source>
        <translation>Stopp med å dele plassering i denne samtalen (% 1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="335"/>
        <source>Hide chat</source>
        <translation>Skjul chat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="344"/>
        <source>Back to Call</source>
        <translation>Tilbake til ring</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="353"/>
        <source>Scroll to end of conversation</source>
        <translation>Rolla til slutten av samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="371"/>
        <source>You can choose a username to help others more easily find and reach you on Jami.</source>
        <translation>Du kan velja ein brukernavn for å hjelpa andre til å finne deg på Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="378"/>
        <source>Are you sure you would like to join Jami without a username?
If yes, only a randomly generated 40-character identifier will be assigned to this account.</source>
        <translation>Er du sikker på at du vil bli med på Jami utan brukernavn? Dersom ja, vert berre ein tilfeldig generert 40-tekstidentifikator tildelt denne kontoen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="379"/>
        <source>- 32 characters maximum
- Alphabetical characters (A to Z and a to z)
- Numeric characters (0 to 9)
- Special characters allowed: dash (-)</source>
        <translation>- 32 teiknastokkar - Alfabetiske teikn (A til Z og a til z) - Numeriske teikn (0 til 9) - Særleg teikn tillatt: strekk (-)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="385"/>
        <source>Your account will be created and stored locally.</source>
        <translation>Konta din vert oppretta og lagra lokalt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="386"/>
        <source>Choosing a username is recommended, and a chosen username CANNOT be changed later.</source>
        <translation>Det er tilrådeleg å velja ein brukernavn, og det valde brukernavn kan IKKE endras seinare.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="387"/>
        <source>Encrypting your account with a password is optional, and if the password is lost it CANNOT be recovered later.</source>
        <translation>Kryptet av kontoen din med ein passord er frivillig, og viss passordet går tapt kan det IKKE gjenoppretta seinare.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="388"/>
        <source>Setting a profile picture and nickname is optional, and can also be changed later in the settings.</source>
        <translation>Ein profilbilde og ein nickname er frivillig og kan endrast seinare i stillingane.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="397"/>
        <source>TLS</source>
        <translation>TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="398"/>
        <source>UDP</source>
        <translation>UDP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="399"/>
        <source>Display Name</source>
        <translation>Vis namn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="403"/>
        <source>A chosen username can help to be found more easily on Jami.
If a username is not chosen, a randomly generated 40-character identifier will be assigned to this account as a username. It is more difficult to be found and reached with this identifier.</source>
        <translation>Ein vald brukernavn kan hjelpa til å finnast lettare på Jami. Dersom eit brukernavn ikkje vert valt, vert ein tilfeldig generert 40-tekstidentifikator tildelt denne kontoen som brukernavn. Det er vanskelegare å finna og nå med denne identifikatoren.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="406"/>
        <source>This Jami account exists only on this device.
The account will be lost if this device is lost or the application is uninstalled. It is recommended to make a backup of this account.</source>
        <translation>Dette Jami-kontoet finst berre på denne enna. Konta vert tapt dersom denne enna forsvinn eller applikasjonen blir avinstallert. Det er tilrådeleg å laga ein backup av denne kontoen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="421"/>
        <source>Encrypt account</source>
        <translation>Kryptekonto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="424"/>
        <source>Back up account to a .gz file</source>
        <translation>Bakset opp kontoen til ein.gz-fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="430"/>
        <source>This profile is only shared with this account's contacts.
The profile can be changed at all times from the account&apos;s settings.</source>
        <translation>Denne profilen vert berre delt med kontaktane til kontoen. Profilen kan endrast til alle tider frå kontoen settings.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="431"/>
        <source>Encrypt account with a password</source>
        <translation>Kryptera konto med passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="432"/>
        <source>A Jami account is created and stored locally only on this device, as an archive containing your account keys. Access to this archive can optionally be protected by a password.</source>
        <translation>Ein Jami-konto blir oppretta og lagra lokalt berre på denne enheten, som eit arkiv som inneholder konto nøklene dine.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="433"/>
        <source>Please note that if you lose your password, it CANNOT be recovered!</source>
        <translation>Legg merke til at viss du mista passordet ditt, kan det IKKE bli rekupera!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="459"/>
        <source>Would you really like to delete this account?</source>
        <translation>Vil du verkeleg sletta denne kontoen?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="460"/>
        <source>If your account has not been backed up or added to another device, your account and registered username will be IRREVOCABLY LOST.</source>
        <translation>Dersom kontot ditt ikkje er backup eller lagt til ein annan enhet, blir kontot og registrert brukernavn IRREVOCABLY LOST.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="471"/>
        <source>Dark</source>
        <translation>Dunkle</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="472"/>
        <source>Light</source>
        <translation>Ljus</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="480"/>
        <source>Include local video in recording</source>
        <translation>Legg med lokal video i opptaket</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="481"/>
        <source>Default settings</source>
        <translation>Standardinnstilling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="484"/>
        <source>Enable typing indicators</source>
        <translation>Legg til bruk for å skrive</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="485"/>
        <source>Send and receive typing indicators showing that a message is being typed.</source>
        <translation>Send og motta skrivingsindikator som viser at ein melding blir skrivd.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="486"/>
        <source>Show link preview in conversations</source>
        <translation>Visa link forhandsvisning i samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="508"/>
        <source>Delete file from device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="525"/>
        <source>Content access error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="526"/>
        <source>Content not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="589"/>
        <source>Enter account password</source>
        <translation>Skriv inn kontopassord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="590"/>
        <source>This account is password encrypted, enter the password to generate a PIN code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="592"/>
        <source>PIN expired</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="593"/>
        <source>On another device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="594"/>
        <source>Install and launch Jami, select &quot;Import from another device&quot; and scan the QR code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="595"/>
        <source>Link new device</source>
        <translation>Link nytt utstyr</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="596"/>
        <source>In Jami, scan QR code or manually enter the PIN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="597"/>
        <source>The PIN code is valid for: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="600"/>
        <source>Enter password</source>
        <translation>Skriv inn passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="602"/>
        <source>Enter account password to confirm the removal of this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="742"/>
        <source>Show less</source>
        <translation>Vis mindre</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="744"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="745"/>
        <source>Continue editing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="748"/>
        <source>Strikethrough</source>
        <translation>Gjennomstreking</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="754"/>
        <source>Unordered list</source>
        <translation>Usortert liste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="755"/>
        <source>Ordered list</source>
        <translation>Sortert liste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="758"/>
        <source>Press Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="759"/>
        <source>Press Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="772"/>
        <source>Select dedicated device for hosting future calls in this swarm. If not set, the host will be the device starting a call.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="773"/>
        <source>Select this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="774"/>
        <source>Select device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="833"/>
        <source>Appearance</source>
        <translation>Utseendet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="852"/>
        <source>Free and private sharing. &lt;a href=&quot;https://jami.net/donate/&quot;&gt;Donate&lt;/a&gt; to expand it.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="853"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="854"/>
        <source>If you enjoy using Jami and believe in our mission, would you make a donation?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="855"/>
        <source>Not now</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="856"/>
        <source>Enable donation campaign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="859"/>
        <source>Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="860"/>
        <source>Shift+Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="861"/>
        <source>Enter or Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="875"/>
        <source>View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="862"/>
        <source>Text formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="865"/>
        <source>Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="866"/>
        <source>Connecting TLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="867"/>
        <source>Connecting ICE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="868"/>
        <source>Connecting</source>
        <translation>Kopplande</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="869"/>
        <source>Waiting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="870"/>
        <source>Contact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="871"/>
        <source>Connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="872"/>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="873"/>
        <source>Copy all data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="874"/>
        <source>Remote: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="496"/>
        <source>Accept transfer limit (in Mb)</source>
        <translation>Ta imot overførselsgrens (i Mb)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="516"/>
        <source>A new version of Jami was found
Would you like to update now?</source>
        <translation>Ein ny versjon av Jami er funne. Vil du oppdatere no?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="539"/>
        <source>Save recordings to</source>
        <translation>Ta opp opptakene til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="540"/>
        <source>Save screenshots to</source>
        <translation>Legg opp skjermbilder til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="582"/>
        <source>Select &quot;Link another device&quot;</source>
        <translation>Velg &quot;Link ein annan enhet&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="609"/>
        <source>Choose a picture as your avatar</source>
        <translation>Velg ein foto som avatar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="685"/>
        <source>Share freely and privately with Jami</source>
        <translation>De deler fritt og privat med Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="711"/>
        <source>Unban</source>
        <translation>Unban</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="728"/>
        <source>Add</source>
        <translation>Legg til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="737"/>
        <source>more emojis</source>
        <translation>meir emojiar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="761"/>
        <source>Reply to</source>
        <translation>Svar på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="762"/>
        <source>In reply to</source>
        <translation>I svar på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="763"/>
        <source> replied to</source>
        <translation>svara på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="765"/>
        <source>Reply</source>
        <translation>Svar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="464"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="767"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="768"/>
        <source>Edited</source>
        <translation>Redigert</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="769"/>
        <source>Join call</source>
        <translation>Kom inn i samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="770"/>
        <source>A call is in progress. Do you want to join the call?</source>
        <translation>Ein samtale er på gang. Vil du bli med?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="771"/>
        <source>Current host for this swarm seems unreachable. Do you want to host the call?</source>
        <translation>No er det ikkje mulig å koma inn på den neste gjengen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="775"/>
        <source>Remove current device</source>
        <translation>Ta vekk strøma</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="776"/>
        <source>Host only this call</source>
        <translation>Ver vert berre vert vert vert vert vert vert</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="777"/>
        <source>Host this call</source>
        <translation>Ver vert vertande denne samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="778"/>
        <source>Make me the default host for future calls</source>
        <translation>Lat meg bli default-hosten for framtida samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="793"/>
        <source>Mute conversation</source>
        <translation>Taus samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="796"/>
        <source>Default host (calls)</source>
        <translation>Foreldre host (kallar)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="799"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="813"/>
        <source>Tip</source>
        <translation>Tipp</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="815"/>
        <source>Add a profile picture and nickname to complete your profile</source>
        <translation>Legg til ein profilbilde og ein nicknam for å fylla opp profilen din</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="36"/>
        <source>Start swarm</source>
        <translation>Start swarm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="37"/>
        <source>Create swarm</source>
        <translation>Skapa sjarm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="176"/>
        <source>Call settings</source>
        <translation>Innstillingar på samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="115"/>
        <source>Disable secure dialog check for incoming TLS data</source>
        <translation>Deaktivera trygg dialogkontroll for innkommende TLS-data</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="146"/>
        <source>Video codecs</source>
        <translation>Video codecs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="147"/>
        <source>Audio codecs</source>
        <translation>Lydkodekkar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="150"/>
        <source>Name server</source>
        <translation>Namnsserver</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="163"/>
        <source>OpenDHT configuration</source>
        <translation>OpenDHT-konfigurasjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="201"/>
        <source>Mirror local video</source>
        <translation>Speglar lokale videoar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="207"/>
        <source>Why should I back-up this account?</source>
        <translation>Kvifor skal eg backup denne kontoen?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="211"/>
        <source>Success</source>
        <translation>Framgang</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="212"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="213"/>
        <source>Jami archive files (*.gz)</source>
        <translation>Jami arkivfiller (*.gz)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="214"/>
        <source>All files (*)</source>
        <translation>Alle arkivar (*)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="217"/>
        <source>Reinstate as contact</source>
        <translation>Återinnfør som kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="218"/>
        <source>name</source>
        <translation>namn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="219"/>
        <source>Identifier</source>
        <translation>Identifikator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="222"/>
        <source>is recording</source>
        <translation>er å registrera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="223"/>
        <source>are recording</source>
        <translation>registrerar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="224"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="716"/>
        <source>Mute</source>
        <translation>Stum</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="225"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="717"/>
        <source>Unmute</source>
        <translation>Ustemmeleg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="226"/>
        <source>Pause call</source>
        <translation>Pause ring</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="227"/>
        <source>Resume call</source>
        <translation>Fortsett samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="228"/>
        <source>Mute camera</source>
        <translation>Stum kamera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="229"/>
        <source>Unmute camera</source>
        <translation>Unmuted kamera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="230"/>
        <source>Add participant</source>
        <translation>Legg til deltakar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="231"/>
        <source>Add participants</source>
        <translation>Legg til deltakarar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="232"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="177"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="233"/>
        <source>Chat</source>
        <translation>Chatta med</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="174"/>
        <source>Manage account</source>
        <translation>Ta styr på kontoen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="175"/>
        <source>Linked devices</source>
        <translation>Kopplande utstyr</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="178"/>
        <source>Advanced settings</source>
        <translation>Forfråte innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="179"/>
        <source>Audio and Video</source>
        <translation>Audio og video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="190"/>
        <source>Sound test</source>
        <translation>Lydtest</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="202"/>
        <source>Screen sharing</source>
        <translation>Skjermdeling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="208"/>
        <source>Your account only exists on this device. If you lose your device or uninstall the application, your account will be deleted and CANNOT be recovered. You can &lt;a href=&apos;blank&apos;&gt; back up your account &lt;/a&gt; now or later (in the Account Settings).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="234"/>
        <source>More options</source>
        <translation>Meir alternativ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="235"/>
        <source>Mosaic</source>
        <translation>Mosaik</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="236"/>
        <source>Participant is still muted on their device</source>
        <translation>Medlempen er berre stille på maskinen deira.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="237"/>
        <source>You are still muted on your device</source>
        <translation>Du er berre still på maskinen din</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="238"/>
        <source>You are still muted by moderator</source>
        <translation>Du er berre stilt opp av moderatoren</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="239"/>
        <source>You are muted by a moderator</source>
        <translation>Du er toned av ein moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="240"/>
        <source>Moderator</source>
        <translation>Moderatør</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="241"/>
        <source>Host</source>
        <translation>Vertskap</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="242"/>
        <source>Local and Moderator muted</source>
        <translation>Lokal og moderator stilla</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="243"/>
        <source>Moderator muted</source>
        <translation>Moderatør stum</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="244"/>
        <source>Not muted</source>
        <translation>Ikkje stilt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="245"/>
        <source>On the side</source>
        <translation>På sida</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="246"/>
        <source>On the top</source>
        <translation>På toppen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="247"/>
        <source>Hide self</source>
        <translation>Skjul deg sjølv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="248"/>
        <source>Hide spectators</source>
        <translation>Skjul sjåarar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="251"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="554"/>
        <source>Copy</source>
        <translation>Kopiert</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="252"/>
        <source>Share</source>
        <translation>Deling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="253"/>
        <source>Cut</source>
        <translation>Skjer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="254"/>
        <source>Paste</source>
        <translation>Paste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="257"/>
        <source>Start video call</source>
        <translation>Start video samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="258"/>
        <source>Start audio call</source>
        <translation>Start lydsamtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="259"/>
        <source>Clear conversation</source>
        <translation>Klar samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="260"/>
        <source>Confirm action</source>
        <translation>Bekrefting av handlinga</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="261"/>
        <source>Remove conversation</source>
        <translation>Ta vekk samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="262"/>
        <source>Would you really like to remove this conversation?</source>
        <translation>Vil du verkeleg fjerne dette samtalet?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="263"/>
        <source>Would you really like to block this conversation?</source>
        <translation>Vil du verkeleg blokkerte denne samtalen?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="264"/>
        <source>Remove contact</source>
        <translation>Ta bort kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="265"/>
        <source>Block contact</source>
        <translation>Blockkontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="266"/>
        <source>Conversation details</source>
        <translation>Detaljer om samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="267"/>
        <source>Contact details</source>
        <translation>Kontaktinformasjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="270"/>
        <source>Sip input panel</source>
        <translation>SIP inputpanel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="271"/>
        <source>Transfer call</source>
        <translation>Overføringskall</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="272"/>
        <source>Stop recording</source>
        <translation>Sluta å opptre</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="273"/>
        <source>Start recording</source>
        <translation>Start med å oppteken</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="274"/>
        <source>View full screen</source>
        <translation>Vis fullskjerm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="275"/>
        <source>Share screen</source>
        <translation>Del skjerm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="276"/>
        <source>Share window</source>
        <translation>Del vindauge</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="277"/>
        <source>Stop sharing screen or file</source>
        <translation>Sluta å dela skjerm eller fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="278"/>
        <source>Share screen area</source>
        <translation>Del skjermområde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="279"/>
        <source>Share file</source>
        <translation>Del filen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="280"/>
        <source>Select sharing method</source>
        <translation>Velg delingsmetode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="281"/>
        <source>View plugin</source>
        <translation>Visa plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="282"/>
        <source>Advanced information</source>
        <translation>Forfråttande informasjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="283"/>
        <source>No video device</source>
        <translation>Ingen videoapparat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="285"/>
        <source>Lower hand</source>
        <translation>Nedre hand</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="286"/>
        <source>Raise hand</source>
        <translation>Rekk opp handa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="287"/>
        <source>Layout settings</source>
        <translation>Settingar på utbygginga</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="288"/>
        <source>Take tile screenshot</source>
        <translation>Ta skjermbild av fløy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="289"/>
        <source>Screenshot saved to %1</source>
        <translation>Skjermskjerm lagra til % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="290"/>
        <source>File saved to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="293"/>
        <source>Renderers information</source>
        <translation>Informasjon til utleivarar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="294"/>
        <source>Call information</source>
        <translation>Informasjon om samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="295"/>
        <source>Peer number</source>
        <translation>Samlingsanummer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="296"/>
        <source>Call id</source>
        <translation>Ring ID</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="297"/>
        <source>Sockets</source>
        <translation>Socketar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="298"/>
        <source>Video codec</source>
        <translation>Video codec</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="299"/>
        <source>Hardware acceleration</source>
        <translation>Hardware akselerasjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="300"/>
        <source>Video bitrate</source>
        <translation>Video bitrate</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="301"/>
        <source>Audio codec</source>
        <translation>Audiokodec</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="302"/>
        <source>Renderer id</source>
        <translation>Kode til utlevering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="303"/>
        <source>Fps</source>
        <translation>Fps</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="306"/>
        <source>Share location</source>
        <translation>Del plassering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="307"/>
        <source>Stop sharing</source>
        <translation>Sluta å dela</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="308"/>
        <source>Your precise location could not be determined.
In Device Settings, please turn on &quot;Location Services&quot;.
Other participants&apos; location can still be received.</source>
        <translation>Det var ikkje greitt å finne det. I Settings, legg på &quot;Location Services&quot;. Andre deltakarar kan likevel bli lokaliserte.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="309"/>
        <source>Your precise location could not be determined. Please check your Internet connection.</source>
        <translation>Det var ikkje greitt å finne det, så sjekk at du har Internett-tilkobling.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="310"/>
        <source>Turn off location sharing</source>
        <translation>Slukk av deling av plassering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="313"/>
        <source>Location is shared in several conversations</source>
        <translation>Lokalitet vert delt i fleire samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="314"/>
        <source>Pin map to be able to share location or to turn off location in specific conversations</source>
        <translation>Pin map for å kunne dele plassering eller slå av plassering i bestemte samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="315"/>
        <source>Location is shared in several conversations, click to choose how to turn off location sharing</source>
        <translation>Lokasjon blir delt i fleire samtal, klikk for å velja korleis du slukkar av å dela med same ligging</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="316"/>
        <source>Share location to participants of this conversation (%1)</source>
        <translation>Dela plassering til deltakarar i samtalen (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="318"/>
        <source>Reduce</source>
        <translation>Reduksjon av</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="319"/>
        <source>Drag</source>
        <translation>Trekk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="320"/>
        <source>Center</source>
        <translation>Centrum</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="322"/>
        <source>Unpin</source>
        <translation>Unpin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="323"/>
        <source>Pin</source>
        <translation>Pin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="324"/>
        <source>Position share duration</source>
        <translation>Varighet av posisjonshandel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="325"/>
        <source>Location sharing</source>
        <translation>Støddsdeileg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="326"/>
        <source>Unlimited</source>
        <translation>Uleimde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="327"/>
        <source>1 min</source>
        <translation>1 min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="328"/>
        <source>%1h%2min</source>
        <translation>% 1h% 2min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="329"/>
        <source>%1h</source>
        <translation>% 1h</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="330"/>
        <source>%1min%2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="331"/>
        <source>%1min</source>
        <translation>%1 min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="332"/>
        <source>%sec</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="336"/>
        <source>Place audio call</source>
        <translation>Legg ein lydsamtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="337"/>
        <source>Place video call</source>
        <translation>Legg videoanrop</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="338"/>
        <source>Show available plugins</source>
        <translation>Visa tilgjengelege plugins</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="339"/>
        <source>Add to conversations</source>
        <translation>Legg til samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="340"/>
        <source>This is the error from the backend: %0</source>
        <translation>Dette er feil frå bakend: %0</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="341"/>
        <source>The account is disabled</source>
        <translation>Konta er deaktivert</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="342"/>
        <source>No network connectivity</source>
        <translation>Ingen nettverksanslutning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="343"/>
        <source>Deleted message</source>
        <translation>Mifte slettet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="347"/>
        <source>Jump to</source>
        <translation>Hoppa opp til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="348"/>
        <source>Messages</source>
        <translation>Meldingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="349"/>
        <source>Files</source>
        <translation>Dateiar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="350"/>
        <source>Search</source>
        <translation>Søk etter</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="354"/>
        <source>{} is typing…</source>
        <translation>Han skriv...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="355"/>
        <source>{} are typing…</source>
        <translation>Dei skriv...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="356"/>
        <source>Several people are typing…</source>
        <translation>Nokre menneske skriv inn...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="357"/>
        <source> and </source>
        <translation>og </translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="360"/>
        <source>Enter the Jami Account Management Server (JAMS) URL</source>
        <translation>Skriv inn URLen til Jami Account Management Server (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="361"/>
        <source>Jami Account Management Server URL</source>
        <translation>Jami Account Management Server URL</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="362"/>
        <source>Enter JAMS credentials</source>
        <translation>Skriv inn JAMS-kredensjonal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="363"/>
        <source>Connect</source>
        <translation>Knytta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="364"/>
        <source>Creating account…</source>
        <translation>Eg skaper ein konto...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="367"/>
        <source>Choose name</source>
        <translation>Velg namn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="368"/>
        <source>Choose username</source>
        <translation>Velg brukernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="369"/>
        <source>Choose a username</source>
        <translation>Velg ein brukernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="428"/>
        <source>Encrypt account with password</source>
        <translation>Kryptera konto med passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="370"/>
        <source>Confirm password</source>
        <translation>Bekreft passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="372"/>
        <source>Choose a name for your rendezvous point</source>
        <translation>Velg eit namn til møtepunkten din</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="373"/>
        <source>Choose a name</source>
        <translation>Velg eit namn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="374"/>
        <source>Invalid name</source>
        <translation>Ugyldig namn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="375"/>
        <source>Invalid username</source>
        <translation>Utilsett brukernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="376"/>
        <source>Name already taken</source>
        <translation>Namn som er tagt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="377"/>
        <source>Username already taken</source>
        <translation>Nøvn som er tekne</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="382"/>
        <source>Good to know</source>
        <translation>Det er godt å vita</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="383"/>
        <source>Local</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="384"/>
        <source>Encrypt</source>
        <translation>Kryptografert</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="391"/>
        <source>SIP account</source>
        <translation>SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="392"/>
        <source>Proxy</source>
        <translation>Styrkemann</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="393"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="394"/>
        <source>Configure an existing SIP account</source>
        <translation>Konfigurera ein eksisterende SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="395"/>
        <source>Personalize account</source>
        <translation>Personleggjøre konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="396"/>
        <source>Add SIP account</source>
        <translation>Legg til SIP konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="402"/>
        <source>Your profile is only shared with your contacts.
Your picture and your nickname can be changed at all time in the settings of your account.</source>
        <translation>Profilen din blir berre delt med kontaktpersonane dine. Bilda og kallnavnet kan endrast til alle tider i innstillingane på kontoen din.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="404"/>
        <source>Your Jami account is registered only on this device as an archive containing the keys of your account. Access to this archive can be protected by a password.</source>
        <translation>Konta om Jami er registrert berre på denne enheten som eit arkiv som inneholder nøklene til kontoen din. Tilgang til dette arkivet kan verna med ein passord.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="405"/>
        <source>Backup account</source>
        <translation>Baksekonto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="407"/>
        <source>Delete your account</source>
        <translation>Slette kontoen din</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="408"/>
        <source>If your account has not been backed up or added to another device, your account and registered name will be irrevocably lost.</source>
        <translation>Dersom kontot ditt ikkje er blitt backupert eller lagt til ein annan enhet, vert kontot og registrert namn uigenkalleleg tapt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="409"/>
        <source>List of the devices that are linked to this account:</source>
        <translation>Liste over dei utstyr som er knytt til denne kontoen:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="410"/>
        <source>This device</source>
        <translation>Denne einheten</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="411"/>
        <source>Other linked devices</source>
        <translation>Andre knytte enheter</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="414"/>
        <source>Backup successful</source>
        <translation>Backup vellykket</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="415"/>
        <source>Backup failed</source>
        <translation>Backupp mislykt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="416"/>
        <source>Password changed successfully</source>
        <translation>Passordet endra seg framgångsrikt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="417"/>
        <source>Password change failed</source>
        <translation>Passordskifte mislyktes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="418"/>
        <source>Password set successfully</source>
        <translation>Passord sett opp med suksess</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="419"/>
        <source>Password set failed</source>
        <translation>Passordsett mislykt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="420"/>
        <source>Change password</source>
        <translation>Endra passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="434"/>
        <source>Enter a nickname, surname…</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="435"/>
        <source>Use this account on other devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="436"/>
        <source>This account is created and stored locally, if you want to use it on another device you have to link the new device to this account.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="465"/>
        <source>Device name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="550"/>
        <source>Markdown</source>
        <translation>Markdown</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="617"/>
        <source>Auto update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="618"/>
        <source>Disable all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="619"/>
        <source>Installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="620"/>
        <source>Install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="621"/>
        <source>Installing</source>
        <translation>Installering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="622"/>
        <source>Install manually</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="623"/>
        <source>Install an extension directly from your device.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="624"/>
        <source>Available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="625"/>
        <source>Plugins store is not available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="627"/>
        <source>Installation failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="628"/>
        <source>The installation of the plugin failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="646"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="647"/>
        <source>Last update %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="648"/>
        <source>By %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="649"/>
        <source>Proposed by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="652"/>
        <source>More information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="739"/>
        <source>Audio message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="740"/>
        <source>Video message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="741"/>
        <source>Show more</source>
        <translation>Vis meir</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="746"/>
        <source>Bold</source>
        <translation>Feit</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="747"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="749"/>
        <source>Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="750"/>
        <source>Heading</source>
        <translation>Overskrift</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="751"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="752"/>
        <source>Code</source>
        <translation>Kode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="753"/>
        <source>Quote</source>
        <translation>Citat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="756"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="757"/>
        <source>Hide formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="824"/>
        <source>Share your Jami identifier in order to be contacted more easily!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="825"/>
        <source>Jami identity</source>
        <translation>identiteten til Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="826"/>
        <source>Show fingerprint</source>
        <translation>Vis fingeravtrykk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="827"/>
        <source>Show registered name</source>
        <translation>Visa registrert namn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="830"/>
        <source>Enabling your account allows you to be contacted on Jami</source>
        <translation>Med kontoen din kan du bli kontaktad på Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="836"/>
        <source>Experimental</source>
        <translation>Eksperimentell</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="839"/>
        <source>Ringtone</source>
        <translation>Ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="842"/>
        <source>Rendezvous point</source>
        <translation>Rendezvouspunkt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="845"/>
        <source>Moderation</source>
        <translation>Medlemskap</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="848"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="849"/>
        <source>Text zoom level</source>
        <translation>Tingde for å zooma tekst</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="422"/>
        <source>Set a password</source>
        <translation>Set ein passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="423"/>
        <source>Change current password</source>
        <translation>Endra gjeldende passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="425"/>
        <source>Display advanced settings</source>
        <translation>Visa avanserte innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="426"/>
        <source>Hide advanced settings</source>
        <translation>Skjåda avanserte innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="173"/>
        <source>Enable account</source>
        <translation>Å aktivera konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="427"/>
        <source>Advanced account settings</source>
        <translation>Forfråte kontoinnstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="429"/>
        <source>Customize profile</source>
        <translation>Anpassar profil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="440"/>
        <source>Set username</source>
        <translation>Sett brukernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="441"/>
        <source>Registering name</source>
        <translation>Namn til registrering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="444"/>
        <source>Identity</source>
        <translation>Identitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="447"/>
        <source>Link a new device to this account</source>
        <translation>Legg ein ny enhet til denne kontoen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="448"/>
        <source>Exporting account…</source>
        <translation>Eksportkontoen...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="449"/>
        <source>Remove Device</source>
        <translation>Ta av utstyret</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="450"/>
        <source>Are you sure you wish to remove this device?</source>
        <translation>Er du sikker på du vil fjerne denne enna?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="451"/>
        <source>Your PIN is:</source>
        <translation>PIN-numret ditt er:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="452"/>
        <source>Error connecting to the network.
Please try again later.</source>
        <translation>Feil ved tilkobling til nettverket. Vennligst prøv igjen seinare.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="455"/>
        <source>Banned</source>
        <translation>Banta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="456"/>
        <source>Banned contacts</source>
        <translation>Forbudt kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="467"/>
        <source>Device Id</source>
        <translation>Enhet ID</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="470"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="473"/>
        <source>Select a folder</source>
        <translation>Velg ein mappe</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="474"/>
        <source>Enable notifications</source>
        <translation>Legg til bruk meldingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="478"/>
        <source>Launch at startup</source>
        <translation>Launinga ved start</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="479"/>
        <source>Choose download directory</source>
        <translation>Velg nedlastingskreds</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="487"/>
        <source>Preview requires downloading content from third-party servers.</source>
        <translation>Forhandsvisning krev å laste ned innhald frå tredjepartsserver.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="489"/>
        <source>User interface language</source>
        <translation>Brukergrensesnittspråk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="490"/>
        <source>Vertical view</source>
        <translation>Loddrett visning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="491"/>
        <source>Horizontal view</source>
        <translation>Vannrett visning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="494"/>
        <source>File transfer</source>
        <translation>File overføring</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="495"/>
        <source>Automatically accept incoming files</source>
        <translation>Ta automatisk imot innkommende filer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="497"/>
        <source>in MB, 0 = unlimited</source>
        <translation>i MB, 0 = ubegrenset</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="500"/>
        <source>Register</source>
        <translation>Registrer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="501"/>
        <source>Incorrect password</source>
        <translation>Feil passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="502"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="519"/>
        <source>Network error</source>
        <translation>Netverksfeil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="503"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="527"/>
        <source>Something went wrong</source>
        <translation>Nokre ting gjekk feil.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="506"/>
        <source>Save file</source>
        <translation>Ta opp filen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="507"/>
        <source>Open location</source>
        <translation>Opne plassering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="764"/>
        <source>Me</source>
        <translation>Eg er berre ein</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="511"/>
        <source>Install beta version</source>
        <translation>Installera betaversjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="512"/>
        <source>Check for updates now</source>
        <translation>Sjå etter oppdateringar no</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="513"/>
        <source>Enable/Disable automatic updates</source>
        <translation>Å aktivera/deaktivera automatisk oppdateringar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="514"/>
        <source>Updates</source>
        <translation>Oppdateringar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="515"/>
        <source>Update</source>
        <translation>Oppdatering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="517"/>
        <source>No new version of Jami was found</source>
        <translation>Ingen ny versjon av Jami blei funne</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="518"/>
        <source>An error occured when checking for a new version</source>
        <translation>Feil kom opp medan du sjekkar etter ein ny versjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="520"/>
        <source>SSL error</source>
        <translation>SSL-feil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="521"/>
        <source>Installer download canceled</source>
        <translation>Nedlasting av installasjonsprogram kansellert</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="523"/>
        <source>This will uninstall your current Release version and you can always download the latest Release version on our website</source>
        <translation>Dette vil fjerne den aktuelle release-versjonen din og du kan alltid laste ned den siste release-versjonen på nettstaden vår</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="524"/>
        <source>Network disconnected</source>
        <translation>Nettrekk frå nettverket</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="530"/>
        <source>Troubleshoot</source>
        <translation>Forklar feil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="531"/>
        <source>Open logs</source>
        <translation>Åpne loggar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="532"/>
        <source>Get logs</source>
        <translation>Ta loggar.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="534"/>
        <source>(Experimental) Enable call support for swarm</source>
        <translation>(Experimentell) Legg til oppfordringa til swarm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="535"/>
        <source>This feature will enable call buttons in swarms with multiple participants.</source>
        <translation>Denne funksjonen vil gje høve til å kalla på knappar i swarmar med fleire deltakarar.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="538"/>
        <source>Quality</source>
        <translation>Kvalitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="542"/>
        <source>Always record calls</source>
        <translation>Alltid registrera samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="545"/>
        <source>Keyboard Shortcut Table</source>
        <translation>Tåle for tastatur</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="546"/>
        <source>Keyboard Shortcuts</source>
        <translation>Knappebordsgjørte</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="547"/>
        <source>Conversation</source>
        <translation>Samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="548"/>
        <source>Call</source>
        <translation>Ring</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="549"/>
        <source>Settings</source>
        <translation>Innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="553"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="555"/>
        <source>Report Bug</source>
        <translation>Rapportera feil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="556"/>
        <source>Clear</source>
        <translation>Åre klar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="557"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="705"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="558"/>
        <source>Copied to clipboard!</source>
        <translation>Kopiert på klippbrett!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="559"/>
        <source>Receive Logs</source>
        <translation>Ta imot loggar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="562"/>
        <source>Archive</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="563"/>
        <source>Open file</source>
        <translation>Åpne fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="566"/>
        <source>Generating account…</source>
        <translation>Det er ein konto som genererer...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="567"/>
        <source>Import from archive backup</source>
        <translation>Import frå backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="569"/>
        <source>Select archive file</source>
        <translation>Velg arkivfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="437"/>
        <source>Link device</source>
        <translation>Linketøy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="573"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="575"/>
        <source>A PIN is required to use an existing Jami account on this device.</source>
        <translation>Ein PIN er nødvendig for å bruke ein befintleg Jami-konto på denne enna.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="581"/>
        <source>Choose the account to link</source>
        <translation>Velg kontoen til å knyta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="587"/>
        <source>The PIN and the account password should be entered in your device within 10 minutes.</source>
        <translation>PIN-nummer og password til kontoen skal leggjast inn i appen innan ti minuttar.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="610"/>
        <source>Choose a picture</source>
        <translation>Velg ein foto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="790"/>
        <source>Contact&apos;s name</source>
        <translation>Namn på kontaktpersonen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="805"/>
        <source>Reinstate member</source>
        <translation>Åter innlegg medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="819"/>
        <source>Delete message</source>
        <translation>Ta bort meldinga</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="820"/>
        <source>*(Deleted Message)*</source>
        <translation>*(Message slettet) *</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="821"/>
        <source>Edit message</source>
        <translation>Redigera melding</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="321"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="588"/>
        <source>Close</source>
        <translation>Nær</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="541"/>
        <source>Call recording</source>
        <translation>Opprinneleg innføring av samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="570"/>
        <source>If the account is encrypted with a password, please fill the following field.</source>
        <translation>Dersom kontoen er kryptert med ein passord, bed å fylla i feltet som følger.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="574"/>
        <source>Enter the PIN code</source>
        <translation>Skriv inn PIN-koden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="576"/>
        <source>Step 01</source>
        <translation>Trinn 1:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="577"/>
        <source>Step 02</source>
        <translation>Trinn 2:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="578"/>
        <source>Step 03</source>
        <translation>Tredje trekk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="579"/>
        <source>Step 04</source>
        <translation>Trinn 04</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="580"/>
        <source>Go to the account management settings of a previous device</source>
        <translation>Gå til kontohanteringane på ein førare enhet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="583"/>
        <source>The PIN code will be available for 10 minutes</source>
        <translation>PIN-koden vert tilgjengeleg i 10 minuttar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="584"/>
        <source>Fill if the account is password-encrypted.</source>
        <translation>Fyll ut om kontoen er passordkrypta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="591"/>
        <source>Add Device</source>
        <translation>Legg til Enhet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="601"/>
        <source>Enter current password</source>
        <translation>Skriv inn gjeldende passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="603"/>
        <source>Enter new password</source>
        <translation>Skriv inn nytt passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="604"/>
        <source>Confirm new password</source>
        <translation>Bekreft nytt passord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="605"/>
        <source>Change</source>
        <translation>Endringar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="606"/>
        <source>Export</source>
        <translation>Utføringa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="611"/>
        <source>Import avatar from image file</source>
        <translation>Import avatar frå bildefile</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="612"/>
        <source>Clear avatar image</source>
        <translation>Klar avatarbilde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="613"/>
        <source>Take photo</source>
        <translation>Ta eit foto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="626"/>
        <source>Preferences</source>
        <translation>Preferanser</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="629"/>
        <source>Reset</source>
        <translation>Omsetning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="630"/>
        <source>Uninstall</source>
        <translation>Uninstallere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="631"/>
        <source>Reset Preferences</source>
        <translation>Restaretting</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="632"/>
        <source>Select a plugin to install</source>
        <translation>Velg ein plugin til å installere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="633"/>
        <source>Uninstall plugin</source>
        <translation>Uninstall plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="634"/>
        <source>Are you sure you wish to reset %1 preferences?</source>
        <translation>Er du sikker på du vil tilbakestilla %1 preferanse?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="635"/>
        <source>Are you sure you wish to uninstall %1?</source>
        <translation>Er du sikker på du vil uninstallere %1?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="636"/>
        <source>Go back to plugins list</source>
        <translation>Gå tilbake til plugins lista</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="637"/>
        <source>Select a file</source>
        <translation>Velg ein fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="119"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="638"/>
        <source>Select</source>
        <translation>Velg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="639"/>
        <source>Choose image file</source>
        <translation>Velg bildefil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="640"/>
        <source>Plugin Files (*.jpl)</source>
        <translation>Plugin-filer (*.jpl)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="641"/>
        <source>Load/Unload</source>
        <translation>Ladd/utlasting</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="642"/>
        <source>Select An Image to %1</source>
        <translation>Velg ein bilde til % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="643"/>
        <source>Edit preference</source>
        <translation>Redigera preferanse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="644"/>
        <source>On/Off</source>
        <translation>På/av</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="645"/>
        <source>Choose Plugin</source>
        <translation>Velg Plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="651"/>
        <source>Information</source>
        <translation>Informasjon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="653"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="656"/>
        <source>Enter the account password to confirm the removal of this device</source>
        <translation>Skriv inn password til kontoen for å bekrefta at denne tilleiken er fjerna</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="659"/>
        <source>Select a screen to share</source>
        <translation>Velg en skjerm å dele</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="660"/>
        <source>Select a window to share</source>
        <translation>Velg ein venstert som skal deles</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="661"/>
        <source>All Screens</source>
        <translation>Alle skjermar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="662"/>
        <source>Screens</source>
        <translation>Skjerm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="663"/>
        <source>Windows</source>
        <translation>Finduar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="664"/>
        <source>Screen %1</source>
        <translation>Skjerm % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="667"/>
        <source>QR code</source>
        <translation>QR-kode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="670"/>
        <source>Link this device to an existing account</source>
        <translation>Link dette til ein tilgjengeleg konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="671"/>
        <source>Import from another device</source>
        <translation>Import frå ein annan enhet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="672"/>
        <source>Import from an archive backup</source>
        <translation>Import frå ein backup arkiv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="673"/>
        <source>Advanced features</source>
        <translation>Utfordrande funksjoner</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="674"/>
        <source>Show advanced features</source>
        <translation>Visa avanserte funksjonar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="675"/>
        <source>Hide advanced features</source>
        <translation>Skjåda avanserte funksjoner</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="676"/>
        <source>Connect to a JAMS server</source>
        <translation>Knytte til ein JAMS server</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="677"/>
        <source>Create account from Jami Account Management Server (JAMS)</source>
        <translation>Skapa konto frå Jami Account Management Server (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="678"/>
        <source>Configure a SIP account</source>
        <translation>Konfigurer ein SIP konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="679"/>
        <source>Error while creating your account. Check your credentials.</source>
        <translation>Feilen under opprettinga av kontoen din. Kontroller legitimasjonane dine.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="680"/>
        <source>Create a rendezvous point</source>
        <translation>Skapa ein møtepunkt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="681"/>
        <source>Join Jami</source>
        <translation>Kom til Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="682"/>
        <source>Create new Jami account</source>
        <translation>Skapa nytt Jami-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="683"/>
        <source>Create new SIP account</source>
        <translation>Skapa nytt SIP konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="684"/>
        <source>About Jami</source>
        <translation>Om Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="686"/>
        <source>I already have an account</source>
        <translation>Eg har allereie ein konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="687"/>
        <source>Use existing Jami account</source>
        <translation>Bruk tilgjengeleg Jami konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="688"/>
        <source>Welcome to Jami</source>
        <translation>Velkommen til Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="691"/>
        <source>Clear Text</source>
        <translation>Klart tekst</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="692"/>
        <source>Conversations</source>
        <translation>Samtal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="693"/>
        <source>Search Results</source>
        <translation>Søkresultatar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="696"/>
        <source>Decline contact request</source>
        <translation>Avsenda kontaktbevis</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="697"/>
        <source>Accept contact request</source>
        <translation>Ta imot kontaktbeviset</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="700"/>
        <source>Automatically check for updates</source>
        <translation>Kontroller automatisk for oppdateringar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="703"/>
        <source>Ok</source>
        <translation>Ok, det er rett.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="463"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="704"/>
        <source>Save</source>
        <translation>Ta vare på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="706"/>
        <source>Upgrade</source>
        <translation>Oppgradering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="707"/>
        <source>Later</source>
        <translation>Seinare</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="708"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="710"/>
        <source>Block</source>
        <translation>Block</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="714"/>
        <source>Set moderator</source>
        <translation>Set moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="715"/>
        <source>Unset moderator</source>
        <translation>Unset moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="317"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="718"/>
        <source>Maximize</source>
        <translation>Maksimalgjøre</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="719"/>
        <source>Minimize</source>
        <translation>Minimer det</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="720"/>
        <source>Hangup</source>
        <translation>Henga opp</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="721"/>
        <source>Local muted</source>
        <translation>Lokalt tyst</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="724"/>
        <source>Default moderators</source>
        <translation>Standardmoderatorar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="725"/>
        <source>Enable local moderators</source>
        <translation>Legg til lokale moderatorar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="726"/>
        <source>Make all participants moderators</source>
        <translation>Lat alle deltakarane vera moderatorar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="727"/>
        <source>Add default moderator</source>
        <translation>Legg til default moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="729"/>
        <source>Remove default moderator</source>
        <translation>Ta vekk standard moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="736"/>
        <source>Add emoji</source>
        <translation>Legg til emoji</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="738"/>
        <source>Send file</source>
        <translation>Sende fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="760"/>
        <source>Send</source>
        <translation>Send inn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="466"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="709"/>
        <source>Remove</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="41"/>
        <source>Migrate conversation</source>
        <translation>Ta tiltal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="475"/>
        <source>Show notifications</source>
        <translation>Visa meldingar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="476"/>
        <source>Minimize on close</source>
        <translation>Minimer på nær</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="477"/>
        <source>Run at system startup</source>
        <translation>Løy ved systemstart</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="564"/>
        <source>Create account from backup</source>
        <translation>Skapa konto frå backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="565"/>
        <source>Restore account from backup</source>
        <translation>Ta kontoen tilbake frå backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="568"/>
        <source>Import Jami account from local archive file.</source>
        <translation>Importar Jami-kontoen frå lokal arkivfil.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="614"/>
        <source>Image Files (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</source>
        <translation>Bildfiler (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="766"/>
        <source>Write to %1</source>
        <translation>Skriv til %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="781"/>
        <source>%1 has sent you a request for a conversation.</source>
        <translation>%1 har sendt deg ein forespørsel om samtale.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="782"/>
        <source>Hello,
Would you like to join the conversation?</source>
        <translation>Helt godt, vil du koma med på samtalen?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="783"/>
        <source>You have accepted
the conversation request</source>
        <translation>Du har akseptert forespørselen om samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="784"/>
        <source>Waiting until %1
connects to synchronize the conversation.</source>
        <translation>Ventar til %1 samtalar for å synkronisere samtalen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="787"/>
        <source>%1 Members</source>
        <translation>%1 Leder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="788"/>
        <source>Member</source>
        <translation>Medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="789"/>
        <source>Swarm&apos;s name</source>
        <translation>Namnet på Swarm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="791"/>
        <source>Add a description</source>
        <translation>Legg til ei beskriving</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="794"/>
        <source>Ignore all notifications from this conversation</source>
        <translation>Ignorera alle meldingar frå denne samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="795"/>
        <source>Choose a color</source>
        <translation>Velg ein farge</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="797"/>
        <source>Leave conversation</source>
        <translation>Lat samtalen vera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="798"/>
        <source>Type of swarm</source>
        <translation>Type av svam</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="802"/>
        <source>Create the swarm</source>
        <translation>Skapa sjarmen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="803"/>
        <source>Go to conversation</source>
        <translation>Gå til samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="804"/>
        <source>Kick member</source>
        <translation>Kjak medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="806"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="807"/>
        <source>Invited</source>
        <translation>Besta inn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="808"/>
        <source>Remove member</source>
        <translation>Ta bort medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="809"/>
        <source>To:</source>
        <translation>Til:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="812"/>
        <source>Customize</source>
        <translation>Egeskrepa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="814"/>
        <source>Dismiss</source>
        <translation>Kast bort</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="816"/>
        <source>Your profile is only shared with your contacts</source>
        <translation>Profilen din blir berre delt med kontaktane dine</translation>
    </message>
</context>
<context>
    <name>KeyboardShortcutTable</name>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="41"/>
        <source>Open account list</source>
        <translation>Liste over oppgavne kontoar</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="45"/>
        <source>Focus conversations list</source>
        <translation>Fokus samtal liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="49"/>
        <source>Requests list</source>
        <translation>Liste over krav</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="53"/>
        <source>Previous conversation</source>
        <translation>Tidlegare samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="57"/>
        <source>Next conversation</source>
        <translation>Neste samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="61"/>
        <source>Search bar</source>
        <translation>Søkbalken</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="65"/>
        <source>Full screen</source>
        <translation>Fullskjerm</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="69"/>
        <source>Increase font size</source>
        <translation>Større skriftstørrelse</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="73"/>
        <source>Decrease font size</source>
        <translation>Reduser skriftstørringa</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="77"/>
        <source>Reset font size</source>
        <translation>Setti opp skriftstørringa</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="85"/>
        <source>Start an audio call</source>
        <translation>Start ein lydsamtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="89"/>
        <source>Start a video call</source>
        <translation>Start ein video samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="93"/>
        <source>Clear history</source>
        <translation>Klare historie</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="97"/>
        <source>Search messages/files</source>
        <translation>Søk meldingar/filer</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="101"/>
        <source>Block contact</source>
        <translation>Blockkontakt</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="105"/>
        <source>Remove conversation</source>
        <translation>Ta vekk samtal</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="109"/>
        <source>Accept contact request</source>
        <translation>Ta imot kontaktbeviset</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="113"/>
        <source>Edit last message</source>
        <translation>Redigera siste melding</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="117"/>
        <source>Cancel message edition</source>
        <translation>Avbryt meldingutgåva</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="125"/>
        <source>Media settings</source>
        <translation>Medieinnstilling</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="129"/>
        <source>General settings</source>
        <translation>Allmenn innstillingar</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="133"/>
        <source>Account settings</source>
        <translation>Innstillingar på konto</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="137"/>
        <source>Plugin settings</source>
        <translation>Innstillingar på plugin</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="141"/>
        <source>Open account creation wizard</source>
        <translation>Åpne konto skapingsvisjonar</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="146"/>
        <source>Open keyboard shortcut table</source>
        <translation>Åpne tastaturskjortbord</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="154"/>
        <source>Answer an incoming call</source>
        <translation>Ta imot ein innkommende samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="158"/>
        <source>End call</source>
        <translation>Avslutt samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="162"/>
        <source>Decline the call request</source>
        <translation>Kvit frå fråkalle</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="166"/>
        <source>Mute microphone</source>
        <translation>Stum mikrofon</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="170"/>
        <source>Stop camera</source>
        <translation>Stopp kamera</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="174"/>
        <source>Take tile screenshot</source>
        <translation>Ta skjermbild av fløy</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="182"/>
        <source>Bold</source>
        <translation>Feit</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="186"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="190"/>
        <source>Strikethrough</source>
        <translation>Gjennomstreking</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="194"/>
        <source>Heading</source>
        <translation>Overskrift</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="198"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="202"/>
        <source>Code</source>
        <translation>Kode</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="206"/>
        <source>Quote</source>
        <translation>Citat</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="210"/>
        <source>Unordered list</source>
        <translation>Usortert liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="214"/>
        <source>Ordered list</source>
        <translation>Sortert liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="218"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="222"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="375"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="377"/>
        <source>&amp;Quit</source>
        <translation>&amp;Kvit</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="383"/>
        <source>&amp;Show Jami</source>
        <translation>&amp;Show Jami</translation>
    </message>
</context>
<context>
    <name>PositionManager</name>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="414"/>
        <source>%1 is sharing their location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="419"/>
        <source>Location sharing</source>
        <translation>Støddsdeileg</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/libclient/qtwrapper/callmanager_wrap.h" line="458"/>
        <source>Me</source>
        <translation>Eg er berre ein</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="62"/>
        <source>Hold</source>
        <translation>Ta vare på</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="64"/>
        <source>Talking</source>
        <translation>Talande</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="66"/>
        <source>ERROR</source>
        <translation>FELD</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="68"/>
        <source>Incoming</source>
        <translation>Innkommande</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="70"/>
        <source>Calling</source>
        <translation>Ringing</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="373"/>
        <location filename="../src/libclient/api/call.h" line="72"/>
        <source>Connecting</source>
        <translation>Kopplande</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="74"/>
        <source>Searching</source>
        <translation>Søk etter</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="76"/>
        <source>Inactive</source>
        <translation>Uaktivt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="390"/>
        <location filename="../src/libclient/api/call.h" line="78"/>
        <location filename="../src/libclient/api/call.h" line="84"/>
        <source>Finished</source>
        <translation>Finne opp</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="80"/>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="82"/>
        <source>Peer busy</source>
        <translation>Samstarter er oppteken</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="86"/>
        <source>Communication established</source>
        <translation>Meddelelse etablert</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="211"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="994"/>
        <source>Invitation received</source>
        <translation>Innbyding motteke</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="260"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="208"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="992"/>
        <source>Contact added</source>
        <translation>Legg til kontakt</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="262"/>
        <source>%1 was invited to join</source>
        <translation>%1 blei invitert til å bli med</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="264"/>
        <source>%1 joined</source>
        <translation>%1 blei med</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="266"/>
        <source>%1 left</source>
        <translation>%1 forlét</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="268"/>
        <source>%1 was kicked</source>
        <translation>%1 vart sparka</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="270"/>
        <source>%1 was re-added</source>
        <translation>%1 blei att</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="359"/>
        <source>Private conversation created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="361"/>
        <source>Swarm created</source>
        <translation>Skarm skapte</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="174"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="180"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="990"/>
        <source>Outgoing call</source>
        <translation>Utgående samtale</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="176"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="186"/>
        <source>Incoming call</source>
        <translation>Innkommende samtal</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="182"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="988"/>
        <source>Missed outgoing call</source>
        <translation>Miska utgående samtale</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="188"/>
        <source>Missed incoming call</source>
        <translation>Miska innkomne samtal</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="198"/>
        <source>Join call</source>
        <translation>Kom inn i samtalen</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="213"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="996"/>
        <source>Invitation accepted</source>
        <translation>Invitasjon akseptert</translation>
    </message>
    <message>
        <location filename="../src/libclient/avmodel.cpp" line="391"/>
        <location filename="../src/libclient/avmodel.cpp" line="410"/>
        <source>default</source>
        <translation>misligholde</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="72"/>
        <source>Null</source>
        <translation>null</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="73"/>
        <source>Trying</source>
        <translation>Eg prøver å</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="74"/>
        <source>Ringing</source>
        <translation>Ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing ringing</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="75"/>
        <source>Being Forwarded</source>
        <translation>Dei blir sendt vidare</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="76"/>
        <source>Queued</source>
        <translation>I kø</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="77"/>
        <source>Progress</source>
        <translation>Framgang</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="78"/>
        <source>OK</source>
        <translation>OK, det er rett.</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="79"/>
        <source>Accepted</source>
        <translation>Aksept</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="80"/>
        <source>Multiple Choices</source>
        <translation>Fleire val</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="81"/>
        <source>Moved Permanently</source>
        <translation>Dei blir alltid bevege</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="82"/>
        <source>Moved Temporarily</source>
        <translation>Dei blei fortrinnleg flytta</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="83"/>
        <source>Use Proxy</source>
        <translation>Bruk proxy</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="84"/>
        <source>Alternative Service</source>
        <translation>Alternativt teneste</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="85"/>
        <source>Bad Request</source>
        <translation>Dåleg beining</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="86"/>
        <source>Unauthorized</source>
        <translation>Utiltekt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="87"/>
        <source>Payment Required</source>
        <translation>Betalingskrav</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="88"/>
        <source>Forbidden</source>
        <translation>Forbudt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="89"/>
        <source>Not Found</source>
        <translation>Ikkje funne</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="90"/>
        <source>Method Not Allowed</source>
        <translation>Metode er ikkje tillatt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="91"/>
        <location filename="../src/libclient/callmodel.cpp" line="111"/>
        <source>Not Acceptable</source>
        <translation>Det er ikkje akseptabelt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="92"/>
        <source>Proxy Authentication Required</source>
        <translation>Trenger ein fullmaktleg verifikasjon</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="93"/>
        <source>Request Timeout</source>
        <translation>Bið om tidsavgang</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="94"/>
        <source>Gone</source>
        <translation>Dei gjekk vekk.</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="95"/>
        <source>Request Entity Too Large</source>
        <translation>Kjøp opphav for stor</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="96"/>
        <source>Request URI Too Long</source>
        <translation>Be om URI for lang</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="97"/>
        <source>Unsupported Media Type</source>
        <translation>Ustødd mediatyp</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="98"/>
        <source>Unsupported URI Scheme</source>
        <translation>Ustødd URI-ordning</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="99"/>
        <source>Bad Extension</source>
        <translation>Dåleg utviding</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="100"/>
        <source>Extension Required</source>
        <translation>Utvidring nødvendig</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="101"/>
        <source>Session Timer Too Small</source>
        <translation>Sessionstimer er for liten</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="102"/>
        <source>Interval Too Brief</source>
        <translation>Stødd er for kort</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="103"/>
        <source>Temporarily Unavailable</source>
        <translation>Dei er ikkje til tid</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="104"/>
        <source>Call TSX Does Not Exist</source>
        <translation>Ring TSX ikkje eksisterer</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="105"/>
        <source>Loop Detected</source>
        <translation>Loop oppdaga</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="106"/>
        <source>Too Many Hops</source>
        <translation>For mange hoppar</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="107"/>
        <source>Address Incomplete</source>
        <translation>Adress Ufullstendig</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="108"/>
        <source>Ambiguous</source>
        <translation>Tvetydig</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="109"/>
        <source>Busy</source>
        <translation>Eg har fullt ut</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="110"/>
        <source>Request Terminated</source>
        <translation>Foreldring avslutta</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="112"/>
        <source>Bad Event</source>
        <translation>Dåleg hending</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="113"/>
        <source>Request Updated</source>
        <translation>Foreldring oppdatert</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="114"/>
        <source>Request Pending</source>
        <translation>Foreldring i vente</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="115"/>
        <source>Undecipherable</source>
        <translation>Udekrypterbart</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="116"/>
        <source>Internal Server Error</source>
        <translation>Intern server feil</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="117"/>
        <source>Not Implemented</source>
        <translation>Ikkje gjennomført</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="118"/>
        <source>Bad Gateway</source>
        <translation>Dåleg gateway</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="119"/>
        <source>Service Unavailable</source>
        <translation>Tjenesten er ikkje tilgjengeleg</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="120"/>
        <source>Server Timeout</source>
        <translation>Server Timeout</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="121"/>
        <source>Version Not Supported</source>
        <translation>Versión ikkje støtt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="122"/>
        <source>Message Too Large</source>
        <translation>For stor budskap</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="123"/>
        <source>Precondition Failure</source>
        <translation>Forutsettingsfeiling</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="124"/>
        <source>Busy Everywhere</source>
        <translation>Ver alle stades opptatt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="125"/>
        <source>Call Refused</source>
        <translation>Ring avvist</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="126"/>
        <source>Does Not Exist Anywhere</source>
        <translation>Ingen stad finst det</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="127"/>
        <source>Not Acceptable Anywhere</source>
        <translation>Ingen stad er akseptabelt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="375"/>
        <source>Accept</source>
        <translation>Ta imot</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="367"/>
        <source>Sending</source>
        <translation>Skifte</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="371"/>
        <source>Sent</source>
        <translation>Sending</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="380"/>
        <source>Unable to make contact</source>
        <translation>Ikkje kan få kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="384"/>
        <source>Waiting for contact</source>
        <translation>Ventar på kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="386"/>
        <source>Incoming transfer</source>
        <translation>Innkommende overføring</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="388"/>
        <source>Timed out waiting for contact</source>
        <translation>Timed ute i vente på kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="742"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="744"/>
        <source>Yesterday</source>
        <translation>I går</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="377"/>
        <source>Canceled</source>
        <translation>Avbrutt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="382"/>
        <source>Ongoing</source>
        <translation>Forlopande</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="723"/>
        <source>just now</source>
        <translation>No, akkurat no</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="369"/>
        <source>Failure</source>
        <translation>Utviklingsbrudd</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="109"/>
        <source>locationServicesError</source>
        <translation>LokaliseringTjenesteneFel</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="112"/>
        <source>locationServicesClosedError</source>
        <translation>lokasjonTjenestene LukkaFel</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="114"/>
        <source>locationServicesUnknownError</source>
        <translation>LokaliseringTjenesteneUkkjentFel</translation>
    </message>
    <message>
        <location filename="../src/libclient/conversationmodel.cpp" line="1166"/>
        <location filename="../src/libclient/conversationmodel.cpp" line="1179"/>
        <source>%1 (you)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SmartListModel</name>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="68"/>
        <location filename="../src/app/smartlistmodel.cpp" line="107"/>
        <location filename="../src/app/smartlistmodel.cpp" line="115"/>
        <location filename="../src/app/smartlistmodel.cpp" line="161"/>
        <location filename="../src/app/smartlistmodel.cpp" line="191"/>
        <location filename="../src/app/smartlistmodel.cpp" line="192"/>
        <source>Calls</source>
        <translation>Ringing</translation>
    </message>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="69"/>
        <location filename="../src/app/smartlistmodel.cpp" line="108"/>
        <location filename="../src/app/smartlistmodel.cpp" line="125"/>
        <location filename="../src/app/smartlistmodel.cpp" line="162"/>
        <location filename="../src/app/smartlistmodel.cpp" line="193"/>
        <location filename="../src/app/smartlistmodel.cpp" line="194"/>
        <source>Contacts</source>
        <translation>Kontakt</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../src/app/systemtray.cpp" line="216"/>
        <source>Answer</source>
        <translation>Svar:</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="217"/>
        <source>Decline</source>
        <translation>Nedgang</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="219"/>
        <source>Open conversation</source>
        <translation>Åpen samtale</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="221"/>
        <source>Accept</source>
        <translation>Ta imot</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="222"/>
        <source>Refuse</source>
        <translation>Avviser</translation>
    </message>
</context>
<context>
    <name>TipsModel</name>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="85"/>
        <source>Customize</source>
        <translation>Egeskrepa</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="88"/>
        <source>What does Jami mean?</source>
        <translation>Kva meiner Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="94"/>
        <source>What is the green dot next to my account?</source>
        <translation>Kva er den grøne punkten ved sidan kontoen min?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="90"/>
        <source>The choice of the name Jami was inspired by the Swahili word &apos;jamii&apos;, which means &apos;community&apos; as a noun and &apos;together&apos; as an adverb.</source>
        <translation>Velginga av namnet Jami var inspirert av swahili ordet &apos;jamii&apos;, som tyder&apos;samfunn&apos; som eit navne og&apos;sammen&apos; som eit adverb.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="81"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="86"/>
        <source>Backup account</source>
        <translation>Baksekonto</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="96"/>
        <source>A red dot means that your account is disconnected from the network; it turns green when it&apos;s connected.</source>
        <translation>Ein raud dot tyder at kontoen din er avkoppla frå nettverket; det vert grønt når det er tilslutte.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="101"/>
        <source>Why should I back up my account?</source>
        <translation>Kvifor skal eg backup kontoen min?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="103"/>
        <source>Jami is distributed and your account is only stored locally on your device. If you lose your password or your local account data, you WILL NOT be able to recover your account if you did not back it up earlier.</source>
        <translation>Jami vert distribuert og kontoen din vert berre lagra lokalt på einheten din. Dersom du mista passordet ditt eller data om den lokale kontoen, kan du IKKE gjenoppretta kontoen din om du ikkje har lagra den opp tidlegare.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="109"/>
        <source>Can I make a conference call?</source>
        <translation>Kan eg gjera ein samtale?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="114"/>
        <source>What is a Jami account?</source>
        <translation>Kva er ein Jami-konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="116"/>
        <source>A Jami account is an asymmetric encryption key. Your account is identified by a Jami ID, which is a fingerprint of your public key.</source>
        <translation>Ein Jami-konto er ein asymmetrisk krypteringsnøkkel. Konta din blir identifisert med ein Jami-ID, som er ein fingeravtrykk av offentleg nøkkel.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="120"/>
        <source>What information do I need to provide to create a Jami account?</source>
        <translation>Kva informasjon treng eg å gje for å oppretta ein Jami-konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="122"/>
        <source>When you create a new Jami account, you do not have to provide any private information like an email, address, or phone number.</source>
        <translation>Når du oppretter ein ny Jami-konto, treng du ikkje å gje nokon privat informasjon som e-post, adresse eller telefonnummer.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="129"/>
        <source>With Jami, your account is stored in a directory on your device. The password is only used to encrypt your account in order to protect you from someone who has physical access to your device.</source>
        <translation>Med Jami vert kontoen din lagra i ein katalog på gardskapet ditt. Passordet vert berre brukt til å kryptera kontoen din for å verne deg mot nokon som har fysisk tilgang til gardskapet ditt.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="149"/>
        <source>Your account is only stored on your own devices. If you delete your account from all of your devices, the account is gone forever and you CANNOT recover it.</source>
        <translation>Konta ditt vert berre lagra på dei eigne enhetane dine. Dersom du sleppar kontoen din frå alle enhetane dine, er kontoen borte for alltid og du kan IKKE gjenopprette den.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="153"/>
        <source>Can I use my account on multiple devices?</source>
        <translation>Kan eg bruke kontoen min på fleire enheter?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="155"/>
        <source>Yes, you can link your account from the settings, or you can import your backup on another device.</source>
        <translation>Ja, du kan knyta kontoen din frå innstillingane, eller du kan importera backup på ein annan enhet.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="127"/>
        <source>Why don&apos;t I have to use a password?</source>
        <translation>Kvifor må eg ikkje bruke ein passord?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="111"/>
        <source>In a call, you can click on &quot;Add participants&quot; to add a contact to a call.</source>
        <translation>I ein samtale kan du klikke på &quot;Tillegg medlegarar&quot; for å leggje til ein kontakt til ein samtale.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="135"/>
        <source>Why don&apos;t I have to register a username?</source>
        <translation>Kvifor må eg ikkje registrera eit brukernavn?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="137"/>
        <source>The most permanent, secure identifier is your Jami ID, but since these are difficult to use for some people, you also have the option of registering a username.</source>
        <translation>Den mest permanente, trygge identifikatoren er Jami ID, men sidan dei er vanskelege å bruka for nokre, har du òg høve til å registrera eit brukernavn.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="142"/>
        <source>How can I back up my account?</source>
        <translation>Korleis kan eg backup kontoen min?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="143"/>
        <source>In Account Settings, a button is available to create a backup your account.</source>
        <translation>I kontoinnstillingane er ein knapp til å oppretta ein backup til kontoen din.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="147"/>
        <source>What happens when I delete my account?</source>
        <translation>Kva skjer når eg slepp kontoen min?</translation>
    </message>
</context>
<context>
    <name>UtilsAdapter</name>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>%1 Mbps</source>
        <translation>%1 Mbps</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>Default</source>
        <translation>Misligholde</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="539"/>
        <source>System</source>
        <translation>System</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="478"/>
        <source>Searching…</source>
        <translation>Sjå etter...</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1015"/>
        <source>Invalid ID</source>
        <translation>Utilgjengeleg ID</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1018"/>
        <source>Username not found</source>
        <translation>Brukarnamn ikkje funne</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1021"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Eg kunne ikkje sjå etter...</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="441"/>
        <source>Bad URI scheme</source>
        <translation>Dåleg URI-ordning</translation>
    </message>
</context>
</TS>