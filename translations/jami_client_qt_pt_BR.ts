<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="pt_BR" sourcelanguage="en">
<context>
    <name>CallAdapter</name>
    <message>
        <location filename="../src/app/calladapter.cpp" line="220"/>
        <source>Missed call</source>
        <translation>Chamada perdida</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="221"/>
        <source>Missed call with %1</source>
        <translation>Chamada perdida com %1</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="542"/>
        <source>Incoming call</source>
        <translation>Chamada recebida</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="543"/>
        <source>%1 is calling you</source>
        <translation>%1 está chamando</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="554"/>
        <source>is calling you</source>
        <translation>está chamando você</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="1052"/>
        <source>Screenshot</source>
        <translation>Captura de tela</translation>
    </message>
</context>
<context>
    <name>ConversationsAdapter</name>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="189"/>
        <source>%1 received a new message</source>
        <translation>%1 recebeu uma nova mensagem</translation>
    </message>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="244"/>
        <source>%1 received a new trust request</source>
        <translation>%1 recebeu um novo pedido de confiança</translation>
    </message>
</context>
<context>
    <name>CurrentCall</name>
    <message>
        <location filename="../src/app/currentcall.cpp" line="185"/>
        <source>Me</source>
        <translation>Eu</translation>
    </message>
</context>
<context>
    <name>CurrentConversation</name>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="137"/>
        <source>Private</source>
        <translation>Privado</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="139"/>
        <source>Private group (restricted invites)</source>
        <translation>Grupo privado (convidados restritamente)</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="141"/>
        <source>Private group</source>
        <translation>Grupo privado</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="143"/>
        <source>Public group</source>
        <translation>Grupo público</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="324"/>
        <source>An error occurred while fetching this repository</source>
        <translation>Ocorreu um erro ao buscar este repositório</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="326"/>
        <source>Unrecognized conversation mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="330"/>
        <source>Not authorized to update conversation information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="332"/>
        <source>An error occurred while committing a new message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="328"/>
        <source>An invalid message was detected</source>
        <translation>Foi detectado um mensagem inválida.</translation>
    </message>
</context>
<context>
    <name>JamiStrings</name>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="29"/>
        <source>Accept</source>
        <translation>Aceitar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="30"/>
        <source>Accept in audio</source>
        <translation>Aceitar em áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="31"/>
        <source>Accept in video</source>
        <translation>Aceitar em vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="32"/>
        <source>Refuse</source>
        <translation>Recusar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="33"/>
        <source>End call</source>
        <translation>Finalizar chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="34"/>
        <source>Incoming audio call from {}</source>
        <translation>Recebendo chamada de áudio de {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="35"/>
        <source>Incoming video call from {}</source>
        <translation>Recebendo chamada de vídeo de {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="38"/>
        <source>Invitations</source>
        <translation>Convites</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="39"/>
        <source>Jami is a universal communication platform, with privacy as its foundation, that relies on a free distributed network for everyone.</source>
        <translation>Jami é uma plataforma de comunicação universal, com privacidade como base, que depende de uma rede distribuída gratuita para todos.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="40"/>
        <source>Migrating to the Swarm technology will enable synchronizing this conversation across multiple devices and improve reliability. The legacy conversation history will be cleared in the process.</source>
        <translation>A migração para a tecnologia Swarm permitirá sincronizar esta conversa em vários dispositivos e melhorar a confiabilidade.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="44"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="733"/>
        <source>Could not re-connect to the Jami daemon (jamid).
Jami will now quit.</source>
        <translation>Não conseguiu se reconectar ao daemon do Jami (jamid).
Jami será finalizado agora.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="45"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="732"/>
        <source>Trying to reconnect to the Jami daemon (jamid)…</source>
        <translation>Tentando se reconectar ao daemon do Jami (jamid)...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="48"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="51"/>
        <source>Jami is a free universal communication software that respects the freedom and privacy of its users.</source>
        <translation>Jami é um software de comunicação universal gratuito que respeita a liberdade e privacidade de seus usuários.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="54"/>
        <source>Display QR code</source>
        <translation>Mostrar o código QR</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="55"/>
        <source>Open settings</source>
        <translation>Abrir configurações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="56"/>
        <source>Close settings</source>
        <translation>Fechar configurações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="57"/>
        <source>Add Account</source>
        <translation>Adicionar Conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="60"/>
        <source>Add to conference</source>
        <translation>Adicionar a conferência</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="61"/>
        <source>Add to conversation</source>
        <translation>Adicionar à conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="62"/>
        <source>Transfer this call</source>
        <translation>Transferir esta chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="63"/>
        <source>Transfer to</source>
        <translation>Transferir para</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="66"/>
        <source>Authentication required</source>
        <translation>Autenticação requerida</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="67"/>
        <source>Your session has expired or been revoked on this device. Please enter your password.</source>
        <translation>Sua sessão expirou ou foi revogada neste dispositivo. Por favor, digite sua senha.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="68"/>
        <source>JAMS server</source>
        <translation>Servidor de JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="69"/>
        <source>Authenticate</source>
        <translation>Autenticado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="70"/>
        <source>Delete account</source>
        <translation>Deletar conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="71"/>
        <source>In progress…</source>
        <translation>Em progresso...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="72"/>
        <source>Authentication failed</source>
        <translation>Falha na autenticação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="73"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="74"/>
        <source>Username</source>
        <translation>Nome de usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="75"/>
        <source>Alias</source>
        <translation>Apelido</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="78"/>
        <source>Allow incoming calls from unknown contacts</source>
        <translation>Permitir chamadas de contatos desconhecidos</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="79"/>
        <source>Convert your account into a rendezvous point</source>
        <translation>Converta sua conta num ponto de encontro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="80"/>
        <source>Automatically answer calls</source>
        <translation>Atender chamadas automaticamente</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="81"/>
        <source>Enable custom ringtone</source>
        <translation>Ativar o toque personalizado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="82"/>
        <source>Select custom ringtone</source>
        <translation>Selecione um toque personalizado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="83"/>
        <source>Select a new ringtone</source>
        <translation>Selecione um novo toque</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="84"/>
        <source>Certificate File (*.crt)</source>
        <translation>Arquivo de certificado (*.crt)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="85"/>
        <source>Audio File (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</source>
        <translation>Arquivo de áudio (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="86"/>
        <source>Push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="87"/>
        <source>Enable push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="88"/>
        <source>Keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="89"/>
        <source>Change keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="92"/>
        <source>Change shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="93"/>
        <source>Press the key to be assigned to push-to-talk shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="94"/>
        <source>Assign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="97"/>
        <source>Enable read receipts</source>
        <translation>Ativar verificação de leitura</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="98"/>
        <source>Send and receive receipts indicating that a message have been displayed</source>
        <translation>Enviar e receber confirmações indicando que uma mensagem foi mostrada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="101"/>
        <source>Voicemail</source>
        <translation>Correio de voz</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="102"/>
        <source>Voicemail dial code</source>
        <translation>Código de discagem de voicemail</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="105"/>
        <source>Security</source>
        <translation>Segurança</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="106"/>
        <source>Enable SDES key exchange</source>
        <translation>Ativar o intercâmbio de chaves SDES</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="107"/>
        <source>Encrypt negotiation (TLS)</source>
        <translation>Negociação de encriptação(TLS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="108"/>
        <source>CA certificate</source>
        <translation>Certificado de CA</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="109"/>
        <source>User certificate</source>
        <translation>Certificado de usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="110"/>
        <source>Private key</source>
        <translation>Chave privada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="111"/>
        <source>Private key password</source>
        <translation>Senha de chave privada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="112"/>
        <source>Verify certificates for incoming TLS connections</source>
        <translation>Verificar certificados para conexões TLS recebidas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="113"/>
        <source>Verify server TLS certificates</source>
        <translation>Verificar os certificados TLS do servidor</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="114"/>
        <source>Require certificate for incoming TLS connections</source>
        <translation>Exigir certificado para conexões TLS recebidas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="116"/>
        <source>Select a private key</source>
        <translation>Selecione uma chave privada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="117"/>
        <source>Select a user certificate</source>
        <translation>Selecione um certificado de usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="118"/>
        <source>Select a CA certificate</source>
        <translation>Selecione um certificado CA</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="120"/>
        <source>Key File (*.key)</source>
        <translation>Arquivo de chave (*.key)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="123"/>
        <source>Connectivity</source>
        <translation>Conectividade</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="124"/>
        <source>Auto Registration After Expired</source>
        <translation>Registro automático após expirado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="125"/>
        <source>Registration expiration time (seconds)</source>
        <translation>Tempo de expiração de registro (segundos)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="126"/>
        <source>Network interface</source>
        <translation>Interface de Rede</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="127"/>
        <source>Use UPnP</source>
        <translation>Usar UPnP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="128"/>
        <source>Use TURN</source>
        <translation>Usar TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="129"/>
        <source>TURN address</source>
        <translation>Endereço TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="130"/>
        <source>TURN username</source>
        <translation>Nome de usuário TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="131"/>
        <source>TURN password</source>
        <translation>Senha TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="132"/>
        <source>TURN Realm</source>
        <translation>ATIVAR o Realm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="133"/>
        <source>Use STUN</source>
        <translation>Usar STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="134"/>
        <source>STUN address</source>
        <translation>Endereço STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="137"/>
        <source>Allow IP Auto Rewrite</source>
        <translation>Permitir reescrita automática do IP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="138"/>
        <source>Public address</source>
        <translation>Endereço público</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="139"/>
        <source>Use custom address and port</source>
        <translation>Use endereço e porta personalizados</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="140"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="141"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="144"/>
        <source>Media</source>
        <translation>Mídia</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="145"/>
        <source>Enable video</source>
        <translation>Habilitar vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="153"/>
        <source>SDP Session Negotiation (ICE Fallback)</source>
        <translation>Negociação de sessão SDP ( ICE Fallback)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="154"/>
        <source>Only used during negotiation in case ICE is not supported</source>
        <translation>Utilizado somente durante a negociação quando o ICE não é suportado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="155"/>
        <source>Audio RTP minimum Port</source>
        <translation>Porta mínima no RTP de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="156"/>
        <source>Audio RTP maximum Port</source>
        <translation>Porta máxima no RTP de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="157"/>
        <source>Video RTP minimum Port</source>
        <translation>Porta mínima no RTP de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="158"/>
        <source>Video RTP maximum port</source>
        <translation>Porta máxima no RTP de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="161"/>
        <source>Enable local peer discovery</source>
        <translation>Ativar descoberta local de pares</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="162"/>
        <source>Connect to other DHT nodes advertising on your local network.</source>
        <translation>Conectar ao outros nós de DHT disponíveis na sua rede local.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="164"/>
        <source>Enable proxy</source>
        <translation>Habilitar proxy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="165"/>
        <source>Proxy address</source>
        <translation>Endereço do proxy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="166"/>
        <source>Bootstrap</source>
        <translation>Inicialização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="169"/>
        <source>Back</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="170"/>
        <source>Account</source>
        <translation>Conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="171"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="172"/>
        <source>Extensions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="182"/>
        <source>Audio</source>
        <translation>Áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="183"/>
        <source>Microphone</source>
        <translation>Microfone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="184"/>
        <source>Select audio input device</source>
        <translation>Selecione o dispositivo de entrada de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="185"/>
        <source>Output device</source>
        <translation>Dispositivo de saída</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="186"/>
        <source>Select audio output device</source>
        <translation>Selecione o dispositivo de saída de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="187"/>
        <source>Ringtone device</source>
        <translation>Tons de toque do dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="188"/>
        <source>Select ringtone output device</source>
        <translation>Selecione o dispositivo de saída do toque de chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="189"/>
        <source>Audio manager</source>
        <translation>Gerenciador de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="193"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="194"/>
        <source>Select video device</source>
        <translation>Selecione o dispositivo de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="195"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="196"/>
        <source>Resolution</source>
        <translation>Resolução</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="197"/>
        <source>Select video resolution</source>
        <translation>Selecione a resolução de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="198"/>
        <source>Frames per second</source>
        <translation>Quadros por segundo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="199"/>
        <source>Select video frame rate (frames per second)</source>
        <translation>Selecione a taxa de quadros de vídeo (quadros por segundo)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="200"/>
        <source>Enable hardware acceleration</source>
        <translation>Ativar aceleração de hardware</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="203"/>
        <source>Select screen sharing frame rate (frames per second)</source>
        <translation>Selecionar a taxa de quadros no compartilhamento de tela (quadros por segundo)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="204"/>
        <source>no video</source>
        <translation>sem vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="209"/>
        <source>Back up account here</source>
        <translation>- Faz backup aqui.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="210"/>
        <source>Back up account</source>
        <translation>Criar cópia de segurança da conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="284"/>
        <source>Unavailable</source>
        <translation>Não disponível</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="311"/>
        <source>Turn off sharing</source>
        <translation>Desligue o compartilhamento</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="312"/>
        <source>Stop location sharing in this conversation (%1)</source>
        <translation>Parar o compartilhamento de localização nesta conversa (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="335"/>
        <source>Hide chat</source>
        <translation>Esconde-se</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="344"/>
        <source>Back to Call</source>
        <translation>Voltar para o Call</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="353"/>
        <source>Scroll to end of conversation</source>
        <translation>Desdobrar para o fim da conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="371"/>
        <source>You can choose a username to help others more easily find and reach you on Jami.</source>
        <translation>Pode escolher um nome de usuário para ajudar os outros a encontrar e alcançá-lo mais facilmente em Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="378"/>
        <source>Are you sure you would like to join Jami without a username?
If yes, only a randomly generated 40-character identifier will be assigned to this account.</source>
        <translation>Se sim, só um identificador de 40 caracteres gerado aleatoriamente será atribuído a esta conta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="379"/>
        <source>- 32 characters maximum
- Alphabetical characters (A to Z and a to z)
- Numeric characters (0 to 9)
- Special characters allowed: dash (-)</source>
        <translation>- 32 caracteres no máximo - caracteres alfabéticos (A a Z e a a z) - caracteres numéricos (0 a 9) - caracteres especiais permitidos: punheta (-)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="385"/>
        <source>Your account will be created and stored locally.</source>
        <translation>A sua conta será criada e armazenada localmente.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="386"/>
        <source>Choosing a username is recommended, and a chosen username CANNOT be changed later.</source>
        <translation>É recomendável escolher um nome de usuário, e um nome de usuário escolhido NÃO PODE ser alterado mais tarde.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="387"/>
        <source>Encrypting your account with a password is optional, and if the password is lost it CANNOT be recovered later.</source>
        <translation>Criptografar a sua conta com uma senha é opcional, e se a senha for perdida, NÃO PODE ser recuperada mais tarde.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="388"/>
        <source>Setting a profile picture and nickname is optional, and can also be changed later in the settings.</source>
        <translation>A configuração de uma foto de perfil e de um apelido é opcional e também pode ser alterada mais tarde nas configurações.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="397"/>
        <source>TLS</source>
        <translation>TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="398"/>
        <source>UDP</source>
        <translation>UDP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="399"/>
        <source>Display Name</source>
        <translation>Exibir Nome</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="403"/>
        <source>A chosen username can help to be found more easily on Jami.
If a username is not chosen, a randomly generated 40-character identifier will be assigned to this account as a username. It is more difficult to be found and reached with this identifier.</source>
        <translation>Um nome de usuário escolhido pode ajudar a ser encontrado mais facilmente no Jami. Se um nome de usuário não for escolhido, um identificador de 40 caracteres gerado aleatoriamente será atribuído a esta conta como um nome de usuário. É mais difícil de encontrar e alcançar com este identificador.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="406"/>
        <source>This Jami account exists only on this device.
The account will be lost if this device is lost or the application is uninstalled. It is recommended to make a backup of this account.</source>
        <translation>Esta conta Jami existe apenas neste dispositivo. A conta será perdida se este dispositivo for perdido ou o aplicativo for desinstalado. É recomendável fazer um backup desta conta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="421"/>
        <source>Encrypt account</source>
        <translation>Conta criptografada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="424"/>
        <source>Back up account to a .gz file</source>
        <translation>Fazer backup de conta para um arquivo.gz</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="430"/>
        <source>This profile is only shared with this account's contacts.
The profile can be changed at all times from the account&apos;s settings.</source>
        <translation>Este perfil só é compartilhado com os contatos desta conta. O perfil pode ser alterado a qualquer momento a partir das configurações da conta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="431"/>
        <source>Encrypt account with a password</source>
        <translation>Criptografar conta com uma senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="432"/>
        <source>A Jami account is created and stored locally only on this device, as an archive containing your account keys. Access to this archive can optionally be protected by a password.</source>
        <translation>Uma conta Jami é criada e armazenada localmente apenas neste dispositivo, como um arquivo contendo as chaves da sua conta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="433"/>
        <source>Please note that if you lose your password, it CANNOT be recovered!</source>
        <translation>Por favor, note que se perder a sua senha, NÃO pode ser recuperada!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="459"/>
        <source>Would you really like to delete this account?</source>
        <translation>Gostaria mesmo de apagar esta conta?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="460"/>
        <source>If your account has not been backed up or added to another device, your account and registered username will be IRREVOCABLY LOST.</source>
        <translation>Se a sua conta não for copiada ou adicionada a outro dispositivo, a sua conta e o seu nome de usuário registrado serão IRREVOCABLE LOST.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="471"/>
        <source>Dark</source>
        <translation>Escuridão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="472"/>
        <source>Light</source>
        <translation>Luz</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="480"/>
        <source>Include local video in recording</source>
        <translation>Incluir vídeo local na gravação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="481"/>
        <source>Default settings</source>
        <translation>Configurações padrão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="484"/>
        <source>Enable typing indicators</source>
        <translation>Habilitar indicadores de digitação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="485"/>
        <source>Send and receive typing indicators showing that a message is being typed.</source>
        <translation>Enviar e receber indicadores de escrita, mostrando que a mensagem está sendo escrita.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="486"/>
        <source>Show link preview in conversations</source>
        <translation>Mostre o link em conversas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="508"/>
        <source>Delete file from device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="525"/>
        <source>Content access error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="526"/>
        <source>Content not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="589"/>
        <source>Enter account password</source>
        <translation>Digite a senha da conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="590"/>
        <source>This account is password encrypted, enter the password to generate a PIN code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="592"/>
        <source>PIN expired</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="593"/>
        <source>On another device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="594"/>
        <source>Install and launch Jami, select &quot;Import from another device&quot; and scan the QR code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="595"/>
        <source>Link new device</source>
        <translation>Adicionar novo dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="596"/>
        <source>In Jami, scan QR code or manually enter the PIN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="597"/>
        <source>The PIN code is valid for: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="600"/>
        <source>Enter password</source>
        <translation>Entre a senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="602"/>
        <source>Enter account password to confirm the removal of this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="742"/>
        <source>Show less</source>
        <translation>Mostre menos</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="744"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="745"/>
        <source>Continue editing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="748"/>
        <source>Strikethrough</source>
        <translation>Tachado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="754"/>
        <source>Unordered list</source>
        <translation>Lista não ordenada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="755"/>
        <source>Ordered list</source>
        <translation>Lista ordenada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="758"/>
        <source>Press Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="759"/>
        <source>Press Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="772"/>
        <source>Select dedicated device for hosting future calls in this swarm. If not set, the host will be the device starting a call.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="773"/>
        <source>Select this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="774"/>
        <source>Select device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="833"/>
        <source>Appearance</source>
        <translation>Aparência</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="852"/>
        <source>Free and private sharing. &lt;a href=&quot;https://jami.net/donate/&quot;&gt;Donate&lt;/a&gt; to expand it.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="853"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="854"/>
        <source>If you enjoy using Jami and believe in our mission, would you make a donation?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="855"/>
        <source>Not now</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="856"/>
        <source>Enable donation campaign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="859"/>
        <source>Enter</source>
        <translation>Introduzir</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="860"/>
        <source>Shift+Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="861"/>
        <source>Enter or Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="875"/>
        <source>View</source>
        <translation>Visualizar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="862"/>
        <source>Text formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="865"/>
        <source>Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="866"/>
        <source>Connecting TLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="867"/>
        <source>Connecting ICE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="868"/>
        <source>Connecting</source>
        <translation>Conectando</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="869"/>
        <source>Waiting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="870"/>
        <source>Contact</source>
        <translation>Contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="871"/>
        <source>Connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="872"/>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="873"/>
        <source>Copy all data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="874"/>
        <source>Remote: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="496"/>
        <source>Accept transfer limit (in Mb)</source>
        <translation>Limite de transferência aceito (em Mb)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="516"/>
        <source>A new version of Jami was found
Would you like to update now?</source>
        <translation>Encontraram uma nova versão da Jami. Quer atualizar agora?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="539"/>
        <source>Save recordings to</source>
        <translation>Salvar gravações para</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="540"/>
        <source>Save screenshots to</source>
        <translation>Salvar capturas de tela para</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="582"/>
        <source>Select &quot;Link another device&quot;</source>
        <translation>Selecione &quot;Conectar outro dispositivo&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="609"/>
        <source>Choose a picture as your avatar</source>
        <translation>Escolha uma foto como seu avatar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="685"/>
        <source>Share freely and privately with Jami</source>
        <translation>Compartilhar livremente e em privado com Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="711"/>
        <source>Unban</source>
        <translation>Cancelar banimento</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="728"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="737"/>
        <source>more emojis</source>
        <translation>mais emojis</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="761"/>
        <source>Reply to</source>
        <translation>Resposta a</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="762"/>
        <source>In reply to</source>
        <translation>Em resposta à</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="763"/>
        <source> replied to</source>
        <translation>Respondeu</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="765"/>
        <source>Reply</source>
        <translation>Responder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="464"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="767"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="768"/>
        <source>Edited</source>
        <translation>Edição</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="769"/>
        <source>Join call</source>
        <translation>Juntar chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="770"/>
        <source>A call is in progress. Do you want to join the call?</source>
        <translation>Uma chamada está em andamento.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="771"/>
        <source>Current host for this swarm seems unreachable. Do you want to host the call?</source>
        <translation>O anfitrião atual deste enxame parece inacessível.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="775"/>
        <source>Remove current device</source>
        <translation>Remover o dispositivo corrente</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="776"/>
        <source>Host only this call</source>
        <translation>Só recebe esta chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="777"/>
        <source>Host this call</source>
        <translation>Organizador desta chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="778"/>
        <source>Make me the default host for future calls</source>
        <translation>Torne-me o anfitrião padrão para futuras chamadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="793"/>
        <source>Mute conversation</source>
        <translation>Conversa silenciosa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="796"/>
        <source>Default host (calls)</source>
        <translation>Host padrão (chamadas)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="799"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="813"/>
        <source>Tip</source>
        <translation>- A dica</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="815"/>
        <source>Add a profile picture and nickname to complete your profile</source>
        <translation>Adicionar uma foto de perfil e apelido para completar o seu perfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="36"/>
        <source>Start swarm</source>
        <translation>Comece o enxame.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="37"/>
        <source>Create swarm</source>
        <translation>Criar enxame</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="176"/>
        <source>Call settings</source>
        <translation>Configurações de chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="115"/>
        <source>Disable secure dialog check for incoming TLS data</source>
        <translation>Desativar a verificação de diálogo segura para dados TLS entrantes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="146"/>
        <source>Video codecs</source>
        <translation>Codecs de Vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="147"/>
        <source>Audio codecs</source>
        <translation>Codecs de Áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="150"/>
        <source>Name server</source>
        <translation>Nome do servidor</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="163"/>
        <source>OpenDHT configuration</source>
        <translation>Configuração do OpenDHT</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="201"/>
        <source>Mirror local video</source>
        <translation>Espelho local vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="207"/>
        <source>Why should I back-up this account?</source>
        <translation>Porque é que eu devia fazer backup desta conta?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="211"/>
        <source>Success</source>
        <translation>Sucesso</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="212"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="213"/>
        <source>Jami archive files (*.gz)</source>
        <translation>Arquivos de arquivo Jami (*.gz)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="214"/>
        <source>All files (*)</source>
        <translation>Todos os ficheiros (*)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="217"/>
        <source>Reinstate as contact</source>
        <translation>Reintegrar como contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="218"/>
        <source>name</source>
        <translation>nome</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="219"/>
        <source>Identifier</source>
        <translation>Identificador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="222"/>
        <source>is recording</source>
        <translation>Está gravando</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="223"/>
        <source>are recording</source>
        <translation>Estão gravando</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="224"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="716"/>
        <source>Mute</source>
        <translation>Mudo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="225"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="717"/>
        <source>Unmute</source>
        <translation>Não silenciar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="226"/>
        <source>Pause call</source>
        <translation>Pausar ligação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="227"/>
        <source>Resume call</source>
        <translation>Retornar chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="228"/>
        <source>Mute camera</source>
        <translation>Câmara mudada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="229"/>
        <source>Unmute camera</source>
        <translation>- Não é uma câmara silenciosa.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="230"/>
        <source>Add participant</source>
        <translation>Adicionar participante</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="231"/>
        <source>Add participants</source>
        <translation>Adicionar participantes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="232"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="177"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="233"/>
        <source>Chat</source>
        <translation>Conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="174"/>
        <source>Manage account</source>
        <translation>Administrar conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="175"/>
        <source>Linked devices</source>
        <translation>Dispositivos vinculados</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="178"/>
        <source>Advanced settings</source>
        <translation>Configurações avançadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="179"/>
        <source>Audio and Video</source>
        <translation>Áudio e vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="190"/>
        <source>Sound test</source>
        <translation>Teste de som</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="202"/>
        <source>Screen sharing</source>
        <translation>Partilha de tela</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="208"/>
        <source>Your account only exists on this device. If you lose your device or uninstall the application, your account will be deleted and CANNOT be recovered. You can &lt;a href=&apos;blank&apos;&gt; back up your account &lt;/a&gt; now or later (in the Account Settings).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="234"/>
        <source>More options</source>
        <translation>Mais opções</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="235"/>
        <source>Mosaic</source>
        <translation>Mosaico</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="236"/>
        <source>Participant is still muted on their device</source>
        <translation>O participante ainda está com o microfone desligado, pelo equipamento dele.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="237"/>
        <source>You are still muted on your device</source>
        <translation>Você ainda está com o seu microfone desligado em seu dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="238"/>
        <source>You are still muted by moderator</source>
        <translation>Você ainda está com o microfone desligado pelo moderador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="239"/>
        <source>You are muted by a moderator</source>
        <translation>Você está com o microfone desligado por um moderador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="240"/>
        <source>Moderator</source>
        <translation>Moderador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="241"/>
        <source>Host</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="242"/>
        <source>Local and Moderator muted</source>
        <translation>Seu microfone está desligado localmente e também pelo moderador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="243"/>
        <source>Moderator muted</source>
        <translation>Seu microfone está desligado pelo moderador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="244"/>
        <source>Not muted</source>
        <translation>Seu microfone não está desligado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="245"/>
        <source>On the side</source>
        <translation>No lado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="246"/>
        <source>On the top</source>
        <translation>No topo.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="247"/>
        <source>Hide self</source>
        <translation>Esconde-se</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="248"/>
        <source>Hide spectators</source>
        <translation>Esconde os espectadores</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="251"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="554"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="252"/>
        <source>Share</source>
        <translation>Compartilhar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="253"/>
        <source>Cut</source>
        <translation>Recortar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="254"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="257"/>
        <source>Start video call</source>
        <translation>Iniciar videochamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="258"/>
        <source>Start audio call</source>
        <translation>Iniciar chamada de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="259"/>
        <source>Clear conversation</source>
        <translation>Eliminar a conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="260"/>
        <source>Confirm action</source>
        <translation>Ação de confirmação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="261"/>
        <source>Remove conversation</source>
        <translation>Excluir uma conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="262"/>
        <source>Would you really like to remove this conversation?</source>
        <translation>Gostaria mesmo de remover esta conversa?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="263"/>
        <source>Would you really like to block this conversation?</source>
        <translation>Queres mesmo bloquear esta conversa?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="264"/>
        <source>Remove contact</source>
        <translation>Remover contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="265"/>
        <source>Block contact</source>
        <translation>Bloquear contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="266"/>
        <source>Conversation details</source>
        <translation>Detalhes da conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="267"/>
        <source>Contact details</source>
        <translation>Detalhes do contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="270"/>
        <source>Sip input panel</source>
        <translation>Painel de entrada de Sip</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="271"/>
        <source>Transfer call</source>
        <translation>Transferir chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="272"/>
        <source>Stop recording</source>
        <translation>Pare de gravar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="273"/>
        <source>Start recording</source>
        <translation>Iniciar a gravação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="274"/>
        <source>View full screen</source>
        <translation>Exibir em tela inteira</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="275"/>
        <source>Share screen</source>
        <translation>Compartilhar tela</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="276"/>
        <source>Share window</source>
        <translation>Compartilhar janela</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="277"/>
        <source>Stop sharing screen or file</source>
        <translation>Parar o compartilhamento de tela ou de arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="278"/>
        <source>Share screen area</source>
        <translation>Compartilhar área da tela</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="279"/>
        <source>Share file</source>
        <translation>Compartilhar arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="280"/>
        <source>Select sharing method</source>
        <translation>Selecionar o método de compartilhamento</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="281"/>
        <source>View plugin</source>
        <translation>Ver os complementos</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="282"/>
        <source>Advanced information</source>
        <translation>Informações avançadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="283"/>
        <source>No video device</source>
        <translation>Nenhum dispositivo de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="285"/>
        <source>Lower hand</source>
        <translation>Abaixar a mão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="286"/>
        <source>Raise hand</source>
        <translation>Levantar a mão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="287"/>
        <source>Layout settings</source>
        <translation>Configurações de layout</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="288"/>
        <source>Take tile screenshot</source>
        <translation>Faça uma captura de tela de telhas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="289"/>
        <source>Screenshot saved to %1</source>
        <translation>Imagem de tela guardada para % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="290"/>
        <source>File saved to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="293"/>
        <source>Renderers information</source>
        <translation>Informações dos fornecedores</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="294"/>
        <source>Call information</source>
        <translation>Informações de ligação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="295"/>
        <source>Peer number</source>
        <translation>Número de pares</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="296"/>
        <source>Call id</source>
        <translation>Identificação de chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="297"/>
        <source>Sockets</source>
        <translation>As portas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="298"/>
        <source>Video codec</source>
        <translation>Códec de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="299"/>
        <source>Hardware acceleration</source>
        <translation>Aceleração de hardware</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="300"/>
        <source>Video bitrate</source>
        <translation>Taxa de bits de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="301"/>
        <source>Audio codec</source>
        <translation>Códec de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="302"/>
        <source>Renderer id</source>
        <translation>Identificação do fornecedor</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="303"/>
        <source>Fps</source>
        <translation>Fps</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="306"/>
        <source>Share location</source>
        <translation>Compartilhar localização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="307"/>
        <source>Stop sharing</source>
        <translation>Parar de compartilhar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="308"/>
        <source>Your precise location could not be determined.
In Device Settings, please turn on &quot;Location Services&quot;.
Other participants&apos; location can still be received.</source>
        <translation>Não foi possível determinar a sua localização precisa. Em Configurações de dispositivo, por favor, ative &quot;Serviços de localização&quot;. A localização dos outros participantes ainda pode ser recebida.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="309"/>
        <source>Your precise location could not be determined. Please check your Internet connection.</source>
        <translation>Não foi possível determinar a sua localização precisa.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="310"/>
        <source>Turn off location sharing</source>
        <translation>Desligue o compartilhamento de localização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="313"/>
        <source>Location is shared in several conversations</source>
        <translation>Localização compartilhada em várias conversas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="314"/>
        <source>Pin map to be able to share location or to turn off location in specific conversations</source>
        <translation>Mapa de pin para compartilhar a localização ou desativar a localização em conversas específicas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="315"/>
        <source>Location is shared in several conversations, click to choose how to turn off location sharing</source>
        <translation>Localização é compartilhada em várias conversas, clique para escolher como desativar o compartilhamento de localização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="316"/>
        <source>Share location to participants of this conversation (%1)</source>
        <translation>Compartilhar a localização com os participantes desta conversa (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="318"/>
        <source>Reduce</source>
        <translation>Reduzir</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="319"/>
        <source>Drag</source>
        <translation>Arraste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="320"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="322"/>
        <source>Unpin</source>
        <translation>Desembaraçamento</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="323"/>
        <source>Pin</source>
        <translation>Pin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="324"/>
        <source>Position share duration</source>
        <translation>Duração das participações em posições</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="325"/>
        <source>Location sharing</source>
        <translation>Partilha de localização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="326"/>
        <source>Unlimited</source>
        <translation>Não limitado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="327"/>
        <source>1 min</source>
        <translation>1 minuto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="328"/>
        <source>%1h%2min</source>
        <translation>% 1h% 2min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="329"/>
        <source>%1h</source>
        <translation>% 1h</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="330"/>
        <source>%1min%2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="331"/>
        <source>%1min</source>
        <translation>%1min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="332"/>
        <source>%sec</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="336"/>
        <source>Place audio call</source>
        <translation>Fazer chamada de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="337"/>
        <source>Place video call</source>
        <translation>Fazer videochamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="338"/>
        <source>Show available plugins</source>
        <translation>Mostrar extensões disponíveis</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="339"/>
        <source>Add to conversations</source>
        <translation>Adicionar às conversas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="340"/>
        <source>This is the error from the backend: %0</source>
        <translation>Este é o erro do backend: %0</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="341"/>
        <source>The account is disabled</source>
        <translation>A conta está desativada.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="342"/>
        <source>No network connectivity</source>
        <translation>Sem conectividade</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="343"/>
        <source>Deleted message</source>
        <translation>Mensagem apagada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="347"/>
        <source>Jump to</source>
        <translation>Salta para</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="348"/>
        <source>Messages</source>
        <translation>Mensagens</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="349"/>
        <source>Files</source>
        <translation>Arquivos</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="350"/>
        <source>Search</source>
        <translation>Busca</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="354"/>
        <source>{} is typing…</source>
        <translation>{} está escrevendo...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="355"/>
        <source>{} are typing…</source>
        <translation>{} estão escrevendo...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="356"/>
        <source>Several people are typing…</source>
        <translation>Várias pessoas estão escrevendo...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="357"/>
        <source> and </source>
        <translation>e</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="360"/>
        <source>Enter the Jami Account Management Server (JAMS) URL</source>
        <translation>Digite o URL do servidor de gerenciamento de contas Jami (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="361"/>
        <source>Jami Account Management Server URL</source>
        <translation>URL do servidor de gestão de conta de Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="362"/>
        <source>Enter JAMS credentials</source>
        <translation>Digite as credenciais JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="363"/>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="364"/>
        <source>Creating account…</source>
        <translation>Criando a conta...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="367"/>
        <source>Choose name</source>
        <translation>Escolha um nome</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="368"/>
        <source>Choose username</source>
        <translation>Escolha um nome de usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="369"/>
        <source>Choose a username</source>
        <translation>Escolha um nome de usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="428"/>
        <source>Encrypt account with password</source>
        <translation>Criptografar conta com senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="370"/>
        <source>Confirm password</source>
        <translation>Confirmar senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="372"/>
        <source>Choose a name for your rendezvous point</source>
        <translation>Escolha um nome para seu ponto de encontro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="373"/>
        <source>Choose a name</source>
        <translation>Escolha um nome</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="374"/>
        <source>Invalid name</source>
        <translation>Nome inválido</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="375"/>
        <source>Invalid username</source>
        <translation>Nome de usuário inválido</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="376"/>
        <source>Name already taken</source>
        <translation>O nome já existe</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="377"/>
        <source>Username already taken</source>
        <translation>Nome de usuário já em uso</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="382"/>
        <source>Good to know</source>
        <translation>É bom saber.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="383"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="384"/>
        <source>Encrypt</source>
        <translation>Encriptação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="391"/>
        <source>SIP account</source>
        <translation>Conta SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="392"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="393"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="394"/>
        <source>Configure an existing SIP account</source>
        <translation>Configurar uma conta existente de SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="395"/>
        <source>Personalize account</source>
        <translation>Personalizar a conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="396"/>
        <source>Add SIP account</source>
        <translation>Adicionar conta SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="402"/>
        <source>Your profile is only shared with your contacts.
Your picture and your nickname can be changed at all time in the settings of your account.</source>
        <translation>O seu perfil só é compartilhado com os seus contatos.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="404"/>
        <source>Your Jami account is registered only on this device as an archive containing the keys of your account. Access to this archive can be protected by a password.</source>
        <translation>A sua conta Jami é registrada apenas neste dispositivo como um arquivo contendo as chaves da sua conta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="405"/>
        <source>Backup account</source>
        <translation>Cópia de segurança da conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="407"/>
        <source>Delete your account</source>
        <translation>Eliminar a sua conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="408"/>
        <source>If your account has not been backed up or added to another device, your account and registered name will be irrevocably lost.</source>
        <translation>Se sua conta não foi copiada ou adicionada em outro dispositivo, sua conta e nome registrado será irrevogavelmente perdida.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="409"/>
        <source>List of the devices that are linked to this account:</source>
        <translation>Lista dos dispositivos que estão ligados a esta conta:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="410"/>
        <source>This device</source>
        <translation>Este dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="411"/>
        <source>Other linked devices</source>
        <translation>Outro dispositivo conectado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="414"/>
        <source>Backup successful</source>
        <translation>Backup bem-sucedido</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="415"/>
        <source>Backup failed</source>
        <translation>Falha na criação de cópia de segurança</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="416"/>
        <source>Password changed successfully</source>
        <translation>A senha alterada com sucesso</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="417"/>
        <source>Password change failed</source>
        <translation>Não foi possível alterar a senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="418"/>
        <source>Password set successfully</source>
        <translation>A senha definida com sucesso</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="419"/>
        <source>Password set failed</source>
        <translation>Não foi possível definir a senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="420"/>
        <source>Change password</source>
        <translation>Mudar senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="434"/>
        <source>Enter a nickname, surname…</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="435"/>
        <source>Use this account on other devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="436"/>
        <source>This account is created and stored locally, if you want to use it on another device you have to link the new device to this account.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="465"/>
        <source>Device name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="550"/>
        <source>Markdown</source>
        <translation>Marcação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="617"/>
        <source>Auto update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="618"/>
        <source>Disable all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="619"/>
        <source>Installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="620"/>
        <source>Install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="621"/>
        <source>Installing</source>
        <translation>Instalação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="622"/>
        <source>Install manually</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="623"/>
        <source>Install an extension directly from your device.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="624"/>
        <source>Available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="625"/>
        <source>Plugins store is not available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="627"/>
        <source>Installation failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="628"/>
        <source>The installation of the plugin failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="646"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="647"/>
        <source>Last update %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="648"/>
        <source>By %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="649"/>
        <source>Proposed by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="652"/>
        <source>More information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="739"/>
        <source>Audio message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="740"/>
        <source>Video message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="741"/>
        <source>Show more</source>
        <translation>Mostre mais</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="746"/>
        <source>Bold</source>
        <translation>Negrito</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="747"/>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="749"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="750"/>
        <source>Heading</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="751"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="752"/>
        <source>Code</source>
        <translation>Código</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="753"/>
        <source>Quote</source>
        <translation>Citação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="756"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="757"/>
        <source>Hide formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="824"/>
        <source>Share your Jami identifier in order to be contacted more easily!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="825"/>
        <source>Jami identity</source>
        <translation>Identidade Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="826"/>
        <source>Show fingerprint</source>
        <translation>Mostre impressão digital</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="827"/>
        <source>Show registered name</source>
        <translation>Mostre nome registrado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="830"/>
        <source>Enabling your account allows you to be contacted on Jami</source>
        <translation>Ativar a sua conta permite que você seja contactado em Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="836"/>
        <source>Experimental</source>
        <translation>Experimentais</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="839"/>
        <source>Ringtone</source>
        <translation>Toque de chamada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="842"/>
        <source>Rendezvous point</source>
        <translation>Ponto de encontro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="845"/>
        <source>Moderation</source>
        <translation>Moderação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="848"/>
        <source>Theme</source>
        <translation>Temática</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="849"/>
        <source>Text zoom level</source>
        <translation>Nível de zoom de texto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="422"/>
        <source>Set a password</source>
        <translation>Configurar uma senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="423"/>
        <source>Change current password</source>
        <translation>Alterar a senha atual</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="425"/>
        <source>Display advanced settings</source>
        <translation>Exibir as configurações avançadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="426"/>
        <source>Hide advanced settings</source>
        <translation>Ocultar as configurações avançadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="173"/>
        <source>Enable account</source>
        <translation>Ativar conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="427"/>
        <source>Advanced account settings</source>
        <translation>Configurações avançadas de conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="429"/>
        <source>Customize profile</source>
        <translation>Personalizar o perfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="440"/>
        <source>Set username</source>
        <translation>Definir um nome de usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="441"/>
        <source>Registering name</source>
        <translation>Registro de nome</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="444"/>
        <source>Identity</source>
        <translation>Identificação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="447"/>
        <source>Link a new device to this account</source>
        <translation>Vincular um novo dispositivo a esta conta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="448"/>
        <source>Exporting account…</source>
        <translation>Exportando a conta...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="449"/>
        <source>Remove Device</source>
        <translation>Excluir o dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="450"/>
        <source>Are you sure you wish to remove this device?</source>
        <translation>Realmente quer excluir este dispositivo?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="451"/>
        <source>Your PIN is:</source>
        <translation>Seu PIN é:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="452"/>
        <source>Error connecting to the network.
Please try again later.</source>
        <translation>Erro de conexão na rede.
Por favor, tente novamente mais tarde.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="455"/>
        <source>Banned</source>
        <translation>Proibido</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="456"/>
        <source>Banned contacts</source>
        <translation>Contatos banidos</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="467"/>
        <source>Device Id</source>
        <translation>Id do Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="470"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="473"/>
        <source>Select a folder</source>
        <translation>Selecione uma pasta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="474"/>
        <source>Enable notifications</source>
        <translation>Ativar notificações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="478"/>
        <source>Launch at startup</source>
        <translation>Abrir ao iniciar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="479"/>
        <source>Choose download directory</source>
        <translation>Escolha a pasta de Downloads</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="487"/>
        <source>Preview requires downloading content from third-party servers.</source>
        <translation>A pré-visualização requer o download de conteúdo de servidores de terceiros.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="489"/>
        <source>User interface language</source>
        <translation>Idioma da interface do usuário</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="490"/>
        <source>Vertical view</source>
        <translation>Exibição vertical</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="491"/>
        <source>Horizontal view</source>
        <translation>Exibição horizontal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="494"/>
        <source>File transfer</source>
        <translation>Transferência de arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="495"/>
        <source>Automatically accept incoming files</source>
        <translation>Aceitar automaticamente os novos arquivos </translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="497"/>
        <source>in MB, 0 = unlimited</source>
        <translation>em MB, 0 = ilimitado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="500"/>
        <source>Register</source>
        <translation>Registrar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="501"/>
        <source>Incorrect password</source>
        <translation>Senha incorreta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="502"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="519"/>
        <source>Network error</source>
        <translation>Erro de rede</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="503"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="527"/>
        <source>Something went wrong</source>
        <translation>Algo deu errado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="506"/>
        <source>Save file</source>
        <translation>Salvar arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="507"/>
        <source>Open location</source>
        <translation>Abrir o local</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="764"/>
        <source>Me</source>
        <translation>Eu</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="511"/>
        <source>Install beta version</source>
        <translation>Instalar a versão beta</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="512"/>
        <source>Check for updates now</source>
        <translation>Checar por atualizações agora</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="513"/>
        <source>Enable/Disable automatic updates</source>
        <translation>Desativar/Ativar a atualização automática</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="514"/>
        <source>Updates</source>
        <translation>Atualizações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="515"/>
        <source>Update</source>
        <translation>Atualização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="517"/>
        <source>No new version of Jami was found</source>
        <translation>Você está usando a versão mais recente de Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="518"/>
        <source>An error occured when checking for a new version</source>
        <translation>Ocorreu um erro ao verificar por uma nova versão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="520"/>
        <source>SSL error</source>
        <translation>Erro de SSL</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="521"/>
        <source>Installer download canceled</source>
        <translation>Download do instalador cancelado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="523"/>
        <source>This will uninstall your current Release version and you can always download the latest Release version on our website</source>
        <translation>Isto irá desinstalar sua versão atual porém você sempre poderá baixar a última versão em nosso site</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="524"/>
        <source>Network disconnected</source>
        <translation>Rede desconectada</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="530"/>
        <source>Troubleshoot</source>
        <translation>Resolução de problemas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="531"/>
        <source>Open logs</source>
        <translation>Abrir logs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="532"/>
        <source>Get logs</source>
        <translation>Obter logs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="534"/>
        <source>(Experimental) Enable call support for swarm</source>
        <translation>(Experimental) Ativar suporte de chamada para enxame</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="535"/>
        <source>This feature will enable call buttons in swarms with multiple participants.</source>
        <translation>Este recurso permitirá os botões de chamada em enxames com múltiplos participantes.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="538"/>
        <source>Quality</source>
        <translation>Qualidade</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="542"/>
        <source>Always record calls</source>
        <translation>Sempre gravar as chamadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="545"/>
        <source>Keyboard Shortcut Table</source>
        <translation>Tabela de atalhos de teclado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="546"/>
        <source>Keyboard Shortcuts</source>
        <translation>Cortareta de teclado</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="547"/>
        <source>Conversation</source>
        <translation>Conversação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="548"/>
        <source>Call</source>
        <translation>Chamar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="549"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="553"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="555"/>
        <source>Report Bug</source>
        <translation>Report falha (bug)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="556"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="557"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="705"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="558"/>
        <source>Copied to clipboard!</source>
        <translation>Copiado para a área de transferência</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="559"/>
        <source>Receive Logs</source>
        <translation>Receber logs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="562"/>
        <source>Archive</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="563"/>
        <source>Open file</source>
        <translation>Abrir arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="566"/>
        <source>Generating account…</source>
        <translation>Criando a conta...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="567"/>
        <source>Import from archive backup</source>
        <translation>Importar de um arquivo de backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="569"/>
        <source>Select archive file</source>
        <translation>Selecionar arquivo de arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="437"/>
        <source>Link device</source>
        <translation>Associar dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="573"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="575"/>
        <source>A PIN is required to use an existing Jami account on this device.</source>
        <translation>É necessário um PIN para usar uma conta existente da Jami neste dispositivo.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="581"/>
        <source>Choose the account to link</source>
        <translation>Escolha a conta para ligar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="587"/>
        <source>The PIN and the account password should be entered in your device within 10 minutes.</source>
        <translation>O PIN e a senha da conta devem ser digitados no seu dispositivo dentro de 10 minutos.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="610"/>
        <source>Choose a picture</source>
        <translation>Escolha uma foto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="790"/>
        <source>Contact&apos;s name</source>
        <translation>Nome do contacto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="805"/>
        <source>Reinstate member</source>
        <translation>Reinstar membro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="819"/>
        <source>Delete message</source>
        <translation>Apagar mensagem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="820"/>
        <source>*(Deleted Message)*</source>
        <translation>*Mensagem apagada) *</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="821"/>
        <source>Edit message</source>
        <translation>Editar mensagem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="321"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="588"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="541"/>
        <source>Call recording</source>
        <translation>Gravação de chamadas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="570"/>
        <source>If the account is encrypted with a password, please fill the following field.</source>
        <translation>Se a conta estiver criptografada com uma senha, preencha o seguinte campo.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="574"/>
        <source>Enter the PIN code</source>
        <translation>Digite o código PIN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="576"/>
        <source>Step 01</source>
        <translation>Passo 01</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="577"/>
        <source>Step 02</source>
        <translation>Passo 02</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="578"/>
        <source>Step 03</source>
        <translation>Passo 03</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="579"/>
        <source>Step 04</source>
        <translation>Passo 04</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="580"/>
        <source>Go to the account management settings of a previous device</source>
        <translation>Vai para as configurações de gerenciamento de conta de um dispositivo anterior</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="583"/>
        <source>The PIN code will be available for 10 minutes</source>
        <translation>O código PIN estará disponível por 10 minutos.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="584"/>
        <source>Fill if the account is password-encrypted.</source>
        <translation>Preencha se a conta está criptografada por senha.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="591"/>
        <source>Add Device</source>
        <translation>Adicionar Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="601"/>
        <source>Enter current password</source>
        <translation>Digite a senha atual</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="603"/>
        <source>Enter new password</source>
        <translation>Digite a nova senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="604"/>
        <source>Confirm new password</source>
        <translation>Confirmar nova senha</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="605"/>
        <source>Change</source>
        <translation>Mudar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="606"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="611"/>
        <source>Import avatar from image file</source>
        <translation>Importar avatar do arquivo de imagem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="612"/>
        <source>Clear avatar image</source>
        <translation>Remover a imagem de avatar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="613"/>
        <source>Take photo</source>
        <translation>Tirar foto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="626"/>
        <source>Preferences</source>
        <translation>Preferências</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="629"/>
        <source>Reset</source>
        <translation>Resetar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="630"/>
        <source>Uninstall</source>
        <translation>Desinstalar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="631"/>
        <source>Reset Preferences</source>
        <translation>Preferências de Reset</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="632"/>
        <source>Select a plugin to install</source>
        <translation>Selecione um complemento para instalar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="633"/>
        <source>Uninstall plugin</source>
        <translation>Desinstalar o complemento</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="634"/>
        <source>Are you sure you wish to reset %1 preferences?</source>
        <translation>Tem a certeza de que deseja restabelecer as preferências %1?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="635"/>
        <source>Are you sure you wish to uninstall %1?</source>
        <translation>Você tem certeza de que deseja desinstalar %1?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="636"/>
        <source>Go back to plugins list</source>
        <translation>Voltar à lista de complementos</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="637"/>
        <source>Select a file</source>
        <translation>Selecione um arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="119"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="638"/>
        <source>Select</source>
        <translation>Selecionar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="639"/>
        <source>Choose image file</source>
        <translation>Escolha o arquivo de imagem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="640"/>
        <source>Plugin Files (*.jpl)</source>
        <translation>Arquivos de plugins (*.jpl)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="641"/>
        <source>Load/Unload</source>
        <translation>Carregar/Descarregar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="642"/>
        <source>Select An Image to %1</source>
        <translation>Selecione uma imagem para % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="643"/>
        <source>Edit preference</source>
        <translation>Editar as preferências</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="644"/>
        <source>On/Off</source>
        <translation>Ativar/Desativar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="645"/>
        <source>Choose Plugin</source>
        <translation>Escolha Plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="651"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="653"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="656"/>
        <source>Enter the account password to confirm the removal of this device</source>
        <translation>Digite a senha de conta para confirmar a remoção deste dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="659"/>
        <source>Select a screen to share</source>
        <translation>Selecione uma tela para compartilhar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="660"/>
        <source>Select a window to share</source>
        <translation>Selecione uma janela para compartilhar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="661"/>
        <source>All Screens</source>
        <translation>Todas as telas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="662"/>
        <source>Screens</source>
        <translation>Telas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="663"/>
        <source>Windows</source>
        <translation>Janelas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="664"/>
        <source>Screen %1</source>
        <translation>Ecrã %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="667"/>
        <source>QR code</source>
        <translation>Código QR</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="670"/>
        <source>Link this device to an existing account</source>
        <translation>Associar este dispositivo à uma conta existente</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="671"/>
        <source>Import from another device</source>
        <translation>Importação a partir de outro dispositivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="672"/>
        <source>Import from an archive backup</source>
        <translation>Importação de um arquivo de backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="673"/>
        <source>Advanced features</source>
        <translation>Recursos avançados</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="674"/>
        <source>Show advanced features</source>
        <translation>Mostrar os recursos avançados</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="675"/>
        <source>Hide advanced features</source>
        <translation>Esconder os recursos avançados</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="676"/>
        <source>Connect to a JAMS server</source>
        <translation>Conectar a um servidor JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="677"/>
        <source>Create account from Jami Account Management Server (JAMS)</source>
        <translation>Criar a conta do Servidor de Gestão de Conta de Jami (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="678"/>
        <source>Configure a SIP account</source>
        <translation>Configurar uma conta SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="679"/>
        <source>Error while creating your account. Check your credentials.</source>
        <translation>Erro ao criar sua conta. Verifique suas credenciais.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="680"/>
        <source>Create a rendezvous point</source>
        <translation>Criar um ponto de encontro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="681"/>
        <source>Join Jami</source>
        <translation>Junte-se a Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="682"/>
        <source>Create new Jami account</source>
        <translation>Criar uma nova conta de Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="683"/>
        <source>Create new SIP account</source>
        <translation>Criar uma nova conta de SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="684"/>
        <source>About Jami</source>
        <translation>Sobre Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="686"/>
        <source>I already have an account</source>
        <translation>Já tenho uma conta.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="687"/>
        <source>Use existing Jami account</source>
        <translation>Use a conta existente da Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="688"/>
        <source>Welcome to Jami</source>
        <translation>Bem-vindo ao Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="691"/>
        <source>Clear Text</source>
        <translation>Limpar texto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="692"/>
        <source>Conversations</source>
        <translation>Conversas</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="693"/>
        <source>Search Results</source>
        <translation>Resultados da Busca</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="696"/>
        <source>Decline contact request</source>
        <translation>Recusar o pedido de contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="697"/>
        <source>Accept contact request</source>
        <translation>Aceitar o pedido de contato</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="700"/>
        <source>Automatically check for updates</source>
        <translation>Verificar automaticamente se há atualizações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="703"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="463"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="704"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="706"/>
        <source>Upgrade</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="707"/>
        <source>Later</source>
        <translation>Mais tarde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="708"/>
        <source>Delete</source>
        <translation>Deletar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="710"/>
        <source>Block</source>
        <translation>Bloquear</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="714"/>
        <source>Set moderator</source>
        <translation>Definir moderador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="715"/>
        <source>Unset moderator</source>
        <translation>Moderador não definido</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="317"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="718"/>
        <source>Maximize</source>
        <translation>Maximizar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="719"/>
        <source>Minimize</source>
        <translation>Minimizar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="720"/>
        <source>Hangup</source>
        <translation>Desligar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="721"/>
        <source>Local muted</source>
        <translation>Seu microfone está desligado localmente</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="724"/>
        <source>Default moderators</source>
        <translation>Moderadores padrão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="725"/>
        <source>Enable local moderators</source>
        <translation>Ativar moderadores locais</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="726"/>
        <source>Make all participants moderators</source>
        <translation>Torne todos os participantes moderadores</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="727"/>
        <source>Add default moderator</source>
        <translation>Adicionar moderador padrão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="729"/>
        <source>Remove default moderator</source>
        <translation>Remover moderador padrão</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="736"/>
        <source>Add emoji</source>
        <translation>Adicionar emoji</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="738"/>
        <source>Send file</source>
        <translation>Enviar arquivo</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="760"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="466"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="709"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="41"/>
        <source>Migrate conversation</source>
        <translation>Migração conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="475"/>
        <source>Show notifications</source>
        <translation>Mostre notificações</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="476"/>
        <source>Minimize on close</source>
        <translation>Minimizar a proximidade</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="477"/>
        <source>Run at system startup</source>
        <translation>Execução no arranque do sistema</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="564"/>
        <source>Create account from backup</source>
        <translation>Criar uma conta a partir de backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="565"/>
        <source>Restore account from backup</source>
        <translation>Restaurar a conta do backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="568"/>
        <source>Import Jami account from local archive file.</source>
        <translation>Importa a conta Jami do arquivo local.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="614"/>
        <source>Image Files (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</source>
        <translation>Arquivos de imagem (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="766"/>
        <source>Write to %1</source>
        <translation>Escrever para %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="781"/>
        <source>%1 has sent you a request for a conversation.</source>
        <translation>%1 te enviou um pedido para uma conversação.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="782"/>
        <source>Hello,
Would you like to join the conversation?</source>
        <translation>Olá,
Você gostaria de entrar na conversação?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="783"/>
        <source>You have accepted
the conversation request</source>
        <translation>Você aceitou
o pedido de conversação</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="784"/>
        <source>Waiting until %1
connects to synchronize the conversation.</source>
        <translation>Aguardando até %1
conectar para sincronizar a conversação.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="787"/>
        <source>%1 Members</source>
        <translation>%1 Membros</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="788"/>
        <source>Member</source>
        <translation>Membro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="789"/>
        <source>Swarm&apos;s name</source>
        <translation>O nome do Enxame</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="791"/>
        <source>Add a description</source>
        <translation>Adicionar uma descrição</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="794"/>
        <source>Ignore all notifications from this conversation</source>
        <translation>Ignore todas as notificações desta conversa.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="795"/>
        <source>Choose a color</source>
        <translation>Escolha uma cor</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="797"/>
        <source>Leave conversation</source>
        <translation>Deixe a conversa</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="798"/>
        <source>Type of swarm</source>
        <translation>Tipo de enxame</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="802"/>
        <source>Create the swarm</source>
        <translation>Criar o enxame</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="803"/>
        <source>Go to conversation</source>
        <translation>Vai conversar.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="804"/>
        <source>Kick member</source>
        <translation>Membro de pontapé</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="806"/>
        <source>Administrator</source>
        <translation>Administrador</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="807"/>
        <source>Invited</source>
        <translation>Convidados</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="808"/>
        <source>Remove member</source>
        <translation>Remover membro</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="809"/>
        <source>To:</source>
        <translation>Para:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="812"/>
        <source>Customize</source>
        <translation>Personalização</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="814"/>
        <source>Dismiss</source>
        <translation>Dispensar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="816"/>
        <source>Your profile is only shared with your contacts</source>
        <translation>Seu perfil é compartilhado apenas com seus contatos</translation>
    </message>
</context>
<context>
    <name>KeyboardShortcutTable</name>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="41"/>
        <source>Open account list</source>
        <translation>Abrir a lista de contas</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="45"/>
        <source>Focus conversations list</source>
        <translation>Foco na lista de conversas</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="49"/>
        <source>Requests list</source>
        <translation>Lista de pedidos</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="53"/>
        <source>Previous conversation</source>
        <translation>Conversa anterior</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="57"/>
        <source>Next conversation</source>
        <translation>Próxima conversa</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="61"/>
        <source>Search bar</source>
        <translation>Barra de pesquisa</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="65"/>
        <source>Full screen</source>
        <translation>Tela inteira</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="69"/>
        <source>Increase font size</source>
        <translation>Aumentar o tamanho da fonte</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="73"/>
        <source>Decrease font size</source>
        <translation>Diminuição do tamanho da fonte</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="77"/>
        <source>Reset font size</source>
        <translation>Reset tamanho de fonte</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="85"/>
        <source>Start an audio call</source>
        <translation>Iniciar uma chamada de áudio</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="89"/>
        <source>Start a video call</source>
        <translation>Iniciar uma chamada de vídeo</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="93"/>
        <source>Clear history</source>
        <translation>Limpar histórico</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="97"/>
        <source>Search messages/files</source>
        <translation>Mensagens/arquivos de busca</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="101"/>
        <source>Block contact</source>
        <translation>Bloquear contato</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="105"/>
        <source>Remove conversation</source>
        <translation>Excluir uma conversa</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="109"/>
        <source>Accept contact request</source>
        <translation>Aceitar o pedido de contato</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="113"/>
        <source>Edit last message</source>
        <translation>Edição da última mensagem</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="117"/>
        <source>Cancel message edition</source>
        <translation>Cancelar edição de mensagem</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="125"/>
        <source>Media settings</source>
        <translation>Configurações de mídia</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="129"/>
        <source>General settings</source>
        <translation>Configurações gerais</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="133"/>
        <source>Account settings</source>
        <translation>Configurações da conta</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="137"/>
        <source>Plugin settings</source>
        <translation>Configurações do plug-in</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="141"/>
        <source>Open account creation wizard</source>
        <translation>Abrir o assistente de criação de conta</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="146"/>
        <source>Open keyboard shortcut table</source>
        <translation>Tabela de atalhos de teclado aberta</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="154"/>
        <source>Answer an incoming call</source>
        <translation>Responder a uma chamada recebida</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="158"/>
        <source>End call</source>
        <translation>Finalizar chamada</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="162"/>
        <source>Decline the call request</source>
        <translation>Declinar o pedido de chamada</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="166"/>
        <source>Mute microphone</source>
        <translation>Desativar microfone</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="170"/>
        <source>Stop camera</source>
        <translation>Aparelhos de câmara</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="174"/>
        <source>Take tile screenshot</source>
        <translation>Faça uma captura de tela de telhas</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="182"/>
        <source>Bold</source>
        <translation>Negrito</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="186"/>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="190"/>
        <source>Strikethrough</source>
        <translation>Tachado</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="194"/>
        <source>Heading</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="198"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="202"/>
        <source>Code</source>
        <translation>Código</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="206"/>
        <source>Quote</source>
        <translation>Citação</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="210"/>
        <source>Unordered list</source>
        <translation>Lista não ordenada</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="214"/>
        <source>Ordered list</source>
        <translation>Lista ordenada</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="218"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="222"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="375"/>
        <source>E&amp;xit</source>
        <translation>Sai&amp;r</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="377"/>
        <source>&amp;Quit</source>
        <translation>Sai&amp;r</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="383"/>
        <source>&amp;Show Jami</source>
        <translation>&amp;Mostrar Jami</translation>
    </message>
</context>
<context>
    <name>PositionManager</name>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="414"/>
        <source>%1 is sharing their location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="419"/>
        <source>Location sharing</source>
        <translation>Partilha de localização</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/libclient/qtwrapper/callmanager_wrap.h" line="458"/>
        <source>Me</source>
        <translation>Eu</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="62"/>
        <source>Hold</source>
        <translation>Aguarde</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="64"/>
        <source>Talking</source>
        <translation>Falando</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="66"/>
        <source>ERROR</source>
        <translation>ERRO</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="68"/>
        <source>Incoming</source>
        <translation>Entrada</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="70"/>
        <source>Calling</source>
        <translation>Chamando</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="373"/>
        <location filename="../src/libclient/api/call.h" line="72"/>
        <source>Connecting</source>
        <translation>Conectando</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="74"/>
        <source>Searching</source>
        <translation>Pesquisando</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="76"/>
        <source>Inactive</source>
        <translation>Inativo</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="390"/>
        <location filename="../src/libclient/api/call.h" line="78"/>
        <location filename="../src/libclient/api/call.h" line="84"/>
        <source>Finished</source>
        <translation>Terminado</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="80"/>
        <source>Timeout</source>
        <translation>Tempo esgotado</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="82"/>
        <source>Peer busy</source>
        <translation>Pessoa ocupada</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="86"/>
        <source>Communication established</source>
        <translation>Comunicação estabelecida</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="211"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="994"/>
        <source>Invitation received</source>
        <translation>Convite recebido</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="260"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="208"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="992"/>
        <source>Contact added</source>
        <translation>Contato adicionado</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="262"/>
        <source>%1 was invited to join</source>
        <translation>%1 foi convidado a aderir</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="264"/>
        <source>%1 joined</source>
        <translation>%1 ingressou</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="266"/>
        <source>%1 left</source>
        <translation>%1 saiu</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="268"/>
        <source>%1 was kicked</source>
        <translation>%1 foi expulso</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="270"/>
        <source>%1 was re-added</source>
        <translation>%1 foi adicionado novamente</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="359"/>
        <source>Private conversation created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="361"/>
        <source>Swarm created</source>
        <translation>Criado um enxame</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="174"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="180"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="990"/>
        <source>Outgoing call</source>
        <translation>Chamada efetuada</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="176"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="186"/>
        <source>Incoming call</source>
        <translation>Chamada recebida</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="182"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="988"/>
        <source>Missed outgoing call</source>
        <translation>Chamadas efetuadas perdidas</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="188"/>
        <source>Missed incoming call</source>
        <translation>Chamadas recebidas perdidas</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="198"/>
        <source>Join call</source>
        <translation>Juntar chamada</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="213"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="996"/>
        <source>Invitation accepted</source>
        <translation>Convite aceito</translation>
    </message>
    <message>
        <location filename="../src/libclient/avmodel.cpp" line="391"/>
        <location filename="../src/libclient/avmodel.cpp" line="410"/>
        <source>default</source>
        <translation>predefinição</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="72"/>
        <source>Null</source>
        <translation>Nulo</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="73"/>
        <source>Trying</source>
        <translation>Tentando</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="74"/>
        <source>Ringing</source>
        <translation>Tocando</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="75"/>
        <source>Being Forwarded</source>
        <translation>Está sendo encaminhado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="76"/>
        <source>Queued</source>
        <translation>Esperando</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="77"/>
        <source>Progress</source>
        <translation>Progresso</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="78"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="79"/>
        <source>Accepted</source>
        <translation>Aceito</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="80"/>
        <source>Multiple Choices</source>
        <translation>Múltiplas Escolhas</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="81"/>
        <source>Moved Permanently</source>
        <translation>Movido Permanentemente</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="82"/>
        <source>Moved Temporarily</source>
        <translation>Movido Temporariamente</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="83"/>
        <source>Use Proxy</source>
        <translation>Usar proxy</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="84"/>
        <source>Alternative Service</source>
        <translation>Serviço Alternativo</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="85"/>
        <source>Bad Request</source>
        <translation>Pedido Inadequado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="86"/>
        <source>Unauthorized</source>
        <translation>Não autorizado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="87"/>
        <source>Payment Required</source>
        <translation>Pagamento Requerido</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="88"/>
        <source>Forbidden</source>
        <translation>Proibido</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="89"/>
        <source>Not Found</source>
        <translation>Não Encontrado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="90"/>
        <source>Method Not Allowed</source>
        <translation>Método Não Permitido</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="91"/>
        <location filename="../src/libclient/callmodel.cpp" line="111"/>
        <source>Not Acceptable</source>
        <translation>Não Aceitável</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="92"/>
        <source>Proxy Authentication Required</source>
        <translation>Autenticação de Proxy Necessária</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="93"/>
        <source>Request Timeout</source>
        <translation>A requisição excedeu o tempo limite</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="94"/>
        <source>Gone</source>
        <translation>Perdido</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="95"/>
        <source>Request Entity Too Large</source>
        <translation>Pedido Muito Grande</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="96"/>
        <source>Request URI Too Long</source>
        <translation>Pedido de URI Muito Longo</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="97"/>
        <source>Unsupported Media Type</source>
        <translation>Mídia Não Suportada</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="98"/>
        <source>Unsupported URI Scheme</source>
        <translation>Tipo de URI Sem Suporte</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="99"/>
        <source>Bad Extension</source>
        <translation>Complemento Inadequado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="100"/>
        <source>Extension Required</source>
        <translation>Complemento Necessário</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="101"/>
        <source>Session Timer Too Small</source>
        <translation>Temporizador de Sessão Muito Pequeno</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="102"/>
        <source>Interval Too Brief</source>
        <translation>Intervalo Muito Curto</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="103"/>
        <source>Temporarily Unavailable</source>
        <translation>Temporariamente Indisponível</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="104"/>
        <source>Call TSX Does Not Exist</source>
        <translation>Chamada TSX Não Existe</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="105"/>
        <source>Loop Detected</source>
        <translation>Loop Detectado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="106"/>
        <source>Too Many Hops</source>
        <translation>Muitos Ressaltos</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="107"/>
        <source>Address Incomplete</source>
        <translation>Endereço Incompleto</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="108"/>
        <source>Ambiguous</source>
        <translation>Ambíguo</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="109"/>
        <source>Busy</source>
        <translation>Ocupado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="110"/>
        <source>Request Terminated</source>
        <translation>Pedido Terminado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="112"/>
        <source>Bad Event</source>
        <translation>Evento Inadequado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="113"/>
        <source>Request Updated</source>
        <translation>Pedido Atualizado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="114"/>
        <source>Request Pending</source>
        <translation>Pedido Pendente</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="115"/>
        <source>Undecipherable</source>
        <translation>Indecifrável</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="116"/>
        <source>Internal Server Error</source>
        <translation>Erro Interno do Servidor</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="117"/>
        <source>Not Implemented</source>
        <translation>Não Implementado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="118"/>
        <source>Bad Gateway</source>
        <translation>Gateway Inadequado</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="119"/>
        <source>Service Unavailable</source>
        <translation>Serviço Indisponível</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="120"/>
        <source>Server Timeout</source>
        <translation>Tempo do Servidor Esgotou</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="121"/>
        <source>Version Not Supported</source>
        <translation>Versão Não Suportada</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="122"/>
        <source>Message Too Large</source>
        <translation>Mensagem Muito Grande</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="123"/>
        <source>Precondition Failure</source>
        <translation>Requisito Insuficiente</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="124"/>
        <source>Busy Everywhere</source>
        <translation>Ocupado em Toda Parte</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="125"/>
        <source>Call Refused</source>
        <translation>Chamada Recusada</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="126"/>
        <source>Does Not Exist Anywhere</source>
        <translation>Não Existe em Nenhum Lugar</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="127"/>
        <source>Not Acceptable Anywhere</source>
        <translation>Não Aceitável em Qualquer Lugar</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="375"/>
        <source>Accept</source>
        <translation>Aceitar</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="367"/>
        <source>Sending</source>
        <translation>Enviando</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="371"/>
        <source>Sent</source>
        <translation>Enviado</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="380"/>
        <source>Unable to make contact</source>
        <translation>Não é possível fazer contato</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="384"/>
        <source>Waiting for contact</source>
        <translation>Aguardando pelo contato</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="386"/>
        <source>Incoming transfer</source>
        <translation>Recebendo transferência</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="388"/>
        <source>Timed out waiting for contact</source>
        <translation>O tempo de aguardo pelo contato chegou ao fim</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="742"/>
        <source>Today</source>
        <translation>Hoje</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="744"/>
        <source>Yesterday</source>
        <translation>Ontem</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="377"/>
        <source>Canceled</source>
        <translation>Cancelada</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="382"/>
        <source>Ongoing</source>
        <translation>Em andamento</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="723"/>
        <source>just now</source>
        <translation>Agora</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="369"/>
        <source>Failure</source>
        <translation>Falha</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="109"/>
        <source>locationServicesError</source>
        <translation>localizaçãoServiçosErro</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="112"/>
        <source>locationServicesClosedError</source>
        <translation>localizaçãoServiçosClosedErro</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="114"/>
        <source>locationServicesUnknownError</source>
        <translation>localizaçãoServiçosInconhecidoErro</translation>
    </message>
    <message>
        <location filename="../src/libclient/conversationmodel.cpp" line="1166"/>
        <location filename="../src/libclient/conversationmodel.cpp" line="1179"/>
        <source>%1 (you)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SmartListModel</name>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="68"/>
        <location filename="../src/app/smartlistmodel.cpp" line="107"/>
        <location filename="../src/app/smartlistmodel.cpp" line="115"/>
        <location filename="../src/app/smartlistmodel.cpp" line="161"/>
        <location filename="../src/app/smartlistmodel.cpp" line="191"/>
        <location filename="../src/app/smartlistmodel.cpp" line="192"/>
        <source>Calls</source>
        <translation>Chamadas</translation>
    </message>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="69"/>
        <location filename="../src/app/smartlistmodel.cpp" line="108"/>
        <location filename="../src/app/smartlistmodel.cpp" line="125"/>
        <location filename="../src/app/smartlistmodel.cpp" line="162"/>
        <location filename="../src/app/smartlistmodel.cpp" line="193"/>
        <location filename="../src/app/smartlistmodel.cpp" line="194"/>
        <source>Contacts</source>
        <translation>Contatos</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../src/app/systemtray.cpp" line="216"/>
        <source>Answer</source>
        <translation>Resposta</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="217"/>
        <source>Decline</source>
        <translation>Rejeitar</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="219"/>
        <source>Open conversation</source>
        <translation>Conversa em curso</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="221"/>
        <source>Accept</source>
        <translation>Aceitar</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="222"/>
        <source>Refuse</source>
        <translation>Recusar</translation>
    </message>
</context>
<context>
    <name>TipsModel</name>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="85"/>
        <source>Customize</source>
        <translation>Personalização</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="88"/>
        <source>What does Jami mean?</source>
        <translation>O que significa Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="94"/>
        <source>What is the green dot next to my account?</source>
        <translation>Qual é o ponto verde ao lado da minha conta?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="90"/>
        <source>The choice of the name Jami was inspired by the Swahili word &apos;jamii&apos;, which means &apos;community&apos; as a noun and &apos;together&apos; as an adverb.</source>
        <translation>A escolha do nome Jami foi inspirada na palavra swahili &apos;jamii&apos;, que significa &apos;comunidade&apos; como substantivo e &apos;juntos&apos; como adjetivo.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="81"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="86"/>
        <source>Backup account</source>
        <translation>Cópia de segurança da conta</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="96"/>
        <source>A red dot means that your account is disconnected from the network; it turns green when it&apos;s connected.</source>
        <translation>Um ponto vermelho significa que a sua conta está desconectada da rede; torna-se verde quando está conectada.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="101"/>
        <source>Why should I back up my account?</source>
        <translation>Porque é que eu devia fazer backup da minha conta?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="103"/>
        <source>Jami is distributed and your account is only stored locally on your device. If you lose your password or your local account data, you WILL NOT be able to recover your account if you did not back it up earlier.</source>
        <translation>Jami é distribuído e sua conta é armazenada apenas localmente no seu dispositivo. Se você perder sua senha ou seus dados da conta local, você NÃO poderá recuperar sua conta se não a fez backup antes.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="109"/>
        <source>Can I make a conference call?</source>
        <translation>Posso fazer uma chamada de conferência?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="114"/>
        <source>What is a Jami account?</source>
        <translation>O que é uma conta Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="116"/>
        <source>A Jami account is an asymmetric encryption key. Your account is identified by a Jami ID, which is a fingerprint of your public key.</source>
        <translation>Uma conta Jami é uma chave de criptografia assimétrica. Sua conta é identificada por um ID Jami, que é uma impressão digital da sua chave pública.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="120"/>
        <source>What information do I need to provide to create a Jami account?</source>
        <translation>Que informações preciso fornecer para criar uma conta da Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="122"/>
        <source>When you create a new Jami account, you do not have to provide any private information like an email, address, or phone number.</source>
        <translation>Quando você cria uma nova conta Jami, você não precisa fornecer nenhuma informação privada, como um e-mail, endereço ou número de telefone.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="129"/>
        <source>With Jami, your account is stored in a directory on your device. The password is only used to encrypt your account in order to protect you from someone who has physical access to your device.</source>
        <translation>Com Jami, sua conta é armazenada em um diretório no seu dispositivo. A senha é usada apenas para criptografar sua conta, a fim de protegê-lo de alguém que tem acesso físico ao seu dispositivo.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="149"/>
        <source>Your account is only stored on your own devices. If you delete your account from all of your devices, the account is gone forever and you CANNOT recover it.</source>
        <translation>Se você excluir sua conta de todos os seus dispositivos, a conta desaparecerá para sempre e você não poderá recuperá-la.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="153"/>
        <source>Can I use my account on multiple devices?</source>
        <translation>Posso usar a minha conta em vários dispositivos?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="155"/>
        <source>Yes, you can link your account from the settings, or you can import your backup on another device.</source>
        <translation>Sim, pode ligar a sua conta a partir das configurações, ou pode importar o seu backup em outro dispositivo.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="127"/>
        <source>Why don&apos;t I have to use a password?</source>
        <translation>Porque não tenho de usar uma senha?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="111"/>
        <source>In a call, you can click on &quot;Add participants&quot; to add a contact to a call.</source>
        <translation>Em uma chamada, você pode clicar em &quot;Adicionar participantes&quot; para adicionar um contato a uma chamada.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="135"/>
        <source>Why don&apos;t I have to register a username?</source>
        <translation>Porque não tenho de registrar um nome de usuário?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="137"/>
        <source>The most permanent, secure identifier is your Jami ID, but since these are difficult to use for some people, you also have the option of registering a username.</source>
        <translation>O identificador mais permanente e seguro é o seu ID Jami, mas como estes são difíceis de usar para algumas pessoas, você também tem a opção de registrar um nome de usuário.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="142"/>
        <source>How can I back up my account?</source>
        <translation>Como posso fazer backup da minha conta?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="143"/>
        <source>In Account Settings, a button is available to create a backup your account.</source>
        <translation>Em Configurações de Conta, um botão está disponível para criar um backup da sua conta.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="147"/>
        <source>What happens when I delete my account?</source>
        <translation>O que acontece quando eu excluir a minha conta?</translation>
    </message>
</context>
<context>
    <name>UtilsAdapter</name>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>%1 Mbps</source>
        <translation>%1 Mbps</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>Default</source>
        <translation>Predefinição</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="539"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="478"/>
        <source>Searching…</source>
        <translation>Buscando...</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1015"/>
        <source>Invalid ID</source>
        <translation>ID inválido</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1018"/>
        <source>Username not found</source>
        <translation>Nome de usuário não encontrado</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1021"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Não consegui procurar...</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="441"/>
        <source>Bad URI scheme</source>
        <translation>Mau esquema de URI</translation>
    </message>
</context>
</TS>