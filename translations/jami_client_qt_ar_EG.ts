<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ar_EG" sourcelanguage="en">
<context>
    <name>CallAdapter</name>
    <message>
        <location filename="../src/app/calladapter.cpp" line="220"/>
        <source>Missed call</source>
        <translation>مكالمة فائتة</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="221"/>
        <source>Missed call with %1</source>
        <translation>غياب المكالمة مع %1</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="542"/>
        <source>Incoming call</source>
        <translation>مكالمة واردة</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="543"/>
        <source>%1 is calling you</source>
        <translation>%1 يتصل بك</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="554"/>
        <source>is calling you</source>
        <translation>يتصل بك</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="1052"/>
        <source>Screenshot</source>
        <translation>لقطة شاشة</translation>
    </message>
</context>
<context>
    <name>ConversationsAdapter</name>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="189"/>
        <source>%1 received a new message</source>
        <translation>%1 تلقى رسالة جديدة</translation>
    </message>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="244"/>
        <source>%1 received a new trust request</source>
        <translation>%1 تلقى طلبًا جديدًا</translation>
    </message>
</context>
<context>
    <name>CurrentCall</name>
    <message>
        <location filename="../src/app/currentcall.cpp" line="185"/>
        <source>Me</source>
        <translation>أنا</translation>
    </message>
</context>
<context>
    <name>CurrentConversation</name>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="137"/>
        <source>Private</source>
        <translation>خاص</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="139"/>
        <source>Private group (restricted invites)</source>
        <translation>مجموعة خاصة (دعوات مقيدة)</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="141"/>
        <source>Private group</source>
        <translation>مجموعة خاصة</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="143"/>
        <source>Public group</source>
        <translation>مجموعة عامة</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="324"/>
        <source>An error occurred while fetching this repository</source>
        <translation>حدث خطأ أثناء استلام هذا المخبأ</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="326"/>
        <source>Unrecognized conversation mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="330"/>
        <source>Not authorized to update conversation information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="332"/>
        <source>An error occurred while committing a new message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="328"/>
        <source>An invalid message was detected</source>
        <translation>تم اكتشاف رسالة غير صالحة</translation>
    </message>
</context>
<context>
    <name>JamiStrings</name>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="29"/>
        <source>Accept</source>
        <translation>اقبل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="30"/>
        <source>Accept in audio</source>
        <translation>قبول مع الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="31"/>
        <source>Accept in video</source>
        <translation>قبول مع الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="32"/>
        <source>Refuse</source>
        <translation>أرفض</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="33"/>
        <source>End call</source>
        <translation>إنهاء المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="34"/>
        <source>Incoming audio call from {}</source>
        <translation>مكالمة صوتية واردة من {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="35"/>
        <source>Incoming video call from {}</source>
        <translation>مكالمة فيديو واردة من {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="38"/>
        <source>Invitations</source>
        <translation>الدعوات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="39"/>
        <source>Jami is a universal communication platform, with privacy as its foundation, that relies on a free distributed network for everyone.</source>
        <translation>جامي هي منصة اتصال عالمية، مع الخصوصية كأساسها، والتي تعتمد على شبكة مجانية مُوزعة للجميع.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="40"/>
        <source>Migrating to the Swarm technology will enable synchronizing this conversation across multiple devices and improve reliability. The legacy conversation history will be cleared in the process.</source>
        <translation>وسوف يتيح الانتقال إلى تكنولوجيا Swarm مزامنة هذه المحادثة عبر أجهزة متعددة وتحسين موثوقيتها. سيتم مسح تاريخ المحادثة القديمة في هذه العملية.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="44"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="733"/>
        <source>Could not re-connect to the Jami daemon (jamid).
Jami will now quit.</source>
        <translation>لم يستطع إعادة الاتصال بالشيطان (جامي) ، (جامي) ستستقيل الآن</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="45"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="732"/>
        <source>Trying to reconnect to the Jami daemon (jamid)…</source>
        <translation>أحاول إعادة الاتصال بالشيطان (جاميد)...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="48"/>
        <source>Version</source>
        <translation>الإصدار</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="51"/>
        <source>Jami is a free universal communication software that respects the freedom and privacy of its users.</source>
        <translation>جامي هو برنامج للتواصل العالمي المجاني الذي يحترم حرية وحرية خصوصية مستخدميه.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="54"/>
        <source>Display QR code</source>
        <translation>عرض رمز الاستجابة السريعة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="55"/>
        <source>Open settings</source>
        <translation>افتح الإعدادات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="56"/>
        <source>Close settings</source>
        <translation>أغلق الإعدادات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="57"/>
        <source>Add Account</source>
        <translation>إضافة حساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="60"/>
        <source>Add to conference</source>
        <translation>إضافة إلى المؤتمر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="61"/>
        <source>Add to conversation</source>
        <translation>أضافة إلى المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="62"/>
        <source>Transfer this call</source>
        <translation>تحويل هذه المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="63"/>
        <source>Transfer to</source>
        <translation>تحويل إلى</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="66"/>
        <source>Authentication required</source>
        <translation>المصادقة مطلوبة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="67"/>
        <source>Your session has expired or been revoked on this device. Please enter your password.</source>
        <translation>انتهت جلسةك أو تم إلغاءها على هذا الجهاز. يرجى إدخال كلمة المرور الخاصة بك.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="68"/>
        <source>JAMS server</source>
        <translation>خادم JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="69"/>
        <source>Authenticate</source>
        <translation> مصادقة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="70"/>
        <source>Delete account</source>
        <translation>حذف الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="71"/>
        <source>In progress…</source>
        <translation>في تَقَدم…</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="72"/>
        <source>Authentication failed</source>
        <translation>المصادقة فشلت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="73"/>
        <source>Password</source>
        <translation>كلمة السر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="74"/>
        <source>Username</source>
        <translation>اسم مستخدم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="75"/>
        <source>Alias</source>
        <translation>الاسم المستعار</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="78"/>
        <source>Allow incoming calls from unknown contacts</source>
        <translation>السماح بمكالمات واردة من جهات إتصال غير معروفة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="79"/>
        <source>Convert your account into a rendezvous point</source>
        <translation>حول حسابك إلى نقطة اجتماع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="80"/>
        <source>Automatically answer calls</source>
        <translation>الرد على المكالمات تلقائيا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="81"/>
        <source>Enable custom ringtone</source>
        <translation>تمكين نغمة رنين مخصصة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="82"/>
        <source>Select custom ringtone</source>
        <translation>حدد نغمة رنين مخصصة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="83"/>
        <source>Select a new ringtone</source>
        <translation>حدد نغمة رنين جديدة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="84"/>
        <source>Certificate File (*.crt)</source>
        <translation>ملف الشهادة (*.crt)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="85"/>
        <source>Audio File (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</source>
        <translation>ملف الصوتي (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="86"/>
        <source>Push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="87"/>
        <source>Enable push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="88"/>
        <source>Keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="89"/>
        <source>Change keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="92"/>
        <source>Change shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="93"/>
        <source>Press the key to be assigned to push-to-talk shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="94"/>
        <source>Assign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="97"/>
        <source>Enable read receipts</source>
        <translation>تمكين إيصالات القراءة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="98"/>
        <source>Send and receive receipts indicating that a message have been displayed</source>
        <translation>إرسال وإستلام الإيصالات التي تشير إلى أن رسالة قد تم عرضها</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="101"/>
        <source>Voicemail</source>
        <translation>البريد الصوتي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="102"/>
        <source>Voicemail dial code</source>
        <translation>رمز الرقم الصوتي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="105"/>
        <source>Security</source>
        <translation>تأمين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="106"/>
        <source>Enable SDES key exchange</source>
        <translation>تمكين تبادل مفتاح SDES</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="107"/>
        <source>Encrypt negotiation (TLS)</source>
        <translation>نظام تشفير التفاوض (TLS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="108"/>
        <source>CA certificate</source>
        <translation>شهادة CA</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="109"/>
        <source>User certificate</source>
        <translation>شهادة المستخدم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="110"/>
        <source>Private key</source>
        <translation>المفتاح الخاص</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="111"/>
        <source>Private key password</source>
        <translation>كلمة سر المفتاح الخاص</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="112"/>
        <source>Verify certificates for incoming TLS connections</source>
        <translation>التحقق من الشهادات لربط TLS الموصول</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="113"/>
        <source>Verify server TLS certificates</source>
        <translation>التحقق من شهادات TLS الخادم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="114"/>
        <source>Require certificate for incoming TLS connections</source>
        <translation>تطلب شهادة لربط TLS الموصول</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="116"/>
        <source>Select a private key</source>
        <translation>تحديد مفتاح خاص</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="117"/>
        <source>Select a user certificate</source>
        <translation>تحديد شهادة المستعمل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="118"/>
        <source>Select a CA certificate</source>
        <translation>تحديد شهادة CA</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="120"/>
        <source>Key File (*.key)</source>
        <translation>ملف المفتاح (*.key)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="123"/>
        <source>Connectivity</source>
        <translation>الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="124"/>
        <source>Auto Registration After Expired</source>
        <translation>تسجيل السيارات بعد انتهاء</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="125"/>
        <source>Registration expiration time (seconds)</source>
        <translation>وقت انتهاء صلاحية التسجيل (ثواني)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="126"/>
        <source>Network interface</source>
        <translation>واجهة الشبكة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="127"/>
        <source>Use UPnP</source>
        <translation>استخدم UPnP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="128"/>
        <source>Use TURN</source>
        <translation>إستعمل خادم TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="129"/>
        <source>TURN address</source>
        <translation>عنوان بروتوكول TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="130"/>
        <source>TURN username</source>
        <translation>اسم المستخدم لبروتوكول TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="131"/>
        <source>TURN password</source>
        <translation>كلمة المرور لبروتوكول TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="132"/>
        <source>TURN Realm</source>
        <translation>عالم التحول</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="133"/>
        <source>Use STUN</source>
        <translation>إستعمل خادم STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="134"/>
        <source>STUN address</source>
        <translation>عنوان STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="137"/>
        <source>Allow IP Auto Rewrite</source>
        <translation>السماح بإعادة كتابة البيانات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="138"/>
        <source>Public address</source>
        <translation>العنوان العام</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="139"/>
        <source>Use custom address and port</source>
        <translation>استخدام عنوان ونقطة عبور مخصصين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="140"/>
        <source>Address</source>
        <translation>العنوان</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="141"/>
        <source>Port</source>
        <translation>منفذ عبور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="144"/>
        <source>Media</source>
        <translation>وسائط</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="145"/>
        <source>Enable video</source>
        <translation>فعل الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="153"/>
        <source>SDP Session Negotiation (ICE Fallback)</source>
        <translation>جلسة تفاوض SDP (حل تعويض ICE)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="154"/>
        <source>Only used during negotiation in case ICE is not supported</source>
        <translation>تستخدم فقط أثناء التفاوض في حالة عدم دعم ICE</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="155"/>
        <source>Audio RTP minimum Port</source>
        <translation>الحد الأدنى من RTP الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="156"/>
        <source>Audio RTP maximum Port</source>
        <translation>أقصى RTP الصوتية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="157"/>
        <source>Video RTP minimum Port</source>
        <translation>الحد الأدنى من RTP الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="158"/>
        <source>Video RTP maximum port</source>
        <translation>المحطة القصوى لـ RTP الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="161"/>
        <source>Enable local peer discovery</source>
        <translation>تمكين اكتشاف النظراء المحلي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="162"/>
        <source>Connect to other DHT nodes advertising on your local network.</source>
        <translation>اتصل بـ عقدات DHT الأخرى التي تُعلن عن شبكتك المحلية.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="164"/>
        <source>Enable proxy</source>
        <translation>تفعيل البروكسي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="165"/>
        <source>Proxy address</source>
        <translation>عنوان النظام الأساسي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="166"/>
        <source>Bootstrap</source>
        <translation>تشغيل ذاتي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="169"/>
        <source>Back</source>
        <translation>السابق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="170"/>
        <source>Account</source>
        <translation>الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="171"/>
        <source>General</source>
        <translation>عام</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="172"/>
        <source>Extensions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="182"/>
        <source>Audio</source>
        <translation>صوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="183"/>
        <source>Microphone</source>
        <translation>الميكرفون</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="184"/>
        <source>Select audio input device</source>
        <translation>حدد جهاز إدخال الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="185"/>
        <source>Output device</source>
        <translation>جهاز الإخراج</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="186"/>
        <source>Select audio output device</source>
        <translation>تحديد جهاز إخراج الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="187"/>
        <source>Ringtone device</source>
        <translation>جهاز النغمات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="188"/>
        <source>Select ringtone output device</source>
        <translation>تحديد جهاز إخراج نغمة الرنين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="189"/>
        <source>Audio manager</source>
        <translation>مدير الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="193"/>
        <source>Video</source>
        <translation>فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="194"/>
        <source>Select video device</source>
        <translation>تحديد جهاز فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="195"/>
        <source>Device</source>
        <translation>جهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="196"/>
        <source>Resolution</source>
        <translation>دقة العرض (resolution)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="197"/>
        <source>Select video resolution</source>
        <translation>تحديد دقة الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="198"/>
        <source>Frames per second</source>
        <translation>إطارات في الثانية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="199"/>
        <source>Select video frame rate (frames per second)</source>
        <translation>تحديد معدل إطار الفيديو (الإطارات في الثانية)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="200"/>
        <source>Enable hardware acceleration</source>
        <translation>تمكين تسريع الأجهزة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="203"/>
        <source>Select screen sharing frame rate (frames per second)</source>
        <translation>حدد معدل إطارات مشاركة الشاشة ﴿إطارات في الثانية﴾</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="204"/>
        <source>no video</source>
        <translation>لا يوجد فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="209"/>
        <source>Back up account here</source>
        <translation>قم بتحميل الحساب هنا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="210"/>
        <source>Back up account</source>
        <translation>حساب النسخ الاحتياطي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="284"/>
        <source>Unavailable</source>
        <translation>غير متوفر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="311"/>
        <source>Turn off sharing</source>
        <translation>أوقف مشاركة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="312"/>
        <source>Stop location sharing in this conversation (%1)</source>
        <translation>توقف مشاركة الموقع في هذه المحادثة (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="335"/>
        <source>Hide chat</source>
        <translation>أُخفي المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="344"/>
        <source>Back to Call</source>
        <translation>العودة إلى الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="353"/>
        <source>Scroll to end of conversation</source>
        <translation>تمرير إلى نهاية المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="371"/>
        <source>You can choose a username to help others more easily find and reach you on Jami.</source>
        <translation>يمكنك اختيار اسم مستخدم لمساعدة الآخرين على إيجادك و الوصول إليك بسهولة على جامي.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="378"/>
        <source>Are you sure you would like to join Jami without a username?
If yes, only a randomly generated 40-character identifier will be assigned to this account.</source>
        <translation>هل أنت متأكد من أنك تود الانضمام إلى جامي دون اسم مستخدم؟ إذا كان ذلك، فسيتم تعيين معرف 40 حرفًا عشوائيًا لهذا الحساب.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="379"/>
        <source>- 32 characters maximum
- Alphabetical characters (A to Z and a to z)
- Numeric characters (0 to 9)
- Special characters allowed: dash (-)</source>
        <translation>- 32 حرفاً أقصى - حرفات أحرفية (A إلى Z و a إلى z) - حرفات رقمية (0 إلى 9) - حرفات خاصة مسموحة: دسر (-)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="385"/>
        <source>Your account will be created and stored locally.</source>
        <translation>سيتم إنشاء حسابك وتخزينه محليًا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="386"/>
        <source>Choosing a username is recommended, and a chosen username CANNOT be changed later.</source>
        <translation>يوصى باختيار اسم مستخدم ، ولا يمكن تغيير اسم المستخدم المختار لاحقًا.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="387"/>
        <source>Encrypting your account with a password is optional, and if the password is lost it CANNOT be recovered later.</source>
        <translation>تشفير حسابك بمشفرة أمر اختياري، وإذا فقدت كلمة المرور فإنها لا يمكن استعادتها في وقت لاحق.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="388"/>
        <source>Setting a profile picture and nickname is optional, and can also be changed later in the settings.</source>
        <translation>تعيين صورة ملف الشخصية واللقب اختياري، ويمكن أيضا تغييرها في وقت لاحق في الإعدادات.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="397"/>
        <source>TLS</source>
        <translation>TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="398"/>
        <source>UDP</source>
        <translation>UDP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="399"/>
        <source>Display Name</source>
        <translation>عرض الإسم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="403"/>
        <source>A chosen username can help to be found more easily on Jami.
If a username is not chosen, a randomly generated 40-character identifier will be assigned to this account as a username. It is more difficult to be found and reached with this identifier.</source>
        <translation>يمكن أن يساعد العثور على اسم مستخدم مختار بسهولة أكبر على جامي. إذا لم يتم اختيار اسم مستخدم، سيتم تعيين معرف 40 حرفًا يتم إنشاؤه عشوائياً لهذا الحساب كاسم مستخدم. من الصعب العثور عليه والوصول إليه مع هذا المعرف.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="406"/>
        <source>This Jami account exists only on this device.
The account will be lost if this device is lost or the application is uninstalled. It is recommended to make a backup of this account.</source>
        <translation>هذا الحساب Jami موجود فقط على هذا الجهاز. سيتم فقدان الحساب إذا فقد هذا الجهاز أو إزالة التطبيق. يوصى بإعداد نسخة احتياطية لهذا الحساب.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="421"/>
        <source>Encrypt account</source>
        <translation>تشفير الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="424"/>
        <source>Back up account to a .gz file</source>
        <translation>تشفير الحساب بكلمة مرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="430"/>
        <source>This profile is only shared with this account's contacts.
The profile can be changed at all times from the account&apos;s settings.</source>
        <translation>يتم مشاركة هذا الملف الشخصي فقط مع جهات الاتصال لهذا الحساب. يمكن تغيير الملف الشخصي في أي وقت من إعدادات الحساب.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="431"/>
        <source>Encrypt account with a password</source>
        <translation>تشفير الحساب بكلمة مرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="432"/>
        <source>A Jami account is created and stored locally only on this device, as an archive containing your account keys. Access to this archive can optionally be protected by a password.</source>
        <translation>يتم إنشاء حساب جامي وتخزينه محليا فقط على هذا الجهاز ، كملف يحتوي على مفاتيح حسابك. يمكن الوصول إلى هذا الملف بشكل اخيار حماية بواسطة كلمة مرور.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="433"/>
        <source>Please note that if you lose your password, it CANNOT be recovered!</source>
        <translation>يرجى ملاحظة أنه إذا فقدت كلمة المرور الخاصة بك، فإنه لا يمكن استعادتها!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="459"/>
        <source>Would you really like to delete this account?</source>
        <translation>هل ترغب حقا في حذف هذا الحساب؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="460"/>
        <source>If your account has not been backed up or added to another device, your account and registered username will be IRREVOCABLY LOST.</source>
        <translation>إذا لم يتم احتياط النسخة الاحتياطية لحسابك أو إضافةها إلى جهاز آخر، فسوف يتم فقدان حسابك واسم المستخدم المسجل بشكل غير قابل للاستعادة.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="471"/>
        <source>Dark</source>
        <translation>ليلي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="472"/>
        <source>Light</source>
        <translation>الضوء</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="480"/>
        <source>Include local video in recording</source>
        <translation>إضافة الفيديو المحلي في التسجيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="481"/>
        <source>Default settings</source>
        <translation>إعدادات الافتراضية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="484"/>
        <source>Enable typing indicators</source>
        <translation>تمكين مؤشرات الكتابة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="485"/>
        <source>Send and receive typing indicators showing that a message is being typed.</source>
        <translation>إرسال مؤشرات الكتابة واستلامها إظهار أن الرسالة تتم كتابتها</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="486"/>
        <source>Show link preview in conversations</source>
        <translation>عرض عرض المراسلة المسبقة في المحادثات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="508"/>
        <source>Delete file from device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="525"/>
        <source>Content access error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="526"/>
        <source>Content not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="589"/>
        <source>Enter account password</source>
        <translation>أدخل كلمة مرور الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="590"/>
        <source>This account is password encrypted, enter the password to generate a PIN code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="592"/>
        <source>PIN expired</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="593"/>
        <source>On another device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="594"/>
        <source>Install and launch Jami, select &quot;Import from another device&quot; and scan the QR code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="595"/>
        <source>Link new device</source>
        <translation>اربط جهاز جديد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="596"/>
        <source>In Jami, scan QR code or manually enter the PIN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="597"/>
        <source>The PIN code is valid for: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="600"/>
        <source>Enter password</source>
        <translation>أدخِل كلمة المرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="602"/>
        <source>Enter account password to confirm the removal of this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="742"/>
        <source>Show less</source>
        <translation>إظهار أقل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="744"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="745"/>
        <source>Continue editing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="748"/>
        <source>Strikethrough</source>
        <translation>يتوسطه خط</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="754"/>
        <source>Unordered list</source>
        <translation>قائمة غير مرتبة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="755"/>
        <source>Ordered list</source>
        <translation>قائمة مرتبة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="758"/>
        <source>Press Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="759"/>
        <source>Press Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="772"/>
        <source>Select dedicated device for hosting future calls in this swarm. If not set, the host will be the device starting a call.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="773"/>
        <source>Select this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="774"/>
        <source>Select device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="833"/>
        <source>Appearance</source>
        <translation>المظهر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="852"/>
        <source>Free and private sharing. &lt;a href=&quot;https://jami.net/donate/&quot;&gt;Donate&lt;/a&gt; to expand it.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="853"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="854"/>
        <source>If you enjoy using Jami and believe in our mission, would you make a donation?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="855"/>
        <source>Not now</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="856"/>
        <source>Enable donation campaign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="859"/>
        <source>Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="860"/>
        <source>Shift+Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="861"/>
        <source>Enter or Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="875"/>
        <source>View</source>
        <translation>عرض</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="862"/>
        <source>Text formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="865"/>
        <source>Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="866"/>
        <source>Connecting TLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="867"/>
        <source>Connecting ICE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="868"/>
        <source>Connecting</source>
        <translation>يتم الإتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="869"/>
        <source>Waiting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="870"/>
        <source>Contact</source>
        <translation>إتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="871"/>
        <source>Connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="872"/>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="873"/>
        <source>Copy all data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="874"/>
        <source>Remote: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="496"/>
        <source>Accept transfer limit (in Mb)</source>
        <translation>القبول في الحد الأقصى من التحويل (في مباني)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="516"/>
        <source>A new version of Jami was found
Would you like to update now?</source>
        <translation>تم العثور على نسخة جديدة من جامي هل تود تحديثها الآن؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="539"/>
        <source>Save recordings to</source>
        <translation>حفظ التسجيلات إلى</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="540"/>
        <source>Save screenshots to</source>
        <translation>حفظ صور الشاشة إلى</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="582"/>
        <source>Select &quot;Link another device&quot;</source>
        <translation>اختر &quot;ربط جهاز آخر&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="609"/>
        <source>Choose a picture as your avatar</source>
        <translation>اختر صورة كآفاتارك</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="685"/>
        <source>Share freely and privately with Jami</source>
        <translation>شاركها بحرية وخاصة مع جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="711"/>
        <source>Unban</source>
        <translation>إعلان</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="728"/>
        <source>Add</source>
        <translation>إضافة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="737"/>
        <source>more emojis</source>
        <translation>المزيد من الايموجيز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="761"/>
        <source>Reply to</source>
        <translation>الإجابة على</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="762"/>
        <source>In reply to</source>
        <translation>في جواب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="763"/>
        <source> replied to</source>
        <translation>أجاب على</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="765"/>
        <source>Reply</source>
        <translation>رد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="464"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="767"/>
        <source>Edit</source>
        <translation>تصحيح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="768"/>
        <source>Edited</source>
        <translation>تحرير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="769"/>
        <source>Join call</source>
        <translation>إلتحق بمكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="770"/>
        <source>A call is in progress. Do you want to join the call?</source>
        <translation>مكالمة جارية هل تريد الانضمام إلى المكالمة؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="771"/>
        <source>Current host for this swarm seems unreachable. Do you want to host the call?</source>
        <translation>المضيف الحالي لهذه السلالة يبدو غير قابل للدخول هل تريد أن تستضيف المكالمة؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="775"/>
        <source>Remove current device</source>
        <translation>إزالة الجهاز الحالي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="776"/>
        <source>Host only this call</source>
        <translation>استضيف هذه المكالمة فقط</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="777"/>
        <source>Host this call</source>
        <translation>استضافة هذه المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="778"/>
        <source>Make me the default host for future calls</source>
        <translation>اجعلني المضيف الافتراضي للمكالمات المستقبلية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="793"/>
        <source>Mute conversation</source>
        <translation>محادثة صامتة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="796"/>
        <source>Default host (calls)</source>
        <translation>المضيف الافتراضي (المكالمات)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="799"/>
        <source>None</source>
        <translation>لا يوجد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="813"/>
        <source>Tip</source>
        <translation>النقش</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="815"/>
        <source>Add a profile picture and nickname to complete your profile</source>
        <translation>إضافة صورة ملف الشخصية واللقب لإكمال ملف الشخصية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="36"/>
        <source>Start swarm</source>
        <translation>أبدأوا السحابة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="37"/>
        <source>Create swarm</source>
        <translation>إنشاء السحابة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="176"/>
        <source>Call settings</source>
        <translation>إعدادات الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="115"/>
        <source>Disable secure dialog check for incoming TLS data</source>
        <translation>إعاقة التحقق الآمن من الحوار للبيانات TLS الموصلة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="146"/>
        <source>Video codecs</source>
        <translation>مرمّزات الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="147"/>
        <source>Audio codecs</source>
        <translation>مرمّزات الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="150"/>
        <source>Name server</source>
        <translation>اسم خادم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="163"/>
        <source>OpenDHT configuration</source>
        <translation>تهيئة OpenDHT</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="201"/>
        <source>Mirror local video</source>
        <translation>مرآة الفيديو المحلي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="207"/>
        <source>Why should I back-up this account?</source>
        <translation>لماذا يجب أن أؤكد هذا الحساب؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="211"/>
        <source>Success</source>
        <translation>نجاح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="212"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="213"/>
        <source>Jami archive files (*.gz)</source>
        <translation>ملفات أرشيف جامي (*.gz)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="214"/>
        <source>All files (*)</source>
        <translation>جميع الملفات (*)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="217"/>
        <source>Reinstate as contact</source>
        <translation>استعادة كوسيلة اتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="218"/>
        <source>name</source>
        <translation>الاسم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="219"/>
        <source>Identifier</source>
        <translation>المعرف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="222"/>
        <source>is recording</source>
        <translation>هو تسجيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="223"/>
        <source>are recording</source>
        <translation>يتم تسجيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="224"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="716"/>
        <source>Mute</source>
        <translation>كتم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="225"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="717"/>
        <source>Unmute</source>
        <translation>إعادة الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="226"/>
        <source>Pause call</source>
        <translation>وقفة المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="227"/>
        <source>Resume call</source>
        <translation>استئناف المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="228"/>
        <source>Mute camera</source>
        <translation>كاميرا صامتة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="229"/>
        <source>Unmute camera</source>
        <translation>كاميرا غير محاكمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="230"/>
        <source>Add participant</source>
        <translation>أضف مشارك</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="231"/>
        <source>Add participants</source>
        <translation>إضافة المشاركين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="232"/>
        <source>Details</source>
        <translation>التفاصيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="177"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="233"/>
        <source>Chat</source>
        <translation>محادثة كتابية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="174"/>
        <source>Manage account</source>
        <translation>إدارة الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="175"/>
        <source>Linked devices</source>
        <translation>الأجهزة المتصلة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="178"/>
        <source>Advanced settings</source>
        <translation>إعدادات متقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="179"/>
        <source>Audio and Video</source>
        <translation>الصوت والفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="190"/>
        <source>Sound test</source>
        <translation>اختبار الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="202"/>
        <source>Screen sharing</source>
        <translation>مشاركة الشاشة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="208"/>
        <source>Your account only exists on this device. If you lose your device or uninstall the application, your account will be deleted and CANNOT be recovered. You can &lt;a href=&apos;blank&apos;&gt; back up your account &lt;/a&gt; now or later (in the Account Settings).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="234"/>
        <source>More options</source>
        <translation>المزيد من الخيارات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="235"/>
        <source>Mosaic</source>
        <translation>الموسيقي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="236"/>
        <source>Participant is still muted on their device</source>
        <translation>المشارك لا يزال يتوقف عن التواصل مع جهازهم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="237"/>
        <source>You are still muted on your device</source>
        <translation>ما زلتِ غائبة على جهازكِ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="238"/>
        <source>You are still muted by moderator</source>
        <translation>ما زلتِ تُخفّفين من قبل المُدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="239"/>
        <source>You are muted by a moderator</source>
        <translation>أنت تمت إغلاق صوتك من قبل المدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="240"/>
        <source>Moderator</source>
        <translation>المُتَدَلِّل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="241"/>
        <source>Host</source>
        <translation>المضيف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="242"/>
        <source>Local and Moderator muted</source>
        <translation>المحلي و المدير غامض</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="243"/>
        <source>Moderator muted</source>
        <translation>المُتَحَاكِمُ مُتَأَمّم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="244"/>
        <source>Not muted</source>
        <translation>لا يُغلق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="245"/>
        <source>On the side</source>
        <translation>على الجانب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="246"/>
        <source>On the top</source>
        <translation>على القمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="247"/>
        <source>Hide self</source>
        <translation>اخفي نفسك</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="248"/>
        <source>Hide spectators</source>
        <translation>إخفاء المشاهدين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="251"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="554"/>
        <source>Copy</source>
        <translation>نسخ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="252"/>
        <source>Share</source>
        <translation>مشاركة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="253"/>
        <source>Cut</source>
        <translation>إقطتاع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="254"/>
        <source>Paste</source>
        <translation>لصق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="257"/>
        <source>Start video call</source>
        <translation>بدء مكالمة فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="258"/>
        <source>Start audio call</source>
        <translation>بدأ مكالمة صوتية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="259"/>
        <source>Clear conversation</source>
        <translation>مسح المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="260"/>
        <source>Confirm action</source>
        <translation>تأكيد الإجراء</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="261"/>
        <source>Remove conversation</source>
        <translation>إزالة المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="262"/>
        <source>Would you really like to remove this conversation?</source>
        <translation>هل تود حقاً إزالة هذه المحادثة؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="263"/>
        <source>Would you really like to block this conversation?</source>
        <translation>هل حقاً تريد منع هذه المحادثة؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="264"/>
        <source>Remove contact</source>
        <translation>إزالة الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="265"/>
        <source>Block contact</source>
        <translation>حظر جهة إتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="266"/>
        <source>Conversation details</source>
        <translation>تفاصيل المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="267"/>
        <source>Contact details</source>
        <translation>تفاصيل جهة الأتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="270"/>
        <source>Sip input panel</source>
        <translation>لوحة دخول</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="271"/>
        <source>Transfer call</source>
        <translation>دعوة نقل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="272"/>
        <source>Stop recording</source>
        <translation>إيقاف التسجيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="273"/>
        <source>Start recording</source>
        <translation>ابدأ التسجيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="274"/>
        <source>View full screen</source>
        <translation>طريقة عرض ملء الشاشة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="275"/>
        <source>Share screen</source>
        <translation>مشاركة الشاشة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="276"/>
        <source>Share window</source>
        <translation>مشاركة النافذة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="277"/>
        <source>Stop sharing screen or file</source>
        <translation>توقف مشاركة الشاشة أو الملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="278"/>
        <source>Share screen area</source>
        <translation>تقاسم منطقة من الشاشة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="279"/>
        <source>Share file</source>
        <translation>مشاركة ملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="280"/>
        <source>Select sharing method</source>
        <translation>اختر طريقة مشاركة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="281"/>
        <source>View plugin</source>
        <translation>عرض المكونات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="282"/>
        <source>Advanced information</source>
        <translation>معلومات متقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="283"/>
        <source>No video device</source>
        <translation>لا يوجد جهاز فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="285"/>
        <source>Lower hand</source>
        <translation>اليد السفلى</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="286"/>
        <source>Raise hand</source>
        <translation>رفع اليد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="287"/>
        <source>Layout settings</source>
        <translation>إعدادات التخطيط</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="288"/>
        <source>Take tile screenshot</source>
        <translation>خذ صورة شاشة من الطلاء</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="289"/>
        <source>Screenshot saved to %1</source>
        <translation>صور الشاشة المحفوظة إلى % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="290"/>
        <source>File saved to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="293"/>
        <source>Renderers information</source>
        <translation>معلومات المقدمين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="294"/>
        <source>Call information</source>
        <translation>معلومات الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="295"/>
        <source>Peer number</source>
        <translation>رقم الأقران</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="296"/>
        <source>Call id</source>
        <translation>رقم الهاتف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="297"/>
        <source>Sockets</source>
        <translation>المفاصل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="298"/>
        <source>Video codec</source>
        <translation>ملف الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="299"/>
        <source>Hardware acceleration</source>
        <translation>تسريع الأجهزة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="300"/>
        <source>Video bitrate</source>
        <translation>معدل بت الفيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="301"/>
        <source>Audio codec</source>
        <translation>ملفات الصوت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="302"/>
        <source>Renderer id</source>
        <translation>هوية المُسجل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="303"/>
        <source>Fps</source>
        <translation>أوقات السفر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="306"/>
        <source>Share location</source>
        <translation>مشاركة الموقع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="307"/>
        <source>Stop sharing</source>
        <translation>إيقاف المشاركة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="308"/>
        <source>Your precise location could not be determined.
In Device Settings, please turn on &quot;Location Services&quot;.
Other participants&apos; location can still be received.</source>
        <translation>لم يتم تحديد موقعك الدقيق. في إعدادات الجهاز، يرجى تشغيل &quot;خدمات الموقع&quot;. لا يزال بإمكانك استلام موقع المشاركين الآخرين.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="309"/>
        <source>Your precise location could not be determined. Please check your Internet connection.</source>
        <translation>لم يتم تحديد موقعك الدقيق، من فضلك تحقق من اتصالك بالإنترنت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="310"/>
        <source>Turn off location sharing</source>
        <translation>إيقاف مشاركة الموقع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="313"/>
        <source>Location is shared in several conversations</source>
        <translation>الموقع يشارك في العديد من المحادثات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="314"/>
        <source>Pin map to be able to share location or to turn off location in specific conversations</source>
        <translation>خريطة الحزمة لتكون قادرة على مشاركة الموقع أو إيقاف الموقع في محادثات محددة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="315"/>
        <source>Location is shared in several conversations, click to choose how to turn off location sharing</source>
        <translation>يتم مشاركة الموقع في العديد من المحادثات ، انقر على اختيار كيفية إيقاف مشاركة الموقع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="316"/>
        <source>Share location to participants of this conversation (%1)</source>
        <translation>مشاركة الموقع للمشاركين في هذه المحادثة (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="318"/>
        <source>Reduce</source>
        <translation>تقليص</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="319"/>
        <source>Drag</source>
        <translation>سحب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="320"/>
        <source>Center</source>
        <translation>المركز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="322"/>
        <source>Unpin</source>
        <translation>إزالة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="323"/>
        <source>Pin</source>
        <translation>الـ &quot;بين&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="324"/>
        <source>Position share duration</source>
        <translation>مدة حصة الموقف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="325"/>
        <source>Location sharing</source>
        <translation>مشاركة الموقع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="326"/>
        <source>Unlimited</source>
        <translation>لا حدود</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="327"/>
        <source>1 min</source>
        <translation>1 دقيقة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="328"/>
        <source>%1h%2min</source>
        <translation>%1h%2min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="329"/>
        <source>%1h</source>
        <translation>%1h</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="330"/>
        <source>%1min%2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="331"/>
        <source>%1min</source>
        <translation>%1 دقيقة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="332"/>
        <source>%sec</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="336"/>
        <source>Place audio call</source>
        <translation>إجراء مكالمة صوتية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="337"/>
        <source>Place video call</source>
        <translation>إجراء مكالمة فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="338"/>
        <source>Show available plugins</source>
        <translation>عرض المكونات المتاحة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="339"/>
        <source>Add to conversations</source>
        <translation>إضافة إلى المحادثات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="340"/>
        <source>This is the error from the backend: %0</source>
        <translation>هذا هو الخطأ من الخلفية: %0</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="341"/>
        <source>The account is disabled</source>
        <translation>الحساب غير فعال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="342"/>
        <source>No network connectivity</source>
        <translation>لا يوجد اتصال بالشبكة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="343"/>
        <source>Deleted message</source>
        <translation>رسالة مُحذفة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="347"/>
        <source>Jump to</source>
        <translation>قفز إلى</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="348"/>
        <source>Messages</source>
        <translation>رسائل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="349"/>
        <source>Files</source>
        <translation>الملفات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="350"/>
        <source>Search</source>
        <translation>إبحث</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="354"/>
        <source>{} is typing…</source>
        <translation>{} هو الكتابة...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="355"/>
        <source>{} are typing…</source>
        <translation>{} يكتبون...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="356"/>
        <source>Several people are typing…</source>
        <translation>العديد من الناس يكتبون...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="357"/>
        <source> and </source>
        <translation>و</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="360"/>
        <source>Enter the Jami Account Management Server (JAMS) URL</source>
        <translation>أدخل عنوان عنوان (JAMS) لخادم إدارة الحسابات Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="361"/>
        <source>Jami Account Management Server URL</source>
        <translation>عنوان عنوان خادم إدارة الحسابات Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="362"/>
        <source>Enter JAMS credentials</source>
        <translation>إدخال إثباتات JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="363"/>
        <source>Connect</source>
        <translation>إتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="364"/>
        <source>Creating account…</source>
        <translation>أنشاء حساب…</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="367"/>
        <source>Choose name</source>
        <translation>اختر الاسم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="368"/>
        <source>Choose username</source>
        <translation>اختر اسم المستخدم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="369"/>
        <source>Choose a username</source>
        <translation>اختر اسم مستخدم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="428"/>
        <source>Encrypt account with password</source>
        <translation>تشفير حساب بكلمة مرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="370"/>
        <source>Confirm password</source>
        <translation>تأكيد كلمة المرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="372"/>
        <source>Choose a name for your rendezvous point</source>
        <translation>اختر اسماً لنقطة الاجتماع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="373"/>
        <source>Choose a name</source>
        <translation>اختر اسم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="374"/>
        <source>Invalid name</source>
        <translation>الاسم غير صالح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="375"/>
        <source>Invalid username</source>
        <translation>اسم مستخدم غير صالح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="376"/>
        <source>Name already taken</source>
        <translation>الاسم المستخدم بالفعل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="377"/>
        <source>Username already taken</source>
        <translation>اسم المستخدم مستخدم بالفعل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="382"/>
        <source>Good to know</source>
        <translation>من الجيد أن أعرف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="383"/>
        <source>Local</source>
        <translation>المحلية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="384"/>
        <source>Encrypt</source>
        <translation>تشفير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="391"/>
        <source>SIP account</source>
        <translation>حساب SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="392"/>
        <source>Proxy</source>
        <translation>الوكيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="393"/>
        <source>Server</source>
        <translation>الخادم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="394"/>
        <source>Configure an existing SIP account</source>
        <translation>إعداد حساب SIP موجود</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="395"/>
        <source>Personalize account</source>
        <translation>إضفاء شخصية على الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="396"/>
        <source>Add SIP account</source>
        <translation>إضافة حساب SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="402"/>
        <source>Your profile is only shared with your contacts.
Your picture and your nickname can be changed at all time in the settings of your account.</source>
        <translation>يتم مشاركة ملفك الشخصي فقط مع جهات الاتصال الخاصة بك. يمكن تغيير صورتك ولقبتك في أي وقت في إعدادات حسابك.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="404"/>
        <source>Your Jami account is registered only on this device as an archive containing the keys of your account. Access to this archive can be protected by a password.</source>
        <translation>يتم تسجيل حساب جامي الخاص بك فقط على هذا الجهاز كملف يحتوي على مفاتيح حسابك. يمكن حماية الوصول إلى هذا الملف بواسطة كلمة مرور.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="405"/>
        <source>Backup account</source>
        <translation>حساب احتياطي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="407"/>
        <source>Delete your account</source>
        <translation>إزالة حسابك</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="408"/>
        <source>If your account has not been backed up or added to another device, your account and registered name will be irrevocably lost.</source>
        <translation>إذا لم يتم احتياط النسخة الاحتياطية لحسابك أو إضافةها إلى جهاز آخر، فسيتم فقدان حسابك والاسم المسجل بشكل لا رجعة فيه.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="409"/>
        <source>List of the devices that are linked to this account:</source>
        <translation>قائمة الأجهزة المرتبطة بهذا الحساب:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="410"/>
        <source>This device</source>
        <translation>هذا الجهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="411"/>
        <source>Other linked devices</source>
        <translation>الأجهزة الأخرى المرتبطة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="414"/>
        <source>Backup successful</source>
        <translation>النسخة الاحتياطية ناجحة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="415"/>
        <source>Backup failed</source>
        <translation>فشل النسخة الاحتياطية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="416"/>
        <source>Password changed successfully</source>
        <translation>تم تغيير كلمة السر بنجاح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="417"/>
        <source>Password change failed</source>
        <translation>فشل تغيير كلمة السر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="418"/>
        <source>Password set successfully</source>
        <translation>تم تعيين كلمة المرور بنجاح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="419"/>
        <source>Password set failed</source>
        <translation>فشل تعيين كلمة المرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="420"/>
        <source>Change password</source>
        <translation>تغيير كلمة المرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="434"/>
        <source>Enter a nickname, surname…</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="435"/>
        <source>Use this account on other devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="436"/>
        <source>This account is created and stored locally, if you want to use it on another device you have to link the new device to this account.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="465"/>
        <source>Device name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="550"/>
        <source>Markdown</source>
        <translation>(ماركداون)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="617"/>
        <source>Auto update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="618"/>
        <source>Disable all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="619"/>
        <source>Installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="620"/>
        <source>Install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="621"/>
        <source>Installing</source>
        <translation>التثبيت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="622"/>
        <source>Install manually</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="623"/>
        <source>Install an extension directly from your device.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="624"/>
        <source>Available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="625"/>
        <source>Plugins store is not available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="627"/>
        <source>Installation failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="628"/>
        <source>The installation of the plugin failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="646"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="647"/>
        <source>Last update %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="648"/>
        <source>By %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="649"/>
        <source>Proposed by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="652"/>
        <source>More information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="739"/>
        <source>Audio message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="740"/>
        <source>Video message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="741"/>
        <source>Show more</source>
        <translation>إظهار المزيد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="746"/>
        <source>Bold</source>
        <translation>غامق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="747"/>
        <source>Italic</source>
        <translation>مائل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="749"/>
        <source>Title</source>
        <translation>العنوان</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="750"/>
        <source>Heading</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="751"/>
        <source>Link</source>
        <translation>ربط</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="752"/>
        <source>Code</source>
        <translation>رمز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="753"/>
        <source>Quote</source>
        <translation>اقتباس</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="756"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="757"/>
        <source>Hide formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="824"/>
        <source>Share your Jami identifier in order to be contacted more easily!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="825"/>
        <source>Jami identity</source>
        <translation>هوية جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="826"/>
        <source>Show fingerprint</source>
        <translation>إظهار بصمات الأصابع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="827"/>
        <source>Show registered name</source>
        <translation>إظهار الاسم المسجل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="830"/>
        <source>Enabling your account allows you to be contacted on Jami</source>
        <translation>إعطاء الحساب الخاص بك يسمح لك بالاتصال على جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="836"/>
        <source>Experimental</source>
        <translation>التجربة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="839"/>
        <source>Ringtone</source>
        <translation>النغمات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="842"/>
        <source>Rendezvous point</source>
        <translation>نقطة الإلتقاء</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="845"/>
        <source>Moderation</source>
        <translation>الاعتدال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="848"/>
        <source>Theme</source>
        <translation>الموضوع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="849"/>
        <source>Text zoom level</source>
        <translation>مستوى زيادة النص</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="422"/>
        <source>Set a password</source>
        <translation>حدد كلمة المرور</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="423"/>
        <source>Change current password</source>
        <translation>تغيير كلمة المرور الحالية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="425"/>
        <source>Display advanced settings</source>
        <translation>عرض الإعدادات المتقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="426"/>
        <source>Hide advanced settings</source>
        <translation>إخفاء الإعدادات المتقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="173"/>
        <source>Enable account</source>
        <translation>تمكين الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="427"/>
        <source>Advanced account settings</source>
        <translation>إعدادات الحساب المتقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="429"/>
        <source>Customize profile</source>
        <translation>تخصيص الملف الشخصي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="440"/>
        <source>Set username</source>
        <translation>إعداد اسم المستخدم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="441"/>
        <source>Registering name</source>
        <translation>الاسم المسجل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="444"/>
        <source>Identity</source>
        <translation>الهوية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="447"/>
        <source>Link a new device to this account</source>
        <translation>ربط جهاز جديد بهذا الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="448"/>
        <source>Exporting account…</source>
        <translation>حساب تصدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="449"/>
        <source>Remove Device</source>
        <translation>إزالة الجهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="450"/>
        <source>Are you sure you wish to remove this device?</source>
        <translation>هل أنت متأكد أنك تريد إزالة هذا الجهاز؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="451"/>
        <source>Your PIN is:</source>
        <translation>رقم رسومك هو:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="452"/>
        <source>Error connecting to the network.
Please try again later.</source>
        <translation>خطأ في الاتصال بالشبكة، رجاءً حاول مرة أخرى لاحقاً.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="455"/>
        <source>Banned</source>
        <translation>ممنوع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="456"/>
        <source>Banned contacts</source>
        <translation>الاتصالات المحظورة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="467"/>
        <source>Device Id</source>
        <translation>معرف الجهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="470"/>
        <source>System</source>
        <translation>نظام</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="473"/>
        <source>Select a folder</source>
        <translation>إختيار المجلد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="474"/>
        <source>Enable notifications</source>
        <translation>تمكين الإخطارات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="478"/>
        <source>Launch at startup</source>
        <translation>إطلاق عند بدء التشغيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="479"/>
        <source>Choose download directory</source>
        <translation>اختر دليل التنزيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="487"/>
        <source>Preview requires downloading content from third-party servers.</source>
        <translation>تطلب المشاهدة المسبقة تنزيل المحتوى من خوادم طرف ثالث.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="489"/>
        <source>User interface language</source>
        <translation>لغة واجهة المستخدم</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="490"/>
        <source>Vertical view</source>
        <translation>طريقة عرض عمودية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="491"/>
        <source>Horizontal view</source>
        <translation>طريقة عرض أفقية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="494"/>
        <source>File transfer</source>
        <translation>نقل ملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="495"/>
        <source>Automatically accept incoming files</source>
        <translation>تقبل الملفات الموصلة تلقائيًا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="497"/>
        <source>in MB, 0 = unlimited</source>
        <translation>في MB، 0 = غير محدود</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="500"/>
        <source>Register</source>
        <translation>سجل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="501"/>
        <source>Incorrect password</source>
        <translation>كلمة مرور خاطئة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="502"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="519"/>
        <source>Network error</source>
        <translation>خطأ في الشبكة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="503"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="527"/>
        <source>Something went wrong</source>
        <translation>شيء ما حدث خطأ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="506"/>
        <source>Save file</source>
        <translation>حفظ ملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="507"/>
        <source>Open location</source>
        <translation>الموقع المفتوح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="764"/>
        <source>Me</source>
        <translation>أنا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="511"/>
        <source>Install beta version</source>
        <translation>إعداد النسخة البيتا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="512"/>
        <source>Check for updates now</source>
        <translation>تحقق من التحديثات الآن</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="513"/>
        <source>Enable/Disable automatic updates</source>
        <translation>تمكين/منع التحديثات التلقائية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="514"/>
        <source>Updates</source>
        <translation>تحديثات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="515"/>
        <source>Update</source>
        <translation>التحديث</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="517"/>
        <source>No new version of Jami was found</source>
        <translation>لم يتم العثور على نسخة جديدة من جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="518"/>
        <source>An error occured when checking for a new version</source>
        <translation>حدث خطأ عند التحقق من نسخة جديدة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="520"/>
        <source>SSL error</source>
        <translation>خطأ SSL</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="521"/>
        <source>Installer download canceled</source>
        <translation>تم إلغاء تنزيل المثبِّت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="523"/>
        <source>This will uninstall your current Release version and you can always download the latest Release version on our website</source>
        <translation>هذا سيقوم بإزالة إصدار الإصدار الحالي الخاص بك ويمكنك دائماً تنزيل أحدث إصدار الإصدار على موقعنا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="524"/>
        <source>Network disconnected</source>
        <translation>إنقطاع الشبكة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="530"/>
        <source>Troubleshoot</source>
        <translation>حل المشاكل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="531"/>
        <source>Open logs</source>
        <translation>سجلات مفتوحة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="532"/>
        <source>Get logs</source>
        <translation>احصل على سجلات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="534"/>
        <source>(Experimental) Enable call support for swarm</source>
        <translation>(تجريبية) تمكين دعم المكالمة لالسحابة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="535"/>
        <source>This feature will enable call buttons in swarms with multiple participants.</source>
        <translation>هذه الميزة تمكن أزرار الدعوة في السحائر مع مشاركين متعددين.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="538"/>
        <source>Quality</source>
        <translation>الجودة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="542"/>
        <source>Always record calls</source>
        <translation>تسجيل المكالمات دائمًا</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="545"/>
        <source>Keyboard Shortcut Table</source>
        <translation>جدول المفاتيح المختصرة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="546"/>
        <source>Keyboard Shortcuts</source>
        <translation>اختصارات لوحة المفاتيح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="547"/>
        <source>Conversation</source>
        <translation>محادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="548"/>
        <source>Call</source>
        <translation>مكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="549"/>
        <source>Settings</source>
        <translation>الإعدادات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="553"/>
        <source>Debug</source>
        <translation>إعادة التشغيل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="555"/>
        <source>Report Bug</source>
        <translation>تقرير الحشرات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="556"/>
        <source>Clear</source>
        <translation>واضح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="557"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="705"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="558"/>
        <source>Copied to clipboard!</source>
        <translation>نسخة على المقطع!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="559"/>
        <source>Receive Logs</source>
        <translation>استلام السجلات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="562"/>
        <source>Archive</source>
        <translation>الملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="563"/>
        <source>Open file</source>
        <translation>فتح ملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="566"/>
        <source>Generating account…</source>
        <translation>إنشاء حساب...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="567"/>
        <source>Import from archive backup</source>
        <translation>الاستيراد من النسخة الاحتياطية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="569"/>
        <source>Select archive file</source>
        <translation>حدد ملف الأرشيف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="437"/>
        <source>Link device</source>
        <translation>اربط جهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="573"/>
        <source>Import</source>
        <translation>استيراد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="575"/>
        <source>A PIN is required to use an existing Jami account on this device.</source>
        <translation>مطلوب رمز PIN لاستخدام حساب جامي موجود على هذا الجهاز.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="581"/>
        <source>Choose the account to link</source>
        <translation>اختر الحساب لربط</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="587"/>
        <source>The PIN and the account password should be entered in your device within 10 minutes.</source>
        <translation>يجب إدخال رقم PIN وكلمة المرور الخاصة بالحساب في جهازك خلال 10 دقائق.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="610"/>
        <source>Choose a picture</source>
        <translation>اختر صورة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="790"/>
        <source>Contact&apos;s name</source>
        <translation>اسم الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="805"/>
        <source>Reinstate member</source>
        <translation>أعاد تعيين العضو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="819"/>
        <source>Delete message</source>
        <translation>حذف رسالة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="820"/>
        <source>*(Deleted Message)*</source>
        <translation>* * رسالة مُحذفة *</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="821"/>
        <source>Edit message</source>
        <translation>إصلاح الرسالة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="321"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="588"/>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="541"/>
        <source>Call recording</source>
        <translation>تسجيل المكالمات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="570"/>
        <source>If the account is encrypted with a password, please fill the following field.</source>
        <translation>إذا كان الحساب مشفرًا بمشفرة، يرجى ملء الحقل التالي.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="574"/>
        <source>Enter the PIN code</source>
        <translation>إدخال رمز الرقم الشخصي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="576"/>
        <source>Step 01</source>
        <translation>الخطوة رقم 01</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="577"/>
        <source>Step 02</source>
        <translation>الخطوة 02</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="578"/>
        <source>Step 03</source>
        <translation>الخطوة الثالثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="579"/>
        <source>Step 04</source>
        <translation>الخطوة 04</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="580"/>
        <source>Go to the account management settings of a previous device</source>
        <translation>اذهب إلى إعدادات إدارة الحسابات من جهاز سابق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="583"/>
        <source>The PIN code will be available for 10 minutes</source>
        <translation>رمز الرقم الحجري سيكون متاحاً لمدة 10 دقائق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="584"/>
        <source>Fill if the account is password-encrypted.</source>
        <translation>إملأ إذا كان الحساب مشفرًا بالكلمة المرورية.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="591"/>
        <source>Add Device</source>
        <translation>إضافة الجهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="601"/>
        <source>Enter current password</source>
        <translation>إدخال كلمة المرور الحالية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="603"/>
        <source>Enter new password</source>
        <translation>إدخال كلمة مرور جديدة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="604"/>
        <source>Confirm new password</source>
        <translation>تأكيد كلمة المرور الجديدة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="605"/>
        <source>Change</source>
        <translation>التغيير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="606"/>
        <source>Export</source>
        <translation>تصدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="611"/>
        <source>Import avatar from image file</source>
        <translation>استيراد المحتويات من ملف الصورة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="612"/>
        <source>Clear avatar image</source>
        <translation>صورة اوتار واضحة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="613"/>
        <source>Take photo</source>
        <translation>التقاط صورة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="626"/>
        <source>Preferences</source>
        <translation>التفضيلات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="629"/>
        <source>Reset</source>
        <translation>إعادة تعيين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="630"/>
        <source>Uninstall</source>
        <translation>إزالة التثبيت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="631"/>
        <source>Reset Preferences</source>
        <translation>إعادة تعيين الاختيارات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="632"/>
        <source>Select a plugin to install</source>
        <translation>اختر مكونات التضمين لتنسيقها</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="633"/>
        <source>Uninstall plugin</source>
        <translation>إزالة التثبيت</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="634"/>
        <source>Are you sure you wish to reset %1 preferences?</source>
        <translation>هل أنت متأكد من أنك تريد إعادة تعيين تفضيلات %1؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="635"/>
        <source>Are you sure you wish to uninstall %1?</source>
        <translation>هل أنت متأكد من أنك تريد إزالة تثبيت %1؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="636"/>
        <source>Go back to plugins list</source>
        <translation>عود إلى قائمة المكونات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="637"/>
        <source>Select a file</source>
        <translation>إختار ملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="119"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="638"/>
        <source>Select</source>
        <translation>اختر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="639"/>
        <source>Choose image file</source>
        <translation>اختر ملف الصورة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="640"/>
        <source>Plugin Files (*.jpl)</source>
        <translation>ملفات المكونات التضخمية (*.jpl)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="641"/>
        <source>Load/Unload</source>
        <translation>الحمل/إفراج</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="642"/>
        <source>Select An Image to %1</source>
        <translation>حدد صورة إلى % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="643"/>
        <source>Edit preference</source>
        <translation>تحرير تفضيلات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="644"/>
        <source>On/Off</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="645"/>
        <source>Choose Plugin</source>
        <translation>اختر المكونات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="651"/>
        <source>Information</source>
        <translation>المعلومات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="653"/>
        <source>Profile</source>
        <translation>الملف الشخصى</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="656"/>
        <source>Enter the account password to confirm the removal of this device</source>
        <translation>إدخال كلمة المرور للحساب للتأكيد على إزالة هذا الجهاز</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="659"/>
        <source>Select a screen to share</source>
        <translation>حدد شاشة للمشاركة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="660"/>
        <source>Select a window to share</source>
        <translation>اختر نافذة لتشاركها</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="661"/>
        <source>All Screens</source>
        <translation>جميع الشاشات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="662"/>
        <source>Screens</source>
        <translation>الشاشات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="663"/>
        <source>Windows</source>
        <translation>النوافذ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="664"/>
        <source>Screen %1</source>
        <translation>الشاشة %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="667"/>
        <source>QR code</source>
        <translation>رمز QR</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="670"/>
        <source>Link this device to an existing account</source>
        <translation>ربط هذا الجهاز بحساب موجود</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="671"/>
        <source>Import from another device</source>
        <translation>الاستيراد من جهاز آخر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="672"/>
        <source>Import from an archive backup</source>
        <translation>الاستيراد من نسخة احتياطية من الأرشيف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="673"/>
        <source>Advanced features</source>
        <translation>الميزات المتقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="674"/>
        <source>Show advanced features</source>
        <translation>إظهار الميزات المتقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="675"/>
        <source>Hide advanced features</source>
        <translation>إخفاء الميزات المتقدمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="676"/>
        <source>Connect to a JAMS server</source>
        <translation>التواصل مع خادم JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="677"/>
        <source>Create account from Jami Account Management Server (JAMS)</source>
        <translation>إنشاء حساب من خادم إدارة الحسابات Jami (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="678"/>
        <source>Configure a SIP account</source>
        <translation>إعداد حساب SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="679"/>
        <source>Error while creating your account. Check your credentials.</source>
        <translation>خطأ أثناء إنشاء حسابك تحقق من إثباتاتك</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="680"/>
        <source>Create a rendezvous point</source>
        <translation>إنشاء نقطة اجتماع</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="681"/>
        <source>Join Jami</source>
        <translation>انضموا إلى جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="682"/>
        <source>Create new Jami account</source>
        <translation>إعداد حساب جامي جديد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="683"/>
        <source>Create new SIP account</source>
        <translation>إعداد حساب SIP جديد</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="684"/>
        <source>About Jami</source>
        <translation>حول جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="686"/>
        <source>I already have an account</source>
        <translation>لدي حساب بالفعل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="687"/>
        <source>Use existing Jami account</source>
        <translation>استخدم حساب جامي الحالي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="688"/>
        <source>Welcome to Jami</source>
        <translation>مرحبا بك في جامي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="691"/>
        <source>Clear Text</source>
        <translation>نص واضح</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="692"/>
        <source>Conversations</source>
        <translation>محادثات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="693"/>
        <source>Search Results</source>
        <translation>نتائج البحث</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="696"/>
        <source>Decline contact request</source>
        <translation>رفض طلب الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="697"/>
        <source>Accept contact request</source>
        <translation>قبول طلب الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="700"/>
        <source>Automatically check for updates</source>
        <translation>بحث تلقائي عن التحديثات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="703"/>
        <source>Ok</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="463"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="704"/>
        <source>Save</source>
        <translation>سجل</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="706"/>
        <source>Upgrade</source>
        <translation>تحسين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="707"/>
        <source>Later</source>
        <translation>لاحقاً</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="708"/>
        <source>Delete</source>
        <translation>حذف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="710"/>
        <source>Block</source>
        <translation>حظر</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="714"/>
        <source>Set moderator</source>
        <translation>حدد المدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="715"/>
        <source>Unset moderator</source>
        <translation>إزالة المدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="317"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="718"/>
        <source>Maximize</source>
        <translation>تكبير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="719"/>
        <source>Minimize</source>
        <translation>خفض</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="720"/>
        <source>Hangup</source>
        <translation>إنهاء المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="721"/>
        <source>Local muted</source>
        <translation>محلية غامضة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="724"/>
        <source>Default moderators</source>
        <translation>المُتَحَاكِمُونَ الافتراضيّون</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="725"/>
        <source>Enable local moderators</source>
        <translation>تمكين المديرين المحليين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="726"/>
        <source>Make all participants moderators</source>
        <translation>اجعل جميع المشاركين منتظمين</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="727"/>
        <source>Add default moderator</source>
        <translation>إضافة المدير الافتراضي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="729"/>
        <source>Remove default moderator</source>
        <translation>إزالة المدير الافتراضي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="736"/>
        <source>Add emoji</source>
        <translation>إضافة الايموجي</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="738"/>
        <source>Send file</source>
        <translation>إرسال ملف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="760"/>
        <source>Send</source>
        <translation>إرسال</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="466"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="709"/>
        <source>Remove</source>
        <translation>احذف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="41"/>
        <source>Migrate conversation</source>
        <translation>الهجرة محادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="475"/>
        <source>Show notifications</source>
        <translation>عرض الإخطارات</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="476"/>
        <source>Minimize on close</source>
        <translation>الحد الأدنى من الإقتراب</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="477"/>
        <source>Run at system startup</source>
        <translation>تشغيل عند بدء النظام</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="564"/>
        <source>Create account from backup</source>
        <translation>إعداد الحساب من النسخة الاحتياطية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="565"/>
        <source>Restore account from backup</source>
        <translation>استعادة الحساب من النسخة الاحتياطية</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="568"/>
        <source>Import Jami account from local archive file.</source>
        <translation>استيراد حساب جامي من ملف أرشيف محلي.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="614"/>
        <source>Image Files (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</source>
        <translation>ملفات الصورة (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="766"/>
        <source>Write to %1</source>
        <translation>إكتب إلى %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="781"/>
        <source>%1 has sent you a request for a conversation.</source>
        <translation>%1 أرسل لك طلب للحديث.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="782"/>
        <source>Hello,
Would you like to join the conversation?</source>
        <translation>مرحباً، هل تودين الانضمام إلى المحادثة؟</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="783"/>
        <source>You have accepted
the conversation request</source>
        <translation>لقد قبلت طلب المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="784"/>
        <source>Waiting until %1
connects to synchronize the conversation.</source>
        <translation>الانتظار حتى يتم توصيل %1 للتزامن مع المحادثة.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="787"/>
        <source>%1 Members</source>
        <translation>%1 أعضاء</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="788"/>
        <source>Member</source>
        <translation>عضو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="789"/>
        <source>Swarm&apos;s name</source>
        <translation>اسم السحرة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="791"/>
        <source>Add a description</source>
        <translation>إضافة وصف</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="794"/>
        <source>Ignore all notifications from this conversation</source>
        <translation>تجاهل كل الإخطارات من هذه المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="795"/>
        <source>Choose a color</source>
        <translation>اختر لون</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="797"/>
        <source>Leave conversation</source>
        <translation>أترك المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="798"/>
        <source>Type of swarm</source>
        <translation>نوع السحابة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="802"/>
        <source>Create the swarm</source>
        <translation>إنشاء السحابة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="803"/>
        <source>Go to conversation</source>
        <translation>إذهب إلى المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="804"/>
        <source>Kick member</source>
        <translation>عضو ركلة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="806"/>
        <source>Administrator</source>
        <translation>المدير</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="807"/>
        <source>Invited</source>
        <translation>مدعوة</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="808"/>
        <source>Remove member</source>
        <translation>إزالة عضو</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="809"/>
        <source>To:</source>
        <translation>إلى:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="812"/>
        <source>Customize</source>
        <translation>تخصيص</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="814"/>
        <source>Dismiss</source>
        <translation>رفض</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="816"/>
        <source>Your profile is only shared with your contacts</source>
        <translation>حسابك مشارك فقط مع جهات إتصالك</translation>
    </message>
</context>
<context>
    <name>KeyboardShortcutTable</name>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="41"/>
        <source>Open account list</source>
        <translation>قائمة الحسابات المفتوحة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="45"/>
        <source>Focus conversations list</source>
        <translation>قائمة محادثات التركيز</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="49"/>
        <source>Requests list</source>
        <translation>قائمة الطلبات</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="53"/>
        <source>Previous conversation</source>
        <translation>محادثة سابقة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="57"/>
        <source>Next conversation</source>
        <translation>المحادثة التالية</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="61"/>
        <source>Search bar</source>
        <translation>شريط البحث</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="65"/>
        <source>Full screen</source>
        <translation>ملء الشاشة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="69"/>
        <source>Increase font size</source>
        <translation>زيادة حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="73"/>
        <source>Decrease font size</source>
        <translation>خفض حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="77"/>
        <source>Reset font size</source>
        <translation>إعادة تعيين حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="85"/>
        <source>Start an audio call</source>
        <translation>أبدأ مكالمة صوتية</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="89"/>
        <source>Start a video call</source>
        <translation>أبدأ مكالمة فيديو</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="93"/>
        <source>Clear history</source>
        <translation>حذف المحفوظات</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="97"/>
        <source>Search messages/files</source>
        <translation>البحث عن رسائل/ملفات</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="101"/>
        <source>Block contact</source>
        <translation>حظر جهة إتصال</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="105"/>
        <source>Remove conversation</source>
        <translation>إزالة المحادثة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="109"/>
        <source>Accept contact request</source>
        <translation>قبول طلب الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="113"/>
        <source>Edit last message</source>
        <translation>تحرير الرسالة الأخيرة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="117"/>
        <source>Cancel message edition</source>
        <translation>إلغاء إصدار الرسالة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="125"/>
        <source>Media settings</source>
        <translation>إعدادات الوسائط</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="129"/>
        <source>General settings</source>
        <translation>الإعدادات العامة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="133"/>
        <source>Account settings</source>
        <translation>إعدادت الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="137"/>
        <source>Plugin settings</source>
        <translation>إعدادات المكونات</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="141"/>
        <source>Open account creation wizard</source>
        <translation>مفتاح إعداد الحساب</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="146"/>
        <source>Open keyboard shortcut table</source>
        <translation>فتح جدول اختصار لوحة المفاتيح</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="154"/>
        <source>Answer an incoming call</source>
        <translation>استجيب للاتصال الوارد</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="158"/>
        <source>End call</source>
        <translation>إنهاء المكالمة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="162"/>
        <source>Decline the call request</source>
        <translation>رفض طلب الدعوة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="166"/>
        <source>Mute microphone</source>
        <translation>كتم ميكروفون</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="170"/>
        <source>Stop camera</source>
        <translation>أوقف الكاميرا</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="174"/>
        <source>Take tile screenshot</source>
        <translation>خذ صورة شاشة من الطلاء</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="182"/>
        <source>Bold</source>
        <translation>غامق</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="186"/>
        <source>Italic</source>
        <translation>مائل</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="190"/>
        <source>Strikethrough</source>
        <translation>يتوسطه خط</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="194"/>
        <source>Heading</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="198"/>
        <source>Link</source>
        <translation>ربط</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="202"/>
        <source>Code</source>
        <translation>رمز</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="206"/>
        <source>Quote</source>
        <translation>اقتباس</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="210"/>
        <source>Unordered list</source>
        <translation>قائمة غير مرتبة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="214"/>
        <source>Ordered list</source>
        <translation>قائمة مرتبة</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="218"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="222"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="375"/>
        <source>E&amp;xit</source>
        <translation>إيكسيت</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="377"/>
        <source>&amp;Quit</source>
        <translation>&quot;توقف&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="383"/>
        <source>&amp;Show Jami</source>
        <translation>أظهروا (جامي)</translation>
    </message>
</context>
<context>
    <name>PositionManager</name>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="414"/>
        <source>%1 is sharing their location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="419"/>
        <source>Location sharing</source>
        <translation>مشاركة الموقع</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/libclient/qtwrapper/callmanager_wrap.h" line="458"/>
        <source>Me</source>
        <translation>أنا</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="62"/>
        <source>Hold</source>
        <translation>تعليق</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="64"/>
        <source>Talking</source>
        <translation>يتم التكلم</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="66"/>
        <source>ERROR</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="68"/>
        <source>Incoming</source>
        <translation>الوارد</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="70"/>
        <source>Calling</source>
        <translation>يتّصل</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="373"/>
        <location filename="../src/libclient/api/call.h" line="72"/>
        <source>Connecting</source>
        <translation>يتم الإتصال</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="74"/>
        <source>Searching</source>
        <translation>يتم البحث</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="76"/>
        <source>Inactive</source>
        <translation>خامل</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="390"/>
        <location filename="../src/libclient/api/call.h" line="78"/>
        <location filename="../src/libclient/api/call.h" line="84"/>
        <source>Finished</source>
        <translation>تم الإنتهاء</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="80"/>
        <source>Timeout</source>
        <translation>انتهت المهلة</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="82"/>
        <source>Peer busy</source>
        <translation>نظير مشغول</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="86"/>
        <source>Communication established</source>
        <translation>تم إنجاز الإتصال</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="211"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="994"/>
        <source>Invitation received</source>
        <translation>تم تلقي دعوة</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="260"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="208"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="992"/>
        <source>Contact added</source>
        <translation>تم إضافة جهة إتصال</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="262"/>
        <source>%1 was invited to join</source>
        <translation>%1 تم دعوة للانضمام</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="264"/>
        <source>%1 joined</source>
        <translation>%1 انضم</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="266"/>
        <source>%1 left</source>
        <translation>%1 يسارا</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="268"/>
        <source>%1 was kicked</source>
        <translation>%1 تم ركل</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="270"/>
        <source>%1 was re-added</source>
        <translation>%1 تم إعادة إضافة</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="359"/>
        <source>Private conversation created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="361"/>
        <source>Swarm created</source>
        <translation>حشرة خلق</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="174"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="180"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="990"/>
        <source>Outgoing call</source>
        <translation>مكالمة خارجة</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="176"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="186"/>
        <source>Incoming call</source>
        <translation>مكالمة واردة</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="182"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="988"/>
        <source>Missed outgoing call</source>
        <translation>مكالمة خارجة فائتة</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="188"/>
        <source>Missed incoming call</source>
        <translation>مكالمة داخلة فائتة</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="198"/>
        <source>Join call</source>
        <translation>إلتحق بمكالمة</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="213"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="996"/>
        <source>Invitation accepted</source>
        <translation>تم قبول الدعوة</translation>
    </message>
    <message>
        <location filename="../src/libclient/avmodel.cpp" line="391"/>
        <location filename="../src/libclient/avmodel.cpp" line="410"/>
        <source>default</source>
        <translation>افتراضي</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="72"/>
        <source>Null</source>
        <translation>فارغ</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="73"/>
        <source>Trying</source>
        <translation>محاولة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="74"/>
        <source>Ringing</source>
        <translation>يرنّ</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="75"/>
        <source>Being Forwarded</source>
        <translation>تتم إحالته</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="76"/>
        <source>Queued</source>
        <translation>قائمة الإنتظار</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="77"/>
        <source>Progress</source>
        <translation>تقدم</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="78"/>
        <source>OK</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="79"/>
        <source>Accepted</source>
        <translation>مقبول</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="80"/>
        <source>Multiple Choices</source>
        <translation>خيارات متعددة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="81"/>
        <source>Moved Permanently</source>
        <translation>انتقال بشكل دائم</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="82"/>
        <source>Moved Temporarily</source>
        <translation>انتقال مؤقت</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="83"/>
        <source>Use Proxy</source>
        <translation>استخدام وكيل</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="84"/>
        <source>Alternative Service</source>
        <translation>خدمة بديلة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="85"/>
        <source>Bad Request</source>
        <translation>طلب غير صالح</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="86"/>
        <source>Unauthorized</source>
        <translation>غير مسموح به</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="87"/>
        <source>Payment Required</source>
        <translation>يرجى الدفع</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="88"/>
        <source>Forbidden</source>
        <translation>محظور</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="89"/>
        <source>Not Found</source>
        <translation>لم يتم العثور</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="90"/>
        <source>Method Not Allowed</source>
        <translation>لا يسمح بإستخدام الطريقة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="91"/>
        <location filename="../src/libclient/callmodel.cpp" line="111"/>
        <source>Not Acceptable</source>
        <translation>غير مقبول</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="92"/>
        <source>Proxy Authentication Required</source>
        <translation>مطلوبة التحقق من الوكيل</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="93"/>
        <source>Request Timeout</source>
        <translation>نفاذ مدة الطلب</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="94"/>
        <source>Gone</source>
        <translation>لقد رحلنا</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="95"/>
        <source>Request Entity Too Large</source>
        <translation>طلب الكيان كبير جدا</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="96"/>
        <source>Request URI Too Long</source>
        <translation>طلب URI طويلاً جداً</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="97"/>
        <source>Unsupported Media Type</source>
        <translation>نوع الوسائط غير المدعومة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="98"/>
        <source>Unsupported URI Scheme</source>
        <translation>نظام URI غير مدعوم</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="99"/>
        <source>Bad Extension</source>
        <translation>التوسع السيئ</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="100"/>
        <source>Extension Required</source>
        <translation>المدة المطلوبة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="101"/>
        <source>Session Timer Too Small</source>
        <translation>توقيت الجلسة صغير جدا</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="102"/>
        <source>Interval Too Brief</source>
        <translation>فترة قصيرة جدا</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="103"/>
        <source>Temporarily Unavailable</source>
        <translation>غير متوفر مؤقتاً</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="104"/>
        <source>Call TSX Does Not Exist</source>
        <translation>الاتصال TSX لا يوجد</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="105"/>
        <source>Loop Detected</source>
        <translation>تم اكتشاف الحلقة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="106"/>
        <source>Too Many Hops</source>
        <translation>الكثير من القفز</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="107"/>
        <source>Address Incomplete</source>
        <translation>العنوان غير مكتمل</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="108"/>
        <source>Ambiguous</source>
        <translation>غير واضح</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="109"/>
        <source>Busy</source>
        <translation>مشغول</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="110"/>
        <source>Request Terminated</source>
        <translation>طلب انتهى</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="112"/>
        <source>Bad Event</source>
        <translation>حدث سيء</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="113"/>
        <source>Request Updated</source>
        <translation>طلب تحديث</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="114"/>
        <source>Request Pending</source>
        <translation>الطلب في انتظار</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="115"/>
        <source>Undecipherable</source>
        <translation>لا يمكن تحديدها</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="116"/>
        <source>Internal Server Error</source>
        <translation>خطأ خادم داخلي</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="117"/>
        <source>Not Implemented</source>
        <translation>لم يتم تنفيذها</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="118"/>
        <source>Bad Gateway</source>
        <translation>بوابة سيئة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="119"/>
        <source>Service Unavailable</source>
        <translation>الخدمة غير متوفرة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="120"/>
        <source>Server Timeout</source>
        <translation>وقت وقف الخادم</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="121"/>
        <source>Version Not Supported</source>
        <translation>الإصدار غير مدعوم</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="122"/>
        <source>Message Too Large</source>
        <translation>رسالة كبيرة جدا</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="123"/>
        <source>Precondition Failure</source>
        <translation>فشل في الحالة السابقة</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="124"/>
        <source>Busy Everywhere</source>
        <translation>مشغول في كل مكان</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="125"/>
        <source>Call Refused</source>
        <translation>مكالمة رفض</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="126"/>
        <source>Does Not Exist Anywhere</source>
        <translation>لا يوجد في أي مكان</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="127"/>
        <source>Not Acceptable Anywhere</source>
        <translation>لا يمكن قبولها في أي مكان</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="375"/>
        <source>Accept</source>
        <translation>اقبل</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="367"/>
        <source>Sending</source>
        <translation>جاري الإرسال</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="371"/>
        <source>Sent</source>
        <translation>أرسلت</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="380"/>
        <source>Unable to make contact</source>
        <translation>لا يمكن الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="384"/>
        <source>Waiting for contact</source>
        <translation>أنتظر الاتصال</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="386"/>
        <source>Incoming transfer</source>
        <translation>النقل المقبل</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="388"/>
        <source>Timed out waiting for contact</source>
        <translation>توقيت الانتظار للتواصل</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="742"/>
        <source>Today</source>
        <translation>اليوم</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="744"/>
        <source>Yesterday</source>
        <translation>البارحة</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="377"/>
        <source>Canceled</source>
        <translation>تم الإلغاء</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="382"/>
        <source>Ongoing</source>
        <translation>مستمرة</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="723"/>
        <source>just now</source>
        <translation>في هذه اللحظة</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="369"/>
        <source>Failure</source>
        <translation>إخفاق</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="109"/>
        <source>locationServicesError</source>
        <translation>الموقعالخدماتخطأ</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="112"/>
        <source>locationServicesClosedError</source>
        <translation>الموقعالخدماتغلق الخطأ</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="114"/>
        <source>locationServicesUnknownError</source>
        <translation>الموقعالخدماتالمعروفةخطأ</translation>
    </message>
    <message>
        <location filename="../src/libclient/conversationmodel.cpp" line="1166"/>
        <location filename="../src/libclient/conversationmodel.cpp" line="1179"/>
        <source>%1 (you)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SmartListModel</name>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="68"/>
        <location filename="../src/app/smartlistmodel.cpp" line="107"/>
        <location filename="../src/app/smartlistmodel.cpp" line="115"/>
        <location filename="../src/app/smartlistmodel.cpp" line="161"/>
        <location filename="../src/app/smartlistmodel.cpp" line="191"/>
        <location filename="../src/app/smartlistmodel.cpp" line="192"/>
        <source>Calls</source>
        <translation>مكالمات</translation>
    </message>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="69"/>
        <location filename="../src/app/smartlistmodel.cpp" line="108"/>
        <location filename="../src/app/smartlistmodel.cpp" line="125"/>
        <location filename="../src/app/smartlistmodel.cpp" line="162"/>
        <location filename="../src/app/smartlistmodel.cpp" line="193"/>
        <location filename="../src/app/smartlistmodel.cpp" line="194"/>
        <source>Contacts</source>
        <translation>جهات إتصال</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../src/app/systemtray.cpp" line="216"/>
        <source>Answer</source>
        <translation>أجب</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="217"/>
        <source>Decline</source>
        <translation>رفض</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="219"/>
        <source>Open conversation</source>
        <translation>محادثة مفتوحة</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="221"/>
        <source>Accept</source>
        <translation>اقبل</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="222"/>
        <source>Refuse</source>
        <translation>أرفض</translation>
    </message>
</context>
<context>
    <name>TipsModel</name>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="85"/>
        <source>Customize</source>
        <translation>تخصيص</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="88"/>
        <source>What does Jami mean?</source>
        <translation>ماذا تعني (جامي) ؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="94"/>
        <source>What is the green dot next to my account?</source>
        <translation>ما هي النقطة الخضراء بجانب حسابي؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="90"/>
        <source>The choice of the name Jami was inspired by the Swahili word &apos;jamii&apos;, which means &apos;community&apos; as a noun and &apos;together&apos; as an adverb.</source>
        <translation>تم إلهام اختيار اسم جامي من الكلمة السواحلية &quot;جامي&quot;، التي تعني &quot;المجتمع&quot; كاسم و&quot;مجمعين&quot; كصفت.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="81"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="86"/>
        <source>Backup account</source>
        <translation>حساب احتياطي</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="96"/>
        <source>A red dot means that your account is disconnected from the network; it turns green when it&apos;s connected.</source>
        <translation>نقطة حمراء تعني أن حسابك منفصل عن الشبكة؛ فإنه يصبح أخضر عندما يتم توصيله.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="101"/>
        <source>Why should I back up my account?</source>
        <translation>لماذا يجب أن أؤكد حسابي؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="103"/>
        <source>Jami is distributed and your account is only stored locally on your device. If you lose your password or your local account data, you WILL NOT be able to recover your account if you did not back it up earlier.</source>
        <translation>يتم توزيع Jami و يتم تخزين حسابك محليا فقط على جهازك. إذا فقدت كلمة المرور أو بيانات حسابك المحلي، فلن تكون قادرًا على استعادة حسابك إذا لم تقوم بحمايته في وقت سابق.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="109"/>
        <source>Can I make a conference call?</source>
        <translation>هل يمكنني إجراء مكالمة مؤتمرية؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="114"/>
        <source>What is a Jami account?</source>
        <translation>ما هو حساب جامي؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="116"/>
        <source>A Jami account is an asymmetric encryption key. Your account is identified by a Jami ID, which is a fingerprint of your public key.</source>
        <translation>حساب جامي هو مفتاح تشفير غير متماثل يتم تحديد حسابك بواسطة رقم هويتك جامي، وهو بصمة أصابع مفتاحك العام.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="120"/>
        <source>What information do I need to provide to create a Jami account?</source>
        <translation>ما المعلومات التي أحتاجها لإعطاءها لإنشاء حساب جامي؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="122"/>
        <source>When you create a new Jami account, you do not have to provide any private information like an email, address, or phone number.</source>
        <translation>عندما تقوم بإنشاء حساب جامي جديد، لا تحتاج إلى إعطاء أي معلومات خاصة مثل بريد إلكتروني أو عنوان أو رقم هاتف.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="129"/>
        <source>With Jami, your account is stored in a directory on your device. The password is only used to encrypt your account in order to protect you from someone who has physical access to your device.</source>
        <translation>مع جامي، يتم تخزين حسابك في دليل على جهازك. يتم استخدام كلمة المرور فقط لتشفير حسابك من أجل حمايتك من شخص لديه إمكانية الوصول المادي إلى جهازك.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="149"/>
        <source>Your account is only stored on your own devices. If you delete your account from all of your devices, the account is gone forever and you CANNOT recover it.</source>
        <translation>حسابك يتم تخزينه فقط على أجهزةك. إذا قمت بحذف حسابك من جميع أجهزتك، فإن الحساب قد اختفى إلى الأبد ولا يمكنك استعاده.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="153"/>
        <source>Can I use my account on multiple devices?</source>
        <translation>هل يمكنني استخدام حسابي على أجهزة متعددة؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="155"/>
        <source>Yes, you can link your account from the settings, or you can import your backup on another device.</source>
        <translation>نعم، يمكنك ربط حسابك من الإعدادات، أو يمكنك استيراد النسخة الاحتياطية الخاصة بك على جهاز آخر.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="127"/>
        <source>Why don&apos;t I have to use a password?</source>
        <translation>لماذا لا يجب أن أستخدم كلمة مرور؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="111"/>
        <source>In a call, you can click on &quot;Add participants&quot; to add a contact to a call.</source>
        <translation>في مكالمة، يمكنك النقر على &quot;إضافة المشاركين&quot; لإضافة جهة اتصال إلى مكالمة.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="135"/>
        <source>Why don&apos;t I have to register a username?</source>
        <translation>لماذا لا يتعين علي تسجيل اسم مستخدم؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="137"/>
        <source>The most permanent, secure identifier is your Jami ID, but since these are difficult to use for some people, you also have the option of registering a username.</source>
        <translation>أكثر المعرفات الدائمة والأمانية هو هويتك جامي، ولكن بما أن هذه صعبة الاستخدام لبعض الأشخاص، لديك أيضاً خيار تسجيل اسم مستخدم.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="142"/>
        <source>How can I back up my account?</source>
        <translation>كيف يمكنني تأمين حسابي؟</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="143"/>
        <source>In Account Settings, a button is available to create a backup your account.</source>
        <translation>في إعدادات الحساب، هناك زر متاح لإنشاء نسخة احتياطية لحسابك.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="147"/>
        <source>What happens when I delete my account?</source>
        <translation>ماذا يحدث عندما أقوم بحذف حسابي؟</translation>
    </message>
</context>
<context>
    <name>UtilsAdapter</name>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>%1 Mbps</source>
        <translation>%1 ميجابت/ثانية</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>Default</source>
        <translation>افتراضي</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="539"/>
        <source>System</source>
        <translation>نظام</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="478"/>
        <source>Searching…</source>
        <translation>يتم البحث...</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1015"/>
        <source>Invalid ID</source>
        <translation>هوية غير صالحة</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1018"/>
        <source>Username not found</source>
        <translation>اسم المستخدم لم يتم العثور</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1021"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>لم أستطع البحث</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="441"/>
        <source>Bad URI scheme</source>
        <translation>خطة URI سيئة</translation>
    </message>
</context>
</TS>